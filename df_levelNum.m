function n = df_levelNum(DF,f)

% n = df_getFactor(DF,f)
% f ... {''} or '' factorname
%

if ischar(f);f = {f};end
nf = length(f);

n = zeros(1,nf);

for i = 1:nf
    isf = strcmp(f{i},DF.factor);
    if any(isf);
        n(i) = length(DF.level{isf});
    end
end
end
