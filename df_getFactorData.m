function df = df_getFactorData(DF,cFac,cLevel)
    % extract data of specified level of a factor
    % same as getContrastData
    % Data ... [1 length(cLevel)] DataFrameObjects
    [nFac,nLev] = getsize(DF);
    if isnumeric(cLevel)
        [iFac] = findFactor(DF,cFac);
        iLevel = cLevel;
    else
        [iLevel,iFac] = findFactorLevel(DF,cFac,cLevel);
    end
    if isnan(iFac) || (isnumeric(iLevel)&&any(iLevel==0)) || (iscell(iLevel)&&any(cellfun('isempty',iLevel)))
        error('Can''t find factor level!'); 
        df=[];return;
    end
    
    nL = length(cLevel);
    
    n = prod(nLev)/nLev(iFac);
    nLevReshaped = nLev([1:nFac]~=iFac);
    if length(nLevReshaped)>1% make safe for one factor
        Data = cell(nLevReshaped);
    else
        Data = cell(nLevReshaped,1);
    end
    
    for iL = 1:nL
        df(iL) = DataFrame;
        df(iL).cells = Data;
        df(iL).factor = DF.factor([1:nFac]~=iFac);
        df(iL).level = DF.level([1:nFac]~=iFac);
    end
    
    LevIdx = cell(1,nFac);
    nLevRed = nLev;
    nLevRed(iFac) = 1;
    for i=1:n
        [LevIdx{:}] = ind2sub(nLevRed,i);
        for iL = 1:nL
            LevIdx{iFac} = iLevel(iL);
            df(iL).cells(i) = DF.cells(LevIdx{:});
        end
    end
end
