function [r] = df_seriesParamRange(self,varargin)

% use label as input
% r = df_seriesParamRange(self,'seriesLabel1','seriesLabel2', ... ,'seriesLabeln')
% 
% use dimension as input
% x = df_getSeries(self,1,2, ... ,n)
%
% to vectorize output for one serieslabel
% x = df_getSeries(self,'seriesLabel1',true)

nSeries = length(varargin);
nDF = numel(self);
r = cell(1,nSeries);
for iDF=1:nDF
    cPar = df_getSeries(self(iDF),varargin{:});
    if ~iscell(cPar)
        cPar = {cPar};
    end
    for iSer = 1:nSeries
        r{iSer}(iDF,:) = [min(cPar{iSer}) max(cPar{iSer})];
    end
end
