function self = df_removeSeriesInfo(self,iSer)

if ischar(iSer)
    iSer = strcmp(self.serieslabel,iSer);
end

fieldnames = {'seriesdim' 'seriesparam' 'serieslabel' 'seriesperiod' 'seriesoffset'};
for i=1:length(fieldnames)
    if length(self.(fieldnames{i}))>=iSer
        self.(fieldnames{i})(iSer) = [];
    end
end
 


