function DF = df_extractFactorLevel(DF,varargin)
% DF_EXTRACTFACTORLEVEL Extract given levels of factors
%
%  i.e. remove non-matching levels from object
%
%
% Examples
% --------
% ::
%
%     % extracts a single level of factors with this level
%     DF = df_extractFactorLevel(DF,level)
%     
%     DF = df_extractFactorLevel(DF, ...
%         'Fac1',{'F1Level1' 'F1Level2' ... 'F1LevelN'}, ... 
%         'Fac2',[par1 par2 par3 ... parN], ...
%         'FacN',{'FNLevel1' 'FNLevel2' ... 'FNLevelN'})
%     
%

argNum = length(varargin);

nDF = numel(DF);
for iDF = 1:nDF
    
    %nFac = df_getsize(DF(iDF));
    nFac = length(DF(iDF).level);
    
    if argNum==1
        levelFac = cell(1,nFac*2);
        levelFac(1:2:end) = DF.factor;
        levelFac(2:2:end)  = varargin{1};
        [iLevel,iFac] = df_findFactorLevel(DF(iDF),levelFac{:});
    elseif argNum>1 && rem(argNum,2)==0
        for iArgFac = 1:argNum/2
            cFac = varargin{(iArgFac-1)*2+1};
            cLev = varargin{(iArgFac-1)*2+2};           
            [iLevel,iFac(iArgFac)] = df_findFactorLevel(DF(iDF),cFac,cLev);
        end    
    end
    
    if isnumeric(iLevel);iLevel = {iLevel};end

    nExFac = length(iFac);
    for iExFac = 1:nExFac
        iLevel{iExFac} = iLevel{iExFac}(iLevel{iExFac}~=0);
        DF(iDF).level{iFac(iExFac)} = DF(iDF).level{iFac(iExFac)}(iLevel{iExFac});
        nLevel = cellfun('length',DF(iDF).level);
        if nFac>1
            permind = circshift(1:nFac,[0 1-iFac(iExFac)]);
            nLevel = circshift(nLevel,[0 1-iFac(iExFac)]);
            DF(iDF).cells = permute(DF(iDF).cells,permind);
        end
        DF(iDF).cells = DF(iDF).cells(iLevel{iExFac},:);
        if nFac>1
            DF(iDF).cells = reshape(DF(iDF).cells,nLevel);
            permind = circshift(1:nFac,[0 iFac(iExFac)-1]);
            nLevel = circshift(nLevel,[0 iFac(iExFac)-1]);
            DF(iDF).cells = permute(DF(iDF).cells,permind);
        end
    end
end

