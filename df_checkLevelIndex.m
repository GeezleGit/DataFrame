function levelIndex = df_checkLevelIndex(self,factorLabel,Operation,RefValue)

% returns a level index of selected factor
% levelIndex = df_checkLevelIndex(self,factorLabel,Operation,RefValue)

error('Under Construction !!!!!!!!!!!!!');

FacNr = df_findFactor(self,factorLabel);
cLev = self.level{FacNr};
nLev = length(self.level{FacNr});

cellnums = size(self.cells);
dimnums = length(cellnums);

% create permutation array to make factor the first dimension (and reverse)
permarray = 1:dimnums;
permarray([1 FacNr]) = [FacNr 1];

% switch Operation
%     case 
% self.cells = permute(self.cells,permarray);

