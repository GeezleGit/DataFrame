function DF = df_permuteData(DF,permArray)
    for i = 1:numel(DF.cells)
        DF.cells{i} = permute(DF.cells{i},permArray);
    end
end

