function DF = df_log10Data(DF)
    nDF = length(DF);
    for iDF = 1:nDF
        nGrps = numel(DF(iDF).cells);
        for iGrp = 1:nGrps
            DF(iDF).cells{iGrp} = log10(DF(iDF).cells{iGrp});
        end
    end
end

