function varargout = df_normData(DF,Option,varargin)
% Perform various normalisation operations on all data cells
%
% ... = df_normData(DF,'MAXTOTAL')
% ... = df_normData(DF,'RATIO',divisor)
% ... = df_normData(DF,'SUBSTRACT',substractor)
% ... = df_normData(DF,'SUBSTRACTFROM',substractor)
%
% ... = df_normData(DF,'MICHELSON',Factor,Level)


nDF = length(DF);
for iDF = 1:nDF
    switch Option
        case 'MAXTOTAL'
            nGrps = numel(DF(iDF).cells);
            CatDim = df_celldim(DF(iDF));
            m = max(cat(CatDim,DF(iDF).cells{:}),[],CatDim);
            m = max(m(:));
            for iGrp = 1:nGrps
                DF(iDF).cells{iGrp} = DF(iDF).cells{iGrp}./m;
            end
            varargout{2}(iDF,1) = m;
        case 'RATIO'
            nGrps = numel(DF(iDF).cells);
            m = varargin{1}(iDF);
            for iGrp = 1:nGrps
                DF(iDF).cells{iGrp} = DF(iDF).cells{iGrp}./m;
            end
            varargout{2}(iDF,1) = m;
        case 'INVERSERATIO'
            nGrps = numel(DF(iDF).cells);
            m = varargin{1}(iDF);
            for iGrp = 1:nGrps
                DF(iDF).cells{iGrp} = m./DF(iDF).cells{iGrp};
            end
            varargout{2}(iDF,1) = m;
        case 'SUBSTRACT'
            nGrps = numel(DF(iDF).cells);
            m = varargin{1}(iDF);
            for iGrp = 1:nGrps
                DF(iDF).cells{iGrp} = DF(iDF).cells{iGrp}-m;
            end
            varargout{2}(iDF,1) = m;
        case 'SUBSTRACTFROM'
            nGrps = numel(DF(iDF).cells);
            m = varargin{1}(iDF);
            for iGrp = 1:nGrps
                DF(iDF).cells{iGrp} = m-DF(iDF).cells{iGrp};
            end
            varargout{2}(iDF,1) = m;
            
        case 'MICHELSON'
            nGrps = numel(DF(iDF).cells);
            m = varargin{1}(iDF);
            for iGrp = 1:nGrps
                DF(iDF).cells{iGrp} = (DF(iDF).cells{iGrp}-m)/(DF(iDF).cells{iGrp}+m);
            end
            varargout{2}(iDF,1) = m;
        case 'MICHELSONinverse'
            nGrps = numel(DF(iDF).cells);
            m = varargin{1}(iDF);
            for iGrp = 1:nGrps
                DF(iDF).cells{iGrp} = (m-DF(iDF).cells{iGrp})/(m+DF(iDF).cells{iGrp});
            end
            varargout{2}(iDF,1) = m;
            
        case 'Z-SCORE'
            nGrps = numel(DF(iDF).cells);
            m = varargin{1}(iDF);
            s = varargin{2}(iDF);
            for iGrp = 1:nGrps
                DF(iDF).cells{iGrp} = (DF(iDF).cells{iGrp} - m ) ./s;
            end
            
    end
end
varargout{1} = DF;

