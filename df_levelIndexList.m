function subPairs = df_levelIndexList(self, comFac, comLevel, grpFac, grpLevel)

% Extracts levels (as indices for self.cells) as a list
% 
% subPairs = df_levelIndexList(levelNum, comFac, comLevel, grpFac, grpLevel)
%
% INPUT
% =====
%
% comFac, comLevel ... define the factors and levels for the
%                      comparison-pairs
% grpFac, grpLevel ... define the factors and levels for grouping
%                      if no grouping is defined, non-comparison-factors
%                      are set to zero.
%
% Output
% ======
%
% subPairs ... numerical array containing indices to self.cells
%              [1 x nFactor x nComparisons]
%
% see df_levelIndexPairs

levelNum = size(self.cells);
nFac = length(levelNum);
n = prod(levelNum);

subList = cell(1,nFac);
[subList{:}] = ind2sub(levelNum,1:n);
subList = cat(1,subList{:});
subList = permute(subList,[3 1 2]);

if nargin==1 || isempty(comFac);return;end

notComFac = setxor([1:nFac],comFac);


comLevNum = cellfun('length',comLevel);
if numel(comLevNum)==1;comLevNum = [comLevNum 1];end
grpLevNum = cellfun('length',grpLevel);
if numel(grpLevNum)==1;grpLevNum = [grpLevNum 1];end



% remove levels that are not in the com list
isWanted = false(n,1);
for iComFac = 1:length(comFac)
    isWanted(ismember(subList(1,comFac(iComFac),:), comLevel{iComFac})) = true;
end
subList = subList(1,:,isWanted);

% make the group array
n = size(subList,3);
subPairs = cell(grpLevNum);
grpIndex = cell(1,length(grpFac));
for iGrp=1:prod(grpLevNum)
    [grpIndex{:}] = ind2sub(grpLevNum,iGrp);
    isWanted = true(n,1);
    for iGrpFac = 1:length(grpFac)
        isWanted = isWanted & permute(subList(1,grpFac(iGrpFac),:) == grpLevel{iGrpFac}(grpIndex{iGrpFac}),[3 1 2]);
    end
    subPairs{iGrp} = subList(1,:,isWanted);
end

