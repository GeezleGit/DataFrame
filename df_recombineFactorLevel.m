function DF = df_recombineFactorLevel(DF,cFac,cLevel,NewLevelName,catdim)

% merge/remove/sort levels of a given factor
%
% Usage
% =====
%
% DF = recombineFactorLevel(DF,cFac,cLevel,NewLevelName,catdim)
%
% Input
% =====
%
% cFac ........... 'Factor'
% cLevel ......... { [Level1 Level2] ... [Leveln1 Leveln2] }
% NewLevelName ... { LevelTerm1 ... LevelTermN }
% catdim ......... dimension of concatenation within data cells

nDF = length(DF);
for iDF = 1:nDF
    [nFac,nLev,nEl] = df_getsize(DF(iDF));
    iFac = df_findFactor(DF(iDF),cFac);
    nMergeGrps = length(cLevel);
    
    % find removals first
    LevelToRemove = ~ismember(DF(iDF).level{iFac},cat(2,cLevel{:}));
    if any(LevelToRemove)
        DF(iDF) = df_removeFactorLevel(DF(iDF),cFac,DF(iDF).level{iFac}(LevelToRemove));
    end
    
    % convert levelnames to strings to make safe for mixed level format
    isNumExist = isnumeric(DF(iDF).level{iFac});
    isCellExist = iscell(DF(iDF).level{iFac});
    isNumGrps = cellfun('isclass',cLevel,'double');
    isNumNew = isnumeric(NewLevelName);
    isCharCellNew = iscell(NewLevelName) && all(cellfun('isclass',NewLevelName,'char'));
    if isNumExist && ~isNumNew
        
        DF(iDF).level{iFac} = num2cellstr(DF(iDF).level{iFac},'');
        for iMergeGrp = 1:nMergeGrps
            cLevel{iMergeGrp} = num2cellstr(cLevel{iMergeGrp},'');
        end
        
    elseif isNumExist && isNumNew
        % don't do anything
    elseif isCellExist && isCharCellNew
        % don't do anything
    else
        error('Discovered a case of mixed levelnames that is not covered yet!');
    end
    
    % now merge remaining
    for iMergeGrp = 1:nMergeGrps
        if isempty(cLevel{iMergeGrp})
            DF(iDF) = df_addFactorLevel(DF(iDF),cFac,NewLevelName(iMergeGrp));
        else
            DF(iDF) = df_mergeFactorLevel(DF(iDF),cFac,cLevel{iMergeGrp},NewLevelName(iMergeGrp),catdim);
        end
    end
end

function y = num2cellstr(x,prec)
y = cell(size(x));
if isempty(prec)
    % find smallest difference
%     n = numel(x);
%     [X,Y] = meshgrid(x(:),x(:));
%     dx = abs(Y-X);
%     dx(sub2ind([n n],1:n,1:n)) = NaN;% remove zeros differences on diagonal 
%     dmin = min(dx(:));
    for i = 1:numel(x)
        y{i} = num2str(x(i));
    end
    
else
    for i = 1:numel(x)
        y{i} = sprintf(prec,x(i));
    end
end
