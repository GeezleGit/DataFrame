function self = df_Array2Series(self,seriesLabel,seriesParam,seriesDim)
% Merge multiple DataFrames of identical factor/levels to a data series.
%
% Parameters
% ----------
% seriesLabel: char
% seriesParam: double or cell
% seriesDim: int
%
% Examples
% --------
% ::
%
%     self = df_Array2Series(self,seriesLabel,seriesParam,seriesDim)
%


nArr = length(self);

if any(strcmp(self(1).serieslabel,seriesLabel))
    error('Serieslabel is already existing!');
end

% current DataFrame info
[sampD,seriesD] = df_celldim(self(1));

if strcmpi(seriesLabel,'SAMPLES')
    seriesDim = sampD;
else
    
    iSeries = length(self(1).seriesdim)+1;
    if nargin<4 || isempty(seriesDim)
        seriesDim = max([sampD,seriesD])+1;
    end
end

nCell = numel(self(1).cells);

if ~isempty(self(1).userdata)
    self(1).userdata = {self(1).userdata};
end

% concatenation
for iArr=2:nArr
    for iCell = 1:nCell
        if isempty(self(1).cells{iCell}) && ~isempty(self(iArr).cells{iCell})
            self(1).cells{iCell} = cat(seriesDim,NaN,self(iArr).cells{iCell});
        elseif ~isempty(self(1).cells{iCell}) && isempty(self(iArr).cells{iCell})
            self(1).cells{iCell} = cat(seriesDim,self(1).cells{iCell},NaN);
        elseif isempty(self(1).cells{iCell}) && isempty(self(iArr).cells{iCell})
            self(1).cells{iCell} = cat(seriesDim,NaN,NaN);
        else
            self(1).cells{iCell} = cat(seriesDim,self(1).cells{iCell},self(iArr).cells{iCell});
        end
        self(iArr).cells{iCell} = [];% clear data
    end
    if ~isempty(self(iArr).userdata)
        self(1).userdata{iArr} = self(iArr).userdata;
    end
end

% create output
self = self(1);

if ~strcmpi(seriesLabel,'SAMPLES')
    self.serieslabel{iSeries} = seriesLabel;
    self.seriesparam{iSeries} = seriesParam;
    self.seriesdim(iSeries) = seriesDim;
end
