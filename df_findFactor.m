function FacNr = df_findFactor(DF,varargin)
% DF_FINDFACTOR
%
% Example
% -------
% ::
% 
%     FacNr = df_findFactor(DF,'Fac1', ... ,'FacN')


if length(varargin)==1&&iscell(varargin{1})
    currFactors = varargin{1};
else
    currFactors = varargin;
end
nFac = length(currFactors);

FacNr = zeros(1,nFac).*NaN;
for iFac = 1:nFac
    FoundFac = strcmp(currFactors{iFac},DF.factor);
    if any(FoundFac);
        FacNr(iFac) = find(FoundFac);
    end
end

