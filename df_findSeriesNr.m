function d = df_findSeriesNr(self,seriesLabel)
if ischar(seriesLabel)
    d  = find(strcmp(seriesLabel,self.serieslabel));
elseif iscell(seriesLabel)
    for i=1:length(seriesLabel)
        x = find(strcmp(seriesLabel{i},self.serieslabel));
        if isempty(x)
            d(i) = NaN;
        else
            d(i) = x;
        end
    end
end
    



