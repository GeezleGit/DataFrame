function m = df_max(self,mode,varargin)
% get maximum value of data cells
% m = df_max(DF,mode,Fac,Lev)
%
% mode ... 'series'  get maximum of series data
%          'samples' get maximum of samples data
%          'total'   get total maximum
%          [dim]     get maximum along dimension

if nargin<2
    mode = 'samples';
end

if ischar(mode) && strcmp(mode,'samples')
    [sampDim,seriesDim] = df_celldim(self);
    opDim = sampDim;
elseif ischar(mode) && strcmp(mode,'series')
    [sampDim,seriesDim] = df_celldim(self);
    opDim = seriesDim;
elseif isnumeric(mode)
    opDim = mode;
elseif ischar(mode) && strcmp(mode,'total')
    opDim = [];
else
    opDim = [];
end

n = numel(self.cells);

if length(opDim) == 1
    m = max(cat(opDim,self.cells{:}),[],opDim);
elseif isempty(opDim)
    m = zeros(size(self.cells)).*NaN;
    for i=1:n
        if ~isempty(self.cells{i})
            m(i) = max(self.cells{i}(:));
        end
    end
    m = max(m(:));
elseif length(opDim) > 1
    m = cell(size(self.cells));
    for i=1:n
        m{i} = self.cells{i};
        for j=1:length(opDim)
            m{i} = max(m{i},[],opDim(j));
        end
    end
    finalDim = max(opDim);
    m = cat(finalDim,m{:});
    m = max(m,[],finalDim);
end
    
    

