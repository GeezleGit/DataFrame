function DF1 = df_substract(DF1,DF2)
N = numel(DF1);
for I=1:N
    n = numel(DF1(I).cells);
    for i=1:n
        DF1(I).cells{i} = DF1(I).cells{i}-DF2(I).cells{i};
    end
end