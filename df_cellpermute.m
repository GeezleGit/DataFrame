function DF = df_cellpermute(DF,i)
    % permutes arrays in cells
    % DF = cellpermute(DF,i)
    % i ... permutation vector
    n = numel(DF.cells);
    for j=1:n
        if isempty(DF.cells{j});continue;end
        DF.cells{j} = permute(DF.cells{j},i);
    end
end
