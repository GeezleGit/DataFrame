function DF = df_addFactor(DF,FacName,FacLevel,FacPos)
% DF_ADDFACTOR adds a factor, existing data will constitutes first level
%
% Example
% -------
% ::
% 
%     DF = addFactor(DF,'FacName',FacLevel,FacPos)
%     DF = addFactor(DF,{'FacName1','FacName2'},{FacLevel1 FacLevel2},FacPos)
%

if ischar(FacName)
    nAdd = 1;
    FacName = {FacName};   
    if ischar(FacLevel) 
        FacLevel = {{FacLevel}};
    elseif  isnumeric(FacLevel) || iscell(FacLevel)
        FacLevel = {FacLevel};
    end       
elseif iscell(FacName)
    nAdd = length(FacName);
    if ~iscell(FacLevel) && ~length(FacLevel)==nAdd
        error('check input arrays');
    end
end

nFac = length(DF.factor);

if nargin<4
    FacPos = nFac+[1:nAdd];% safe for adding an array of factors
elseif nargin>=4
    error('Currently only appending is allowed!');
end

DF.factor(FacPos) = FacName;
DF.level(FacPos) = FacLevel;

levNum = cellfun('length',DF.level);
repNum = ones(1,max([2 length(DF.level)]));
repNum(FacPos) = levNum(FacPos);
DF.cells = repmat(DF.cells,repNum);