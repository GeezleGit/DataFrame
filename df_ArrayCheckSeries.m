function [foundPar, allPar, DFarr] = df_ArrayCheckSeries(DFarr,seriesLabel,minN)

N = numel(DFarr);

iSer = zeros(1,N);
allPar = [];
for i=1:N
    iSer(i) = find(strcmp(seriesLabel, DFarr(i).serieslabel));
    allPar = union(allPar, DFarr(i).seriesparam{iSer(i)}(:));
end

foundPar = false(size(allPar,1),N);
for i = 1:N
    iFoundPar = ismember(allPar, DFarr(i).seriesparam{iSer(i)}(:));
    foundPar(iFoundPar,i) = true;
end

if nargout>2
    isOK = sum(foundPar,2)>=minN;
    parLim = [min(allPar(isOK)) max(allPar(isOK))];
    for i = 1:N
        DFarr(i) = df_extractSeries(DFarr(i),seriesLabel,'BOUNDS',parLim);
    end
end
