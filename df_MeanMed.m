function R = df_MeanMed(DF,d,cell2matFlag)
    % calculates means and medians within cells
    % R = MeanMed(DF,d,cell2matFlag)
    % d .............. dimension to compute means within cells
    % cell2matFlag ... output will be numerical, default is cells 
    % R .............. structure with the fields
    %                  [M,S,SE,MED,P25,P75]
    % 
    
    if nargin<2
        d = 1;
    end
    if nargin<3
        cell2matFlag = true;
    end
    
    R.nLevel = size(DF.cells);
    R.nFactor = length(R.nLevel);
    R.nDims = cellfun('ndims',DF.cells);
    
    R.N = cell(R.nLevel);
    R.M = cell(R.nLevel);
    R.S = cell(R.nLevel);
    R.SE = cell(R.nLevel);
    R.MED = cell(R.nLevel);
    R.P25 = cell(R.nLevel);
    R.P75 = cell(R.nLevel);
    
    for i = 1:prod(R.nLevel)
        [R.M{i},R.S{i},R.SE{i}] = gzlStats.agmean(DF.cells{i},[],d);
        R.N{i} = sum(~isnan((DF.cells{i})),d);
        R.MED{i} = prctile(DF.cells{i},50,d);
        R.P25{i} = prctile(DF.cells{i},25,d);
        R.P75{i} = prctile(DF.cells{i},75,d);
    end
    
    if cell2matFlag
        NoData = cellfun('isempty',DF.cells);
        R.M(NoData) = {NaN};
        R.S(NoData) = {NaN};
        R.SE(NoData) = {NaN};
        R.MED(NoData) = {NaN};
        R.P25(NoData) = {NaN};
        R.P75(NoData) = {NaN};
        R.M = cell2mat(R.M);
        R.S = cell2mat(R.S);
        R.SE = cell2mat(R.SE);
        R.MED = cell2mat(R.MED);
        R.P25 = cell2mat(R.P25);
        R.P75 = cell2mat(R.P75);
    end
end

