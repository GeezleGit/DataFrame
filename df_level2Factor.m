function self = df_level2Factor(self,factorName,levelLabel,newFactorName, oldFactorName)

% Fork levels of a given factor to levels of a new factor
%
% self = df_level2Factor(self,factorName,levelLabel,newFactorName)
%
%

[iLevel,FacNr] = df_findFactorLevel(DF,factorName,levelLabel);

NewFacNr = length(self.factor)+1;

% set factor
self.factor{NewFacNr} = newFactorName;
if nargin>4 && ~isempty(oldFactorName)
    self.factor{FacNr} = newFactorName;
end

% set level
self.level{NewFacNr} = levelLabel;
self.level{FacNr}(iLevel) = [];

% set cells
nNewLevel = length(levelLabel);
oldShape = size(self.cells);
newShape = oldShape;
newShape(FacNr) = oldShape(FacNr)-nNewLevel;
oldShape(NewFacNr) = nNewLevel;

permArr = [FacNr setxor(1:length(oldShape),FacNr)];
self.cells = permute(self.cells, permArr);
newCells = self.cells(iLevel,:);
self.cells(iLevel,:) = [];
self.cells = cat(3,self.cells, newCells);



