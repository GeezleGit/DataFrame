function self = df_permuteCells(self,permArray)
%% DF_PERMUTECELLS apply permute function to all arrays in cells
%
% self = df_permuteCells(self,[d1, d2, d3, ... dn])
%
% self = df_permuteCells(self,':')
%

Ns = numel(self);

if isnumeric(permArray)
    for Is = 1:Ns
        ns = numel(self(Is).cells);
        for is = 1:ns
            self(Is).cells{is} = permute(self(Is).cells{is},permArray);
        end
    end
elseif ischar(permArray) && strcmp(permArray,':')
    for Is = 1:Ns
        ns = numel(self(Is).cells);
        for is = 1:ns
            self(Is).cells{is} = self(Is).cells{is}(:);
        end
    end
end    
    