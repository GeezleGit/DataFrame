function varargout = df_compositemean(MeanSelf,STDSelf,NSelf)

% computes mean across samples/series

nGrps = numel(MeanSelf.cells);
[sampD,seriesD] = df_celldim(MeanSelf);
MeanDim = sampD;

for iGrp = 1:nGrps
    MeanArray = MeanSelf.cells{iGrp};
    STDArray = STDSelf.cells{iGrp};
    
    csz = size(MeanArray);
    csz(MeanDim) = 1;
    
    NumArray = NSelf.cells{iGrp}(:);
    
    if isempty(NSelf.cells{iGrp})
        MeanSelf.cells(iGrp) = {[]};
        STDSelf.cells(iGrp) = {[]};
    else
        NumArray = shiftdim(NumArray,1-MeanDim);
        NumArray = repmat(NumArray,csz);
        [MeanSelf.cells{iGrp},STDSelf.cells{iGrp},GN] = compositemean(MeanArray,STDArray,NumArray,1,MeanDim);
    end
end

% construct output
if nargout==2
    varargout{1} = MeanSelf(1);
    varargout{2} = STDSelf(1);
elseif nargout==1
    varargout{1} = MeanSelf(1);
end






function [GM,GS,GN] = compositemean(Means,SDevs,Ns,stdOption,grpDim)

% compute grand mean and std from composite/group means+std
% [GM,GS,GN] = compositemean(Means,SDevs,Ns,stdOption,grpDim)
%
% Means .......
% SDevs .......
% Ns ..........
% stdOption ...
% grpDim ......
%
% GM ...
% GS ...
% GN ...
%
% adapted from
% http://www.burtonsys.com/climate/composite_standard_deviations.html

nGrps = size(Means,grpDim);

% repmat array to compensate for dim-reduction due to averaging
repArr = ones(1,length(size(Means)));
repArr(grpDim) = nGrps;

GN = sum(Ns,grpDim);

if stdOption==1
    degFs = Ns;
    GdegF = GN;
else
    degFs = Ns-1;
    GdegF = GN-1;
end

%% compute
GM = sum(Means.*Ns,grpDim)./GN;

Vars = SDevs.^2;
ErrSSqs = Vars.*degFs;
GErrSSq = sum(ErrSSqs,grpDim);

SSqs = ((Means-repmat(GM,repArr)).^2).*Ns;
GSSq = sum(SSqs,grpDim);

GV = (GErrSSq + GSSq) ./ GdegF;
GS = sqrt(GV);
