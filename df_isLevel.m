function [isLev,FNr,LNr] = df_isLevel(DF,FacName,FacLevel)
    nLev = length(FacLevel);
    isLev = false(1,nLev);
    FNr = NaN;
    LNr = ones(1,nLev).*NaN;
    FNr = find(strcmp(FacName,DF.factor));
    if isempty(FNr);return;end
    [isLev,FNr] = ismember(FacLevel,DF.level{FNr});
end
