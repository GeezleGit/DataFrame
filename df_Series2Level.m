function self = df_Series2Level(self,SeriesName)
% DF_SERIES2LEVEL Transform data series to level (unnest data).
%
% Example
% -------
% ::
% 
%     self = df_Series2Level(self,SeriesName)
%

if strcmpi('SAMPLES',SeriesName)
    iSer = false(1,length(self.serieslabel)+1);
    iSer(end) = true;
    [sampD,seriesD] = df_celldim(self);
    nSamples = cellfun('size',self.cells(:),sampD);
    nSamples = unique(nSamples);
    if length(nSamples)>1
        error('Data cells have to have the same number of samples!!!');
    end
    self.seriesdim(1,find(iSer)) = sampD;
    self.serieslabel{1,find(iSer)} = 'SAMPLES';
    self.seriesparam{1,find(iSer)} = [1:nSamples];
else
    iSer = strcmp(self.serieslabel,SeriesName);
end

dSer = self.seriesdim(iSer);
nSer = length(self.seriesparam{iSer});

nFac = length(self.factor);
nLev = size(self.cells);
iNewFac = nFac+1;
cellindex = cell(1,nFac+1);

for iGrp = 1:prod(nLev)
    [cellindex{:}] = ind2sub(nLev,iGrp);
    
    % extract matrix from current group cell 
    cellindex{iNewFac} = 1;
    X = self.cells{cellindex{:}};
    
    % transform matrix array to cell array, each series element will be in one cell 
    transformArray = num2cell(size(X));
    transformArray{dSer} = ones(1,transformArray{dSer});
    X = mat2cell(X,transformArray{:});

    % permute new data cells to fit into DataFrame
    permNumDim = max([nFac+1 max(self.seriesdim)]);
    permarray = circshift(1:permNumDim,[1 iNewFac-dSer]);
    X = permute(X,permarray);
    
    % insert cells into DataFrame cell array
    cellindex{iNewFac} = 1:nSer;
    self.cells(cellindex{:}) = X;
    
end

% clean up series information
self.factor(iNewFac) = self.serieslabel(iSer);
self.level(iNewFac) = self.seriesparam(iSer);
self = df_removeSeriesInfo(self,iSer);

