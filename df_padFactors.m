function [DF1,DF2] = df_padFactors(DF1,DF2)

cCellPair = {DF1.cells DF2.cells};
nCells = length(cCellPair);

% check array dimensions
nd = cellfun('ndims', cCellPair);
[lowDimNr, lowDimCellNr] = min(nd);
[highDimNr, highDimCellNr] = max(nd);

% check array sizes
sz = ones(nCells,highDimNr);
for iCell=1:nCells
    sz(iCell,1:nd(iCell)) = size(cCellPair{iCell});
end
repDim = max(sz,[],1);

% replicate arrays
repMatArr = ones(nCells,highDimNr);
for iCell=1:nCells
    if any(sz(iCell,:)~=repDim & sz(iCell,:)~=1)
        error('Array dimension in cell-pairs must pair equal or singleton!');
    end
    cDimIndex = sz(iCell,:)==1;
    repMatArr(iCell,cDimIndex) = repDim(cDimIndex);
    cCellPair{iCell} = repmat(cCellPair{iCell},repMatArr(iCell,:));
    
    padFactorNr{iCell} = find(cDimIndex);
    padFactorLabel{iCell} = cell(1,length(padFactorNr{iCell}));
    padFactorLevelLabel{iCell} = cell(1,length(padFactorNr{iCell}));
    for iPadFac = padFactorNr{iCell}
        padFactorLabel{iCell}{iPadFac} = sprintf('padded_Factor-%u',iPadFac);
        padFactorLevelLabel{iCell}{iPadFac} = 1:repMatArr(iCell,iPadFac);
    end
end

if ~isempty(padFactorNr{1})
    DF1.factor(padFactorNr{1}) = padFactorLabel{1}(padFactorNr{1});
    DF1.level(padFactorNr{1}) = padFactorLevelLabel{1}(padFactorNr{1});
end
DF1.cells = cCellPair{1};

if ~isempty(padFactorNr{2})
    DF2.factor(padFactorNr{2}) = padFactorLabel{2}(padFactorNr{2});
    DF2.level(padFactorNr{2}) = padFactorLevelLabel{2}(padFactorNr{2});
end
DF2.cells = cCellPair{2};
