function [sampD,seriesD] = df_celldim(DF)
% DF_CELLDIM returns matrix dimension of samples
%
% Example
% -------
% ::
% 
%     [sampD,seriesD] = df_celldim(DF)

nd = cellfun('ndims',DF.cells);

if ~isempty(DF.seriesdim) && all(DF.seriesdim~=0)
    seriesD = DF.seriesdim;
    sampD = setdiff([1:max(nd(:))],DF.seriesdim);
    if isempty(sampD)
        sampD = max(seriesD)+1;
    elseif length(sampD)>1
        sampD = sort(sampD);
        for iD = 1:length(sampD)
            sampDNum(iD) = max(cellfun('size',DF.cells(:),sampD(iD)));
        end
        if all(sampDNum==1)
            sampD = sampD(1);
        elseif any(sampDNum>1)
            sampD = sampD(find(sampDNum>1,1,'first'));
        else
            sampD = NaN;
        end
            
    end
    
else
    if any(nd(:)>2)
        error('When no series information present only 2d arrays in data cells please!');
        sampD = NaN;
        seriesD = NaN;
        return;
    end
    d1 = cellfun('size',DF.cells,1);
    d2 = cellfun('size',DF.cells,2);
    
    if all((d1(:)==1 & d2(:)==1) | d1(:)==0 | d2(:)==0)
        % cells contain scalars or empty
        sampD = 1;
        seriesD = 2;
    elseif all( (d1(:)>=1 & d2(:)==1)  | d1(:)==0 | d2(:)==0)
        sampD = 1;
        seriesD = 2;
    elseif all( (d2(:)>=1 & d1(:)==1)  | d1(:)==0 | d2(:)==0)
        sampD = 2;
        seriesD = 1;
    else
        sampD = 0;
        seriesD = 0;
    end
end
