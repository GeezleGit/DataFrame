function n = df_cellnum(df)
sz = size(df.cells);
nl = cellfun('length',df.level);
n = nl;