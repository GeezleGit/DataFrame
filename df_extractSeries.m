function self = df_extractSeries(self,seriesLabel,extractType,extractParam)
% DF_EXTRACTSERIES Extract datapointa of each level, defined by series parameter
%
% Parameters
% ----------
% seriesLabel: char 
% extractType: char
%     - ``'BOUNDS'`` include bounds
%     - ``'WINDOW'`` exclude bounds
%     - ``'BOUNDWINDOW'``
%     - ``'WINDOWBOUND'``
%     - ``'EXACT'`` match array of values
% extractParam: double
%


nDF = numel(self);
for iDF = 1:nDF
    if strcmp(seriesLabel,'SAMPLES') || isempty(seriesLabel)
        seriesLabel = 'SAMPLES';
        sD = df_celldim(self(iDF));
        sP = [];
    else
        iSeries = strcmp(self(iDF).serieslabel,seriesLabel);
        sD = self(iDF).seriesdim(iSeries);
        sP = self(iDF).seriesparam{iSeries};
    end
    
    switch extractType
        case 'INDEX'
            i = extractParam;
            extractArgs = {i,sD};
        case 'BOUNDS'
            i = sP>=extractParam(1) & sP<=extractParam(2);
            extractArgs = {i,sD};
        case 'WINDOW'
            i = sP>extractParam(1) & sP<extractParam(2);
            extractArgs = {i,sD};
        case 'BOUNDWINDOW'
            i = sP>=extractParam(1) & sP<extractParam(2);
            extractArgs = {i,sD};
        case 'WINDOWBOUND'
            i = sP>extractParam(1) & sP<=extractParam(2);
            extractArgs = {i,sD};
        case 'EXACT'
            if isnumeric(sP)
                i = ismember(sP,extractParam);
                extractArgs = {i,sD};
            elseif iscell(sP) || ischar(extractParam)
                i = strcmp(extractParam,sP);
                extractArgs = {i,sD};
            end
        case 'PICK'
            numPar = length(extractParam);
            i = zeros(1,numPar);
            if isnumeric(sP)
                for j = 1:numPar
                    i(j) = find(sP==extractParam(j));
                end
            elseif iscell(sP)
                for j = 1:numPar
                    i(j) = find(strcmp(extractParam(j),sP));
                end
            end
            extractArgs = {i,sD};
    end
    
    self(iDF) = df_extractDataPoints(self(iDF),extractArgs{:});
    
    if ~isempty(sP)
        self(iDF).seriesparam{iSeries} = sP(i);
    end
end


