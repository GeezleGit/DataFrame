function d = df_getSeriesDim(self,label)
% DF_GETSERIESDIM returns the dimension of a series within datacell
%
% Example
% -------
% ::
% 
%     d = df_getSeriesDim(self,'seriesLabel')
%     d = df_getSeriesDim(self,{'seriesLabel1' 'seriesLabel2' ...})
%     d = df_getSeriesDim(self,[seriesNr seriesNr ...])
% 

if ischar(label)
    i = strcmp(self.serieslabel,label);
    d = self.seriesdim(i);
elseif iscell(label)
    for k = 1:length(label)
        i = strcmp(self.serieslabel,label{k});
        d(1,k) = self.seriesdim(i);
    end
elseif isnumeric(label)
    for k = 1:length(label)
        d(1,k) = self.seriesdim(label(k));
    end
end    
    