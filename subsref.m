function value = subsref(s,subscript)

% Subsref for DataFrame Objects
%
% function value= subsref(s,subscript)
%

if length(subscript)==1 && strcmp(subscript(1).type,'()')
    % this is to extract al_spk
   value = s(subscript(1).subs{:});
elseif length(subscript)==1 && strcmp(subscript(1).type,'.')
   value = s.(char(subscript(1).subs));
elseif length(subscript)==2 && strcmp(subscript(1).type,'.')
    switch subscript(2).type
        case '{}'
            value = s.(char(subscript(1).subs)){subscript(2).subs{1}};
        case '()'
            value = s.(char(subscript(1).subs))(subscript(2).subs{1});
        otherwise
            error('Can''t reference values to fields "%s"!',char(subscript(1).subs));
    end
else
    error('Can''t reference values to fields!');
end
