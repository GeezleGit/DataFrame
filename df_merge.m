function [DF,FacNr] = df_merge(DF,DF2,FacName,FacLevel)
% Use 1);
% DF = merge(DF1,DF2,FacName,FacLevel)
% merge two Dataframes, source DataFrame becomes a level of a
% new factor
%
% use 2)
% DF = merge(DF(1:n))
% merge an array of data frames, merge cell content


if nargin>2
    DF = df_addFactor(DF,FacName,FacLevel(1));
    DF2 = df_addFactor(DF2,FacName,FacLevel(2));
    DF(2) = DF2;
end

FacNr = [];
n = length(DF);
x = DF(1);
[sampD,seriesD] = df_celldim(x);
xLevNum = size(x.cells);
%for i=1:n ----------> this was a bug until 19/5/2014
for i=2:n
    
    %             % merge fields (this actually the old code until 19/5/2014)
    %             cLevNum = size(DF(i).cells);
    %             if ~all(xLevNum==cLevNum)
    %                 error('cells inconsistent');
    %             end
    %             for j=1:prod(cLevNum)
    %                 x.cells{j} = cat(sampD,x.cells{j},DF(i).cells{j});
    %             end
    
    % check for levels
    xFacNum = length(x.factor);
    for ixFac = 1:xFacNum
        isFac = strcmp(x.factor{ixFac},DF(i).factor);
        if any(isFac)
            % add new levels to x
            [isLev,xLevNr] = ismember(DF(i).level{isFac},x.level{ixFac});
            nLevPad = sum(~isLev);
            if nLevPad>0
                x = df_addFactorLevel(x,x.factor{ixFac},DF(i).level{isFac}(~isLev));
            end
            % sort levels
            if isnumeric(x.level{ixFac})
                [dummy,cLevelSortIndex] = sort(x.level{ixFac});
                x = df_sortFactorLevel(x,x.factor{ixFac},cLevelSortIndex,false);
            end
        end
    end
    
    % check for factors
    % this is untested so far
    [isFac,xFacNr] = ismember(DF(i).factor,x.factor);
    nFacPad = sum(~isFac);
    if nFacPad>0
        x = df_addFactor(x,DF(i).factor(~isFac),DF(i).level(~isFac));
    end
    
    % merge fields
    xFacNum = length(x.factor);
    xCellSize = size(x.cells);
    xCellSub = cell(1,max([xFacNum 2]));
    cxFactor = cell(1,xFacNum);
    for j=1:numel(x.cells)
        [xCellSub{:}] = ind2sub(xCellSize,j);
        cxFactor(:) = {[]};
        cxLevel = [];
        for k=1:xFacNum
            cxFactor(k) = x.factor(k);
            cxLevel{k} = x.level{k}(xCellSub{k});
        end
        dummy = [cxFactor;cxLevel];
        jj = df_findData(DF(i),dummy{:});
        if ~any(cellfun('isempty',jj))
            x.cells{j} = cat(sampD,x.cells{j},DF(i).cells{jj{:}});
        end
    end
end
DF = x;
end

