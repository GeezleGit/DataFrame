function DF = df_levelReplace(DF,FacName,FacLevel,NewLevel)

% Replaces level codes
% DF = df_levelReplace(DF,FacName,FacLevel,NewLevel)
%
% complete level replacement
% DF = df_levelReplace(DF,FacName,[],NewLevel)

FNr = find(strcmp(FacName,DF.factor));
if isempty(FNr);return;end

if isempty(FacLevel)
    % complete replacement
    if length(DF.level{FNr})~=length(NewLevel)
        error('New level array has to be of the same length!');
    end
    DF.level{FNr} = NewLevel;
else
    nLev = length(FacLevel);
    isLev = false(1,nLev);
    [isLev,levNr] = ismember(FacLevel,DF.level{FNr});
    DF.level{FNr}(levNr) = NewLevel;
end
