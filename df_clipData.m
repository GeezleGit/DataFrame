function DF = df_clipData(DF,clipLim)
    nDF = length(DF);
    for iDF = 1:nDF
        nGrps = numel(DF(iDF).cells);
        for iGrp = 1:nGrps
            DF(iDF).cells{iGrp}(DF(iDF).cells{iGrp}<clipLim(1)) = clipLim(1);
            DF(iDF).cells{iGrp}(DF(iDF).cells{iGrp}>clipLim(2)) = clipLim(2);
        end
    end
end

