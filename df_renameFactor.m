function self = df_renameFactor(self,label,FacLevel,NewLevel)

% self = df_renameFactor(self,{'oldFactorLabel','newFactorLabel'},FacLevel,NewLevel)

N = numel(self);
for i = 1:N
    
    % replace label
    if iscell(label) && numel(label)==2
        facNr = find(strcmp(label{1},self(i).factor));
        if isempty(facNr);return;end
        self(i).factor{facNr} = label{2};
    else
        facNr = find(strcmp(label,self(i).factor));
    end
    
    % replace level-label

    if isempty(FacLevel)
        % complete replacement
        if length(self(i).level{facNr})~=length(NewLevel)
            error('New level array has to be of the same length!');
        end
        self(i).level{facNr} = NewLevel;
    else
        nLev = length(FacLevel);
        isLev = false(1,nLev);
        [isLev,levNr] = ismember(FacLevel,self(i).level{facNr});
        if sum(isLev)==3
            disp('hold')
        end
        NewLevel = NewLevel(levNr(isLev));
        self(i).level{facNr} = NewLevel;
    end
end
