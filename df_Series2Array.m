function [newArray,sP] = df_Series2Array(self,seriesLabel)

if nargin<2
    [sD,nPar] = df_checkSamples(self);
    newArray = DataFrame();
    for ip = 1:nPar
        newArray(ip) = df_extractSeries(self,'SAMPLES','INDEX',ip);
    end
    
else
    iSeries = strcmp(self.serieslabel,seriesLabel);
    sD = self.seriesdim(iSeries);
    sP = self.seriesparam{iSeries};
    newArray = DataFrame();
    for ip = 1:length(sP)
        newArray(ip) = df_extractSeries(self,seriesLabel,'EXACT',sP(ip));
        newArray(ip) = df_removeSeriesInfo(newArray(ip),iSeries);
    end
end



