function [R,ANOVAdata,ANOVAgroup] = df_anovan(DF,varargin)
%% DF_ANOVAN
%
% [R,ANOVAdata,ANOVAgroup] = df_anovan(DF,varargin)
%
% varargin ... anovan.m input arguments
%
% R ............ results structure with fields:
%                nLevel   -
%                nFactor  -
%                nSample  -
%                nTotal   -
%                p        - 
%                table    -
%                stats    -
% ANOVAdata .... the data array fed into the anovan function
% ANOVAgroup ... the group array fed into the anovan function


if nargin<4
    ANOVANpar = {};
end

[sampD,seriesD] = df_celldim(DF);
for i=1:length(seriesD)
    if any(cellfun('size',DF.cells,seriesD(i))>1)
        error('Anovan is not implemented for data series yet!!!');
    end
end

%% analyse data cell array
R.nLevel = size(DF.cells);
% safe for 1-factor
if length(R.nLevel)==2 && R.nLevel(end)==1
    R.nLevel(end)=[];
end
R.nFactor = length(R.nLevel);
R.nSample = cellfun('size',DF.cells,sampD);
R.nTotal = sum(R.nSample(:));
R.p = [];
R.table = [];
R.stats = [];

%% allocate ANOVA arrays
ANOVAdata = ones(R.nTotal,1);
ANOVAgroup = cell(1,R.nFactor);
for i = 1:R.nFactor
    if isnumeric(DF.level{i})
        ANOVAgroup{i} = ones(R.nTotal,1).*NaN;
    elseif iscell(DF.level{i})
        ANOVAgroup{i} = cell(R.nTotal,1);
    end
end

ANOVAdata = cat(1,DF.cells{:});
CellNs = R.nSample(:);
cSub = cell(1,R.nFactor);
pointer = 1;
for i = 1:length(CellNs)
    [cSub{:}] = ind2sub(R.nLevel,i);
    for k = 1:R.nFactor
        ANOVAgroup{k}(pointer:pointer+CellNs(i)-1) = DF.level{k}(cSub{k});
    end
    pointer = pointer+CellNs(i);
end

%% DO ANOVAN
if isempty(isnan(ANOVAdata)) || ~all(isnan(ANOVAdata(:)))
    [R.p,R.table,R.stats] = anovan(ANOVAdata,ANOVAgroup, ...
        'sstype',3, ... default
        'display','off', ...
        'alpha',0.05, ...
        'model','full', ...
        'varnames',DF.factor(1:R.nFactor), ...
        varargin{:});
end

