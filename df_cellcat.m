function self = df_cellcat(self,seriesID)

for iGrp = 1:numel(self.cells)
    self.cells{iGrp} = self.cells{iGrp}(:);
end