function self = df_setCells(self,x,i)
% set data container cells of a DataFrame object
% 
% Parameters
% ----------
% self: DataFrame
% x: cell
% i: int
%     index for self.cells
%
% Returns
% -------
% self: DataFrame
%
% Examples
% --------
% ::
%
%     self = df_setCells(self,x,i)
%

if nargin<3
    self.cells = x;
else
    self.cells(i) = x;
end