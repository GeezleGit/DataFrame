function DF = df_extractDataPoints(DF,varargin)
% extract elements of arrays in cells in one dimension
%
% DF = df_extractDataPoints(DF,i,d)
% i ... index (logical or numerical)
% d ... dimension
%
% DF = df_extractDataPoints(DF,indexDataFrame)

n = numel(DF.cells);

if length(varargin)==1 && isa(varargin{1},'DataFrame')
    indDF = varargin{1};
    for j=1:n
        DF.cells{j} = DF.cells{j}(indDF.cells{j});
    end
elseif length(varargin)==2
    i = varargin{1};
    d = varargin{2};
    if islogical(i)
        ni = sum(i);
    else
        ni = length(i);
    end
    for j=1:n
        if isempty(DF.cells{j});continue;end

        cSize = size(DF.cells{j});
        nd = ndims(DF.cells{j});

        % permute dimension to first
        iDimPerm = circshift(1:nd,[1 1-d]);
        cSize = circshift(cSize,[1 1-d]);
        DF.cells{j} = permute(DF.cells{j},iDimPerm);

        % take indices and reshape
        DF.cells{j} = DF.cells{j}(i,:);
        cSize(1) = ni;
        DF.cells{j} = reshape(DF.cells{j},cSize);

        % permute to original dimensions
        iDimPerm = circshift(1:nd,[1 d-1]);
        DF.cells{j} = permute(DF.cells{j},iDimPerm);
    end
end
