function DF = df_importAsLevel(DF,FacName,FacLevel,c)
    % import data-cells as a level of a given/existing factor
    % DF = importAsLevel(DF,FacName,FacLevel,c)
    FacNr = findFactor(DF,FacName);
    if isnan(FacNr)
        DF = addFactor(DF,FacName,FacLevel);
    end
    existSize = size(DF.cells);
    importSize  = size(c);
    existSize(FacNr) = 0;
    importSize(FacNr) = 0;
    if ~(length(existSize)==length(importSize)) || ~all(existSize==importSize)
        error('Datacells not consistent => can''t concatenate!');
    end
    DF.cells = cat(FacNr,DF.cells,c);
    if any(ismember(DF.level{FacNr},FacLevel))
        error('Level exists!!');
    end
    DF.level{FacNr} = cat(2,DF.level{FacNr},FacLevel); 
end
