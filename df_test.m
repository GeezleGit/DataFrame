function [p,h,stats] = df_test(DF,FunName,FunInput)
%% DF_TEST test between 2 identical dataFrames
%
% [p,h,stats] = df_test([DF1 DF2],FunName,FunInput)
% DF ........ DataFrame is array of two 
% FunName ... FunInput
%
% use df_getContrastData(DF,cFac,cLevel))

[nFac,nLev]     = df_getsize(DF(1));
[sampD,seriesD] = df_celldim(DF(1));
nSeries = length(seriesD);

nSeriesPar = ones(1,max([sampD,seriesD]));
nSeriesPar(DF(1).seriesdim) = cellfun('length',DF(1).seriesparam);
% non series dimensions stay size 1
SeriesParIndex = cell(1,length(nSeriesPar));

% use these arrays to bring the test-dimension to the front
shiftArray = [1 1-sampD 1];
SeriesPermArray = circshift(1:length(nSeriesPar),shiftArray);

p = cell(size(DF(1).cells));
h = cell(size(DF(1).cells));
stats = cell(size(DF(1).cells));

for i = 1:prod(nLev)
    
    DF(1).cells{i} = permute(DF(1).cells{i},SeriesPermArray);
    DF(2).cells{i} = permute(DF(2).cells{i},SeriesPermArray);
    
    for j = 1:prod(nSeriesPar)
        [SeriesParIndex{:}] = ind2sub(nSeriesPar,j);
        
        % move test dimension to the front and get data vectors
        SeriesPermParIndex = circshift(SeriesParIndex,shiftArray);
        X = DF(1).cells{i}(:,SeriesPermParIndex{2:end});
        Y = DF(2).cells{i}(:,SeriesPermParIndex{2:end});
        
        if isempty(X)||isempty(Y)||all(isnan(X))||all(isnan(Y))
            p{i}(SeriesParIndex{:}) = NaN;
            h{i}(SeriesParIndex{:}) = false;
            stats{i}{SeriesParIndex{:}} = [];
            continue;
        end
    
        switch FunName
            case 'signrank'
                ii = ~isnan(X)&~isnan(Y);
                [p{i}(SeriesParIndex{:}),h{i}(SeriesParIndex{:}),stats{i}{SeriesParIndex{:}}] = signrank(X(ii),Y(ii),FunInput{:});
            case 'signtest'
                ii = ~isnan(X)&~isnan(Y);
                [p{i}(SeriesParIndex{:}),h{i}(SeriesParIndex{:}),stats{i}{SeriesParIndex{:}}] = signtest(X(ii),Y(ii),FunInput{:});
            case 'ttest'
                ii = ~isnan(X)&~isnan(Y);
                [h{i}(SeriesParIndex{:}),p{i}(SeriesParIndex{:}),ci,stats{i}{SeriesParIndex{:}}] = ttest(X(ii),Y(ii),FunInput{:});
            case 'ttest2'
                iix = ~isnan(X);
                iiy = ~isnan(Y);
                [h{i}(SeriesParIndex{:}),p{i}(SeriesParIndex{:}),ci,stats{i}{SeriesParIndex{:}}] = ttest2(X(iix),Y(iiy),FunInput{:});
            case 'ranksum'
                iix = ~isnan(X);
                iiy = ~isnan(Y);
                [p{i}(SeriesParIndex{:}),h{i}(SeriesParIndex{:}),stats{i}{SeriesParIndex{:}}] = ranksum(X(iix),Y(iiy),FunInput{:});
        end
    end
end

for i=1:numel(h)
    h{i} = logical(h{i});
end

