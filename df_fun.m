function self = df_fun(self,funHandle,varargin)
% wrapper for cellfun.m
self.cells = cellfun(funHandle,self.cells,varargin{:});