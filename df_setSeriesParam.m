function self = df_setSeriesParam(self,parDim,par,NewParLabel,repMatData)
% DF_SETSERIESPARAM set series parameter
%
% Parameters
% ----------
% parDim: int
%     dimension OR label of series
% par: double or cell
%     parameter array numeric or cell of strings
% NewParLabel: char
%     rename series
% repMatData: bool
%     if true, and series dimension is one, replicates data array in cells
%
% Returns
% -------
% self: DataFrame
%
% Examples
% --------
% ::
%
%     DataFrame = df_setSeriesParam(DataFrame,ParDim,Parameter,NewParLabel)
%     DataFrame = df_setSeriesParam(DataFrame,ParLabel,Parameter)
%     DataFrame = df_setSeriesParam(DataFrame,[],par,NewParLabel)
%

if nargin<5
    repMatData = false;
    if nargin<4
        NewParLabel = [];
    end
end

if all([iscell(par) iscell(NewParLabel)]) && length(par) == length(NewParLabel) && isempty(self.seriesdim)
    % set all existing series
    self.seriesdim = parDim;
    self.seriesparam = par;
    self.serieslabel = NewParLabel;

else
    % get series index
    if isnumeric(parDim)
        isPar = self.seriesdim==parDim;
        if any(isPar)
            iSeries = find(isPar);
        else
            iSeries = length(self.seriesdim)+1;
        end
    else
        iSeries = df_findSeriesNr(self,parDim);
    end
    
    % set series dimension
    if isnumeric(parDim)
        self.seriesdim(iSeries) = parDim;
    end
    
    % set series parameter
    if isempty(self.seriesparam) && iSeries==1
        self.seriesparam = {par};
    else
        if iscell(par)
            self.seriesparam(iSeries) = par;
        else
            self.seriesparam{iSeries} = par;
        end
    end
    
    % set Series label
    if ~isempty(NewParLabel)
        if iscell(NewParLabel)
            self.serieslabel(iSeries) = NewParLabel;
        else
            self.serieslabel(iSeries) = {NewParLabel};
        end
    end
end


% replicate arrays in data cells 
if repMatData
    parDim = self.seriesdim(iSeries);
    if size(self.cells,parDim)~=1
        error('Length in the dimension of replication must be 1!!!');
    end
    parN = length(self.seriesparam{iSeries});
    nGrps = numel(self.cells);
    for iGrp = 1:nGrps
        nRepArray = ndims(self.cells{iGrp});
        RepArray = ones(1,nRepArray);
        RepArray(parDim) = parN;
        self.cells{iGrp} = repmat(self.cells(iGrp),RepArray);
    end
end