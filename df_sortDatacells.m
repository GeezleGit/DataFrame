function [DF,I] = df_sortDatacells(DF,dim,direction)
    if nargin<3
        direction = 'ascend';
        if nargin<2
            dim = df_celldim(DF);
        end;end;
    [nFac,nLev,nEl] = df_size(DF);
    I = cell(nLev);
    for i=1:prod(nLev)
        [DF.cells{i},I{i}] = sort(DF.cells{i},dim,direction);
    end
    
end
