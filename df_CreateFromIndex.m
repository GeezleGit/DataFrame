function DF = df_CreateFromIndex(DF,F,L,Index,Data,DataIndex)
    % 
    nFac = length(F);
    nLevel = cellfun('length',L);
    nGrps = prod(nLevel);
    nData = size(Data,2);
    DF.factor = F;
    DF.level = L;
    if nargin<6||isempty(DataIndex)
        DataIndex = true(1,nData);
    end
    
    if nFac ==1;DF.cells = cell([nLevel 1]);
    else DF.cells = cell(nLevel);end
    iGrp = cell(1,nFac);
    for i=1:nGrps
        [iGrp{:}] = ind2sub([nLevel],i);
        cGrpIndex = false(nFac,nData);
        for iFac = 1:nFac
            cGrpIndex(iFac,:) = Index{iFac}(iGrp{iFac},:);
        end
        DF.cells{i} = Data(all(cGrpIndex,1) & DataIndex)';
    end
end

% function i = isempty(DF,opt)
%     if isnumeric(DF.cells) && isempty(DF.cells)
%         i = true;
%     elseif iscell(DF.cells)
%         i = cellfun('isempty',DF.cells);
%         if nargin>1
%             switch upper(opt)
%                 case 'ALL';i = all(i(:));
%                 case 'ANY';i = any(i(:));
%             end
%         end
%     end
% end

