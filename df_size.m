function [nFac,nLev,nEl] = df_size(DF,seriesLabel)
% DF_SIZE
% 
% Example
% -------
% ::
% 
%     [nFac,nLev,nEl] = df_size(DF,seriesLabel)
% 
% Returns
% -------
% nFac:
%     number of Factors (dimensions of cells)
% nLev:
%     number of level for each factor
% nEl:
%     number of elements in each group (cell)
%

nLev = size(DF.cells);
nFac = length(nLev);
if isempty(DF.cells)
    nEl = 0;
else
    if nargin<2
        nEl = cellfun('prodofsize',DF.cells);
    elseif isempty(seriesLabel)
        sampleD = df_celldim(DF);
        nEl = cellfun('size',DF.cells,sampleD);
    elseif ~isempty(seriesLabel)
        sampleD = df_findSeriesDim(DF,seriesLabel);
        nEl = cellfun('size',DF.cells,sampleD);
    end
end
nNonSingle = find(nLev>1,1,'last');% trailing singletons
numDFFac = length(DF.factor);
numDFLev = length(DF.level);
if all(nLev==1);nLev=1;nFac=1;return;end% this is the case of 1 cell
if numDFFac>0 && numDFLev>0
    if numDFFac>0 && numDFFac<nFac && (isempty(nNonSingle) || nNonSingle<=numDFFac)
        % less factor labels than dimensions in cell array
        nFac = numDFFac;
        nLev = nLev(1:nFac);
        nEl = squeeze(nEl);
    elseif numDFFac>0 && numDFFac<nFac && nNonSingle>numDFFac
        error('Factor field doesn''t match data cells!');
    else
        nFac = nNonSingle;
        nLev = nLev(1:nFac);
        nEl = squeeze(nEl);
    end
end

