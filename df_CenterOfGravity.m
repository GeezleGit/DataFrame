function [dfCOG, dfGRAV] = df_CenterOfGravity(self,seriesLabel)
%% DF_CENTEROFGRAVITY 
%

nGrps = numel(self.cells);
[sampD,seriesD] = df_celldim(self);

if nargin<2 || isempty(seriesLabel) || strcmpi(seriesLabel,'samples')
    MeanDim = sampD;
    MeanSeriesIndex = NaN; 
else
    MeanSeriesIndex = strcmp(self.serieslabel,seriesLabel); 
    MeanDim = self.seriesdim(MeanSeriesIndex);
end

dfCOG = self;
dfGRAV = self;
for iGrp = 1:nGrps
    [dfCOG.cells{iGrp}, dfGRAV.cells{iGrp}] = centerOfGravity(self.seriesparam{MeanSeriesIndex},self.cells{iGrp},MeanDim);
end
dfCOG.seriesparam(MeanSeriesIndex) = [];
dfCOG.seriesdim(MeanSeriesIndex) = [];
dfGRAV.seriesparam(MeanSeriesIndex) = [];
dfGRAV.seriesdim(MeanSeriesIndex) = [];



function [Y,mM] = centerOfGravity(X,M,D)
%% CENTEROFGRAVITY
%
% [Y,mM] = centerOfGravity(X,M,D)
%
% X ... vector of positions
% M ... matrix of masses
% D ... dimension of positions within M

X = X(:);
n = length(X);
s = size(M);
if s(D)~=n
    error('Arrays are inconsistent!');
end
permArray = 1:length(s);
permArray(1) = D;
permArray(D) = 1;
X = permute(X,permArray);
repArray = s;
repArray(D) = 1;
X = repmat(X,repArray);
Y = sum(X.*M,D)./sum(M,D);
mM = sum(M,D)./n;