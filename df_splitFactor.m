function DF = df_splitFactor(DF,OldFactor,OldLevel,NewFactors,LevelConversion)
% DF_SPLITFACTOR 
%
% Parameters
% ----------
% OldFactor: char
%     The factor to split
% OldLevel: cell or double
%     {1xN}
% NewFactors: cell
%     {1xK}
% LevelConversion: cell
%     {KxN}
%
% Example
% -------
% ::
% 
%     DF = df_splitFactor(DF,OldFactor,OldLevel,NewFactors,LevelConversion)
%     df_splitFactor(cR,'compartment',{'SG-source' 'SG-sink' 'IG-sink' 'IG-source'},{'compartment' 'current'}, {'SG' 'SG' 'IG' 'IG';'source' 'sink' 'source' 'sink'})
%

nNewFactor = size(LevelConversion,1);
for i=1:nNewFactor
    LevelNew{i} = unique(LevelConversion(i,:));
    nLevelNew(i) = length(LevelNew{i});
end

nDF = length(DF);
for iDF = 1:nDF
%     [nFac,nLev,nEl] = df_size(DF(iDF));
%     [iLevel,iFac] = df_findFactorLevel(DF(iDF),OldFactor);
    iFac = df_findFactor(DF(iDF),OldFactor);
    if isempty(iFac);continue;end
    
    % shift the factor to split to end
    DF(iDF).cells = shiftdim(DF(iDF).cells,iFac);
    DF(iDF).factor = circshift(DF(iDF).factor,[1 -iFac]);
    DF(iDF).level = circshift(DF(iDF).level,[1 -iFac]);
    [nFac,nLev,nEl] = df_size(DF(iDF));
    
    % construct new arrays
    xfactor = [DF(iDF).factor(1:end-1) NewFactors];
    xlevel = [DF(iDF).level(1:end-1) LevelNew];
    xnLev = [nLev(1:end-1) nLevelNew];
    xCells = cell(xnLev);
    xnFac = length(xfactor);
    
    % fill new array
    ix = cell(1,xnFac);
    iold = cell(1,nFac);
    for i = 1:prod(xnLev)
        [ix{:}] = ind2sub(xnLev,i);
        iold(1:nFac-1) = ix(1:nFac-1);% old factors keep their level index
        convIndex = false(size(LevelConversion));
        
        if isnumeric(OldLevel)
            for j=1:nNewFactor
                convIndex(j,:) = LevelNew{j}(ix{nFac-1+j}) == LevelConversion(j,:);
            end
            iold{nFac} = find(OldLevel(all(convIndex,1)) == DF(iDF).level{nFac});
        elseif iscell(OldLevel)
            for j=1:nNewFactor
                convIndex(j,:) = strcmp(LevelNew{j}{ix{nFac-1+j}},LevelConversion(j,:));
            end
            iold{nFac} = find(strcmp(OldLevel{all(convIndex,1)},DF(iDF).level{nFac}));
        end
        
        xCells(ix{:}) = DF(iDF).cells(iold{:});
    end
    
    % shift the factor to split to end
    DF(iDF).cells = shiftdim(xCells,nFac-iFac);
    DF(iDF).factor = circshift(xfactor,[1 -(nFac-iFac)]);
    DF(iDF).level = circshift(xlevel,[1 -(nFac-iFac)]);
end

