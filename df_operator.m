function DF1 = df_operator(DF1,DF2,operator,ExpandSeries)

% basic mathmatical operations between two DataFrames
%
% Allows for one DataFrame to have a single DataCell (Level)
%
% DF1 = df_operator(DF1,DF2,'+')
% DF1 = df_operator(DF1,DF2,'-')
% DF1 = df_operator(DF1,DF2,'*')
% DF1 = df_operator(DF1,DF2,'/')
%
% ExpandSeries

if nargin<4
    ExpandSeries = false;
end

% allow for single data cells
nGrps1 = numel(DF1.cells);
nGrps2 = numel(DF2.cells);
nonGrps1 = cellfun('isempty',DF1.cells);
nonGrps2 = cellfun('isempty',DF2.cells);

if nGrps1==1 && nGrps2>1
    GrpIndex(1,:) = ones(1,nGrps2);
    GrpIndex(2,:) = [1:nGrps2];
    nGrps = nGrps2;
elseif nGrps1>1 && nGrps2==1
    GrpIndex(1,:) = [1:nGrps1];
    GrpIndex(2,:) = ones(1,nGrps1);
    nGrps = nGrps1;
else
    GrpIndex(1,:) = [1:nGrps1];
    GrpIndex(2,:) = [1:nGrps1];
    nGrps = nGrps1;
end

if ExpandSeries
    [DF1,DF2] = df_padFactors(DF1,DF2);
    [DF1,DF2] = df_padSeries(DF1,DF2);
end

% if nargin>3 && ~isempty(ExpandSeries)
%     if strcmp(ExpandSeries,'samples')
%         expandDim(1) = df_celldim(DF1);
%         expandDim(2) = df_celldim(DF2);
%         expandDim = unique(expandDim);
%     elseif any([strcmp(DF1.serieslabel,ExpandSeries) strcmp(DF2.serieslabel,ExpandSeries)])
%         expandDim = df_getSeriesDim(DF1,ExpandSeries);
%     end
%     
%     xn1 = unique(cellfun('size',DF1.cells(~nonGrps1),expandDim));
%     xn2 = unique(cellfun('size',DF2.cells(~nonGrps2),expandDim));
%     nd1 = unique(cellfun('ndims',DF1.cells(~nonGrps1)));
%     nd2 = unique(cellfun('ndims',DF2.cells(~nonGrps2)));
%     repArr1 = ones(1,nd1);
%     repArr2 = ones(1,nd2);
%     if xn1==1 && xn2>1
%         repArr1(expandDim) = xn2;
%         for iGrp = unique(GrpIndex(1,:))
%             DF1.cells{GrpIndex(1,iGrp)} = repmat(DF1.cells{GrpIndex(1,iGrp)},repArr1);
%         end
%     elseif xn1>1 && xn2==1
%         repArr2(expandDim) = xn1;
%         for iGrp = unique(GrpIndex(2,:))
%             DF2.cells{GrpIndex(2,iGrp)} = repmat(DF2.cells{GrpIndex(2,iGrp)},repArr2);
%         end
%     end
% end

switch operator
    case {'+'}
        for iGrp = 1:nGrps
            if any([isempty(DF1.cells{GrpIndex(1,iGrp)}) isempty(DF2.cells{GrpIndex(2,iGrp)})])
                DF1.cells{GrpIndex(1,iGrp)} = [];
            else
                DF1.cells{GrpIndex(1,iGrp)} = DF1.cells{GrpIndex(1,iGrp)} + DF2.cells{GrpIndex(2,iGrp)};
            end
        end
    case {'-'}
        for iGrp = 1:nGrps
            if any([isempty(DF1.cells{GrpIndex(1,iGrp)}) isempty(DF2.cells{GrpIndex(2,iGrp)})])
                DF1.cells{GrpIndex(1,iGrp)} = [];
            else
                DF1.cells{GrpIndex(1,iGrp)} = DF1.cells{GrpIndex(1,iGrp)} - DF2.cells{GrpIndex(2,iGrp)};
            end
        end
    case {'*'}
        for iGrp = 1:nGrps
            if any([isempty(DF1.cells{GrpIndex(1,iGrp)}) isempty(DF2.cells{GrpIndex(2,iGrp)})])
                DF1.cells{GrpIndex(1,iGrp)} = [];
            else
                DF1.cells{GrpIndex(1,iGrp)} = DF1.cells{GrpIndex(1,iGrp)} .* DF2.cells{GrpIndex(2,iGrp)};
            end
        end
    case {'/'}
        for iGrp = 1:nGrps
            if any([isempty(DF1.cells{GrpIndex(1,iGrp)}) isempty(DF2.cells{GrpIndex(2,iGrp)})])
                DF1.cells{GrpIndex(1,iGrp)} = [];
            else
                DF1.cells{GrpIndex(1,iGrp)} = DF1.cells{GrpIndex(1,iGrp)} ./ DF2.cells{GrpIndex(2,iGrp)};
            end
        end
end

