function [arr] = df_getLevelArray(DF,cFac,iLevel)
% converts the cell array in DF.level to double or char
if nargin<3
    iLevel = [];
end

if isempty(cFac)
    iFac = 1:length(DF.factor);
elseif isnumeric(cFac)
    [iFac] = cFac;
elseif iscell(cFac)
    [iFac] = find(strcmp(cFac,DF.factor));
end

for iFac = 1:length(iFac)
    
    if isempty(iLevel)
        if ischar(DF.level{iFac})
            arr = char(DF.level{iFac});
        elseif isnumeric(DF.level{iFac})
            arr = cat(2,DF.level{iFac});
        end
    else
        if ischar(DF.level{iFac})
            arr{iFac} = char(DF.level{iFac}(iLevel{iFac}));
        elseif isnumeric(DF.level{iFac})
            arr{iFac} = cat(2,DF.level{iFac}(iLevel{iFac}));
        end
    end
end
end
