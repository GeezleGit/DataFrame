function subPairs = df_levelIndexPairs(self, comFac, comLevel, grpFac, grpLevel)

% Extracts levels (as indices for self.cells) for paired comparisons
% 
% subPairs = df_levelIndexPairs(levelNum, comFac, comLevel, grpFac, grpLevel)
%
% INPUT
% =====
%
% comFac, comLevel ... define the factors and levels for the
%                      comparison-pairs
% grpFac, grpLevel ... define the factors and levels for grouping
%                      if no grouping is defined, non-comparison-factors
%                      are set to zero.
%
% Output
% ======
%
% subPairs ... numerical array containing indices to self.cells
%              [2 x nFactor x nComparisons]
%
% see df_levelIndexList


levelNum = cellfun('length',self.level);
nFac = length(self.factor);
szCellDim = size(self.cells);
nCellDim = length(szCellDim);
notComFac = setxor([1:nFac],comFac);

if nargin==1
    arrsize = levelNum;
    comFac = [];
    comLevel = {};
    grpFac = [];
    grpLevel = [];
elseif nargin==2
    arrsize = levelNum(comFac);
    comLevel = cell(size(comFac));
    for i=1:length(comFac)
        comLevel{i} = 1:levelNum(comFac(i));
    end
    grpFac = [];
    grpLevel = [];
elseif nargin>=3
    arrsize = cellfun('length', comLevel);
end

indPairs = getIndPairs(arrsize);

% get original level indices
iii = reshape(1:prod(levelNum),szCellDim);
if ~isempty(notComFac)
    iii = permute(iii,[comFac notComFac]);
end
iii = iii(comLevel{:});
if size(indPairs,1)==1 % exception for only one pair
    indPairs = iii(indPairs);
    if size(indPairs,1)~=1
        indPairs = indPairs';
    end
else
    indPairs = iii(indPairs);
end

% calculate subscripts indices
nPairs = size(indPairs,1);
subPairs = zeros(2, nCellDim, nPairs);
dimindex = cell(1,nCellDim);
for iPair = 1:nPairs
    [dimindex{:}] = ind2sub(szCellDim,indPairs(iPair,1));
    subPairs(1,:,iPair) = cat(2,dimindex{:});
    [dimindex{:}] = ind2sub(szCellDim,indPairs(iPair,2));
    subPairs(2,:,iPair) = cat(2,dimindex{:});
end

% set all the factors that weren't used for comparison to zero
subPairs(:,notComFac,:) = 0;

% group the comparisons
if ~isempty(grpFac) && ~isempty(grpLevel)
    grpFacNum = length(grpFac);
    grpLevNum = cellfun('length',grpLevel);
    if numel(grpLevNum)==1
        grpArr = cell(grpLevNum,1);
    else
        grpArr = cell(grpLevNum);
    end
    grpIndex = cell(size(grpLevNum));
    for i = 1:prod(grpLevNum)
        [grpIndex{:}] = ind2sub(grpLevNum,i);
        grpArr{grpIndex{:}} = subPairs;
        for j = 1:grpFacNum
            grpArr{grpIndex{:}} (:,grpFac(j),:) = grpLevel{j}(grpIndex{j});
        end
    end
    subPairs = grpArr;
end




function indPairs = getIndPairs(levelNum)
n = prod(levelNum);
nDim = max([2 length(levelNum)]);
nPairs = sum(n-1:-1:1);

indPairs = zeros(nPairs,2);
cPointer = 0;
for i=1:n
    cIndex = cPointer+[1:n-i];
    indPairs(cIndex,1) = i;
    indPairs(cIndex,2) = i+1:1:n;
    cPointer = cPointer+(n-i);
end
