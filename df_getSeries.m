function varargout = df_getSeries(self,varargin)
% DF_GETSERIES extract series parameter
%
% Example
% -------
% ::
% 
%     [par,label,dim] = df_getSeries(self)
%     
%     use label as input
%     [par,label] = df_getSeries(self,'seriesLabel1','seriesLabel2', ... ,'seriesLabeln')
%     
%     use dimension as input
%     [par,label] = df_getSeries(self,1,2, ... ,n)
%     
%     to vectorize output for one serieslabel
%     [par,label] = df_getSeries(self,'seriesLabel1',true)


nIndex = length(varargin);
if nIndex==1
    nIndex = 1;
    vectorize = false;
elseif nIndex==2 && islogical(varargin{2})
    nIndex = 1;
    vectorize = varargin{2};
elseif nIndex==0
    varargout{1} = self.seriesparam;
    varargout{2} = self.serieslabel;
    varargout{3} = self.seriesdim;
    return;
else
    vectorize = false;
end


for i=1:nIndex
    if isnumeric(varargin{i})
        k = self.seriesdim==varargin{i};
        if nIndex==1
            vectorize = true;
        end
    elseif ischar(varargin{i})
        k = strcmp(varargin{i},self.serieslabel);
        if nIndex==1
            vectorize = true;
        end
    elseif iscell(varargin{i})
        k = strcmp(varargin{i},self.serieslabel);
    end
    
    par(i) = self.seriesparam(k);
    label(i) = self.serieslabel(k);
end

if vectorize
    par = par{1};
end

varargout{1} = par;
varargout{2} = label;


