function [DF,DFpar] = df_minSeries(DF,seriesLabel,range)
%
% find the minimum in a range of series parameter

serDim = df_getSeriesDim(DF, seriesLabel);
serInd = df_findSeriesNr(DF,seriesLabel);
if nargin>2 && ~isempty(range)
    DF = df_extractSeries(DF,seriesLabel,'BOUNDS',range);
end
serPar = df_getSeries(DF, seriesLabel);
n = numel(DF.cells);
DFpar = DF;
for i = 1:n
    [DF.cells{i},serParIndex] = min(DF.cells{i},[],serDim);
    DFpar.cells{i} = serPar(serParIndex);
end
DF = df_removeSeriesInfo(DF,serInd);
DFpar = df_removeSeriesInfo(DFpar,serInd);