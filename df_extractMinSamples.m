function [self,iRemoveLevel] = df_extractMinSamples(self,m,replaceValue,countMethod,checkFactor,FactorRemovalMode)
% extracts data cells that have a minimum number of samples
%
% self = df_extractMinSamples(self,m,replaceValue,countMethod,checkFactor,FactorRemovalMode)
% m ................... minimum samples accepted
% replaceValue ........ replace samples of detected data cells, use [] to clear
%                       cells
% countMethod ......... 'number' number of elements
%                       '~NaN'   number of elements ~isnan()
% checkFactor ......... remove empty levels of these factors
%                       leave empty to skip
% FactorRemovalMode ... 'all' - remove levels that are completely empty
%                       'any' - remove levels that are partially empty


sampleDim = df_celldim(self);
nFac = ndims(self.cells);

% count
switch countMethod
    case 'number'
        n = cellfun('size',self.cells,sampleDim);
    case '~NaN'
        n = zeros(size(self.cells));
        for i=1:numel(self.cells)
            nnnn = sum(~isnan(self.cells{i}),sampleDim);
            n(i) = min(nnnn(:));
        end
end

% check data cells
isGoodCells = n>=m;
if isempty(replaceValue)
    % replace with empty cells
    self.cells(~isGoodCells) = cell(1,sum(~isGoodCells(:)));
else
    % replace sample values with replacement values to preserve number of
    % elements
    for i=1:numel(self.cells)
        if ~isGoodCells(i)
            self.cells{i}(:) = replaceValue;
        end
    end
end
        
% remove redundant factor level
if ~isempty(checkFactor)
    FacNr = df_findFactor(self,checkFactor);
    iRemoveLevel = cell(1,length(FacNr));
    for iFac = FacNr(:)'
        permArray = [iFac setxor(iFac,1:nFac)];
        isGoodCells = n>=m;
        isGoodCells = permute(isGoodCells,permArray);
        switch FactorRemovalMode
            case 'all'
                iRemoveLevel{FacNr==iFac} = all(~isGoodCells(:,:),2);
            case 'any'
                iRemoveLevel{FacNr==iFac} = any(~isGoodCells(:,:),2);
            otherwise
                iRemoveLevel{FacNr==iFac} = false(size(isGoodCells,1),1);
        end
        self.cells = permute(self.cells,permArray);
        self.cells(iRemoveLevel{FacNr==iFac},:) = [];
        self.level{iFac}(iRemoveLevel{FacNr==iFac}) = [];
        [sortedpermArray,permArray] = sort(permArray);
        self.cells = permute(self.cells,permArray);
    end
else
    iRemoveLevel = [];
end
    
    