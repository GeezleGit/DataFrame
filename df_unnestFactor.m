function DF = df_unnestFactor(DF,NestedFactor,NestedFactorLevelOrder,NestDim,FactorDim)
    % Moves data that is stored as a matrix dimension in data cells
    % df = unnestFactor(DF,NestedFactor,NestedFactorLevelOrder,NestDim,FactorDim)
    % NestedFactor ............. factor name
    % NestedFactorLevelOrder ... reorder the levels
    % NestDim .................. dimension for concatenation in cells
    % FactorDim ................ dimension for concatenation in cells
    
    nNestLevel = length(NestedFactorLevelOrder);
    [nFac,nLev,nEl] = getsize(DF);
    if any(nEl(:)~=nNestLevel);error('Number of elements in nested cells are inconsistent!');end
    if nargin<5 || isempty(FactorDim)
        FactorDim = nFac+1;
    end
    
    newFactor = [DF.factor {NestedFactor}];
    newLevel = [DF.level {NestedFactorLevelOrder}];
    newCells = cell([nLev nNestLevel]);
    
    LevelIndex = cell(1,nFac);
    for i = 1:prod(nLev)
        [LevelIndex{:}] = ind2sub(nLev,i);
        for j = 1:nNestLevel
            CellElementIndex = num2cell(ones(ndims(DF.cells(LevelIndex{:}))));
            CellElementIndex(NestDim) = {j};
            newCells{LevelIndex{:},j} = DF.cells{LevelIndex{:}}(CellElementIndex{:});
        end
    end
    
    newFactor = circshift(newFactor,[1 FactorDim-(nFac+1)]);
    newLevel = circshift(newLevel,[1 FactorDim-(nFac+1)]);
    newCells = shiftdim(newCells,(nFac+1)-FactorDim);
    
    DF.factor = newFactor;
    DF.level = newLevel;
    DF.cells = newCells;

end

