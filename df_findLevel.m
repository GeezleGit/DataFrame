function [LevelNr,FacNr] = df_findLevel(self,Factor,Level)
% find index of level within level array
% [...] = findFactor(DF,Factor,Level)

FacNr = strcmp(self.factor,Factor);

if iscell(self.level{FacNr})
    LevelNr = strcmp(self.level{FacNr},Level);
elseif isnumeric(self.level{FacNr})
    LevelNr = self.level{FacNr}==Level;
end

FacNr = find(FacNr);
LevelNr = find(LevelNr);

