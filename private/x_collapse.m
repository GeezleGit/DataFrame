function [CE,cellidx] = x_collapse(CE,collDim,collIndex,catdim)
% concatenate content of cells, collapsed data in first collIndex
% [Y,cellidx] = x_collapse(CE,collDim,collIndex,catdim)
% IN:
% collDim ..... dimension of collapse
% collIndex ... index along collDim of cells to collapse, [] collapses all.
% catdim ...... concatenates content of cells along this dimension
% OUT:
% cellidx ... vector of length in collapsed dim. Contains 0's (collapsed
% indices) and indices of array before collapse.

n = size(CE);
nDim = length(n);

%% check indices to collapse
if isempty(collIndex)
    collIndex = 1:n(collDim);% collapse all along dim
end
collIndex = sort(collIndex);

%% make collapse dim first dim
ShiftArray = [1:nDim];
PermuteArr1 = circshift([1:nDim],[0 1-collDim]);
PermuteArr2 = circshift([1:nDim],[0 1-collDim].*-1);
CE = permute(CE,PermuteArr1);
n = circshift(n,[0 1-collDim]);

%% re-map cell indices
collidx = collIndex(1);% take FIRST!!!! cell index for the collapsed data
cellidx = 1:n(1);
cellidx = cellidx(~ismember(cellidx,collIndex(2:end)));% cell index after collapse
cellidx(cellidx==collidx) = 0;% mark collapsed cell with zero

%% loop thru first dim
n(1) = n(1)-length(collIndex)+1;
Y = cell(n);
for i=1:n(1)
    if i~=collidx
        Y(i,:) = CE(cellidx(i),:);
    else
        Y1 = cell(size(Y(i,:)));
        Y1(:) = {[]};
        for icoll = 1:length(collIndex)
            X1 = CE(collIndex(icoll),:);
            for icell=1:length(X1);
                Y1{icell} = cat(catdim,Y1{icell},X1{icell});
            end
        end
        Y(i,:) = Y1;
    end
end

%% shift dimensions back
Y = permute(Y,PermuteArr2);

CE = Y;
end
