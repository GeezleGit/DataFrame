function [df,nestLevel] = df_Level2Series(DF,NestFactor,NestFactorLevelOrder,CatDim)
% DF_LEVEL2SERIES Moves data of a given factor from the data cell array into
% the cells. Uses the :func:`df_collapsemulti` method.
%
% Parameters
% ----------
% NestFactor: char or cell
%     factor name
% NestFactorLevelOrder: cell
%     reorder the levels
% CatDim: int
%     dimension for concatenation in cells
%
% Examples
% --------
% ::
% 
%     df = df_Level2Series(DF,NestFactor,NestFactorLevelOrder,CatDim)

nFac = length(DF.factor);

if ischar(NestFactor)
    NestFactor = {NestFactor};
    nNestFac = length(NestFactor);
    if nargin<4;CatDim = ones(1,nNestFac).*NaN;end
    if nargin<3;NestFactorLevelOrder(1:nNestFac) = {[]};
    else NestFactorLevelOrder = {NestFactorLevelOrder};end
else
    nNestFac = length(NestFactor);
    if nargin<4, CatDim = ones(1,nNestFac).*NaN;end
    if nargin<3, NestFactorLevelOrder(1:nNestFac) = {[]};end
end

    
[sampD,seriesD] = df_celldim(DF);
nextDim = max([sampD(:);seriesD(:)])+1;
CatDim(isnan(CatDim)) = cumsum(ones(1,sum(isnan(CatDim))))+nextDim;
    
collDim = df_findFactor(DF,NestFactor);
collIndex(1:nNestFac) = {[]};
SqueezeFlag = false;
% put level data into cells
% [df,cellidx] = df_collapse(DF,nestFacNr,NestFactorLevelOrder,CatDim,false);
[df,SourceDimIndex,SqueezeSort] = df_collapsemulti(DF,collDim,collIndex,CatDim,SqueezeFlag);
    
remFacNr = setdiff(1:nFac,collDim);

if ~isempty(remFacNr)
    % rearrange data-cell array
    df.cells  = permute(df.cells,[remFacNr collDim]);
    
    % remove factor info
    df.factor = df.factor(remFacNr);
    df.level = df.level(remFacNr);
else
    df.factor = {'nofactor'};
    df.level = {[0]};
end

% set series parameter
newSeriesDim = length(df.seriesdim)+1 : length(df.seriesdim)+nNestFac;
nestLevel = DF.level(collDim);
df.seriesdim(newSeriesDim) = CatDim;
df.seriesparam(newSeriesDim) = nestLevel;
df.serieslabel(newSeriesDim) = NestFactor;

