function [Lev,Fac] = df_getLevelFromIndex(DF,i)
    % returns levellabel from a data cell index
    % [Lev,Fac] = getLevelFromIndex(DF,i)
    % i as vertical vector indicates indexes
    % i as matrix vector indicates subscripts
    [nFac,nLev,nEl] = getsize(DF);
    Lev = cell(1,nFac);
    [n,nd] = size(i);
    if nd>1
        cSubInd = num2cell(i,1);
    else
        cSubInd = cell(n,nFac);
        [cSubInd{:}] = ind2sub(nLev,i);
    end
    for iFac = 1:nFac
        Lev(iFac) = DF.level{iFac}(cSubInd{iFac});
    end
    Fac = DF.factor;
end
