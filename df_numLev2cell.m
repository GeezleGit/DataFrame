function DF  = df_numLev2cell(DF,Fac)
    % converts numerical DF.level to cell array of strings
    % DF  = numLev2cell(DF,Factor)
    % Factor ... can be name or index
    if isnumeric(Fac)
        iFac = Fac;
    else
        iFac = findFactor(DF,Fac);
    end
    
    for iFac = iFac
        if isnumeric(DF.level{iFac})
            DF.level{iFac} = cellstr(num2str(DF.level{iFac}(:)))';
        elseif iscell(DF.level{iFac}) && isnumeric(DF.level{iFac}{1})
             DF.level{iFac} = cellstr(num2str(cat(1,DF.level{iFac}{:})))';
        end
    end
        
end

