function sz = df_sizeOfCells(self,dim)
% DF_GETSIZEOFCELLS returns the size of all cells
%
% Example
% -------
% ::
% 
%     sz = df_sizeOfCells(self,dim)
%     
%     to get dim use
%     dim = df_getSeriesDim(self,label)
%     [sampD,seriesD] = df_celldim(DF)

if ischar(dim)
    dim = find(strcmp(dim,self.serieslabel));
end
sz = cellfun('size',self.cells,dim);