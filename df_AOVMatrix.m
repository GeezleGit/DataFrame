function [t,factors] = df_AOVMatrix(DF,removeNans)
%
% transform a @DataFrame to a matlab table
% 
% varName .... name of the "dependent" variable, i.e. the datatype/units in cells
% subjName ... name for each SamplePoint, default is sample index in cells
%
% t........... dependent variable                       =column 1
%              independent variable 1 (within subjects) =column 2
%              independent variable 2 (within subjects) =column 3
%              independent variable 3 (within subjects) =column 4
%              subject                                  =column 5

if nargin<2
    removeNans = false;
end
    
[samDim,serDim]     = df_celldim(DF);
[facLabel,levLabel] = df_getFactor(DF);
nSer                = length(serDim);
nFac                = length(facLabel);

[v, indexCells, indexSeries] = df_vectorize(DF,'subscript');
if removeNans
    indexSeries(isnan(v),:) = [];
    indexCells(isnan(v),:) = [];
    v(isnan(v)) = [];
end

t = [v,indexCells];
factors = facLabel;

% now add series
% remember that series index does not translate directly into array dimension 
for iSer = 1:nSer
    serNr = find(DF.seriesdim==serDim(iSer));
    t = [t indexSeries(:,serDim(iSer))];
    factors = [factors DF.serieslabel{serNr}];
end

% add samples/subjects
t = [t indexSeries(:,samDim)];
