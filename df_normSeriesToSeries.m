function DF = df_normSeriesToSeries(DF, Option, SeriesLabel)
%
% 1. take one data point 
% 2. expand it over the series dimension
% 3. do norm operation

refOption = 'equal parameter';

nDF = length(DF);
for iDF = 1:nDF
    [nLevels] = size(DF(iDF).cells);
    nGrps = prod(nLevels);
    
    serNr  = strcmp(DF(iDF).serieslabel,SeriesLabel{1});
    serNum = length(DF(iDF).seriesparam{serNr});
    serDim = df_getSeriesDim(DF(iDF),SeriesLabel{1});
    sampD  = df_celldim(DF(iDF));
    serPar = DF(iDF).seriesparam{serNr};

    rSerDim = df_getSeriesDim(DF(iDF),SeriesLabel{2});
    rSerPar = df_getSeries(DF(iDF),SeriesLabel{2});
    
    for iGrp = 1:nGrps
        
        cellSz = size(DF(iDF).cells{iGrp});
        cellDimNum = length(cellSz);
        isSeries = false(1,cellDimNum);
        isSeries(serDim) = true;
        cellIdx = cell(1,cellDimNum);
        
        refSz = cellSz;
        refSz(serDim) = 1;
        RefData = zeros(refSz).*NaN;
        refIdx = cellIdx;
        
        subIndex = cell(1,sum(~isSeries));
        for i = 1:prod(cellSz(~isSeries))
            [subIndex{:}] = ind2sub(cellSz(~isSeries),i);
            cellIdx(~isSeries) = subIndex;
            switch refOption
                case 'equal parameter'; cellIdx{isSeries} = find(serPar==rSerPar(cellIdx{rSerDim}));
            end
            refIdx(~isSeries) = subIndex;
            refIdx{isSeries} = 1;
            RefData(refIdx{:}) = DF(iDF).cells{iGrp}(cellIdx{:});
        end
        
        repArray = ones(1,cellDimNum);
        repArray(serDim) = cellSz(serDim);
        RefData = repmat(RefData,repArray);
        
        switch Option
            case 'MICHELSON'
                DF(iDF).cells{iGrp} = (DF(iDF).cells{iGrp} - RefData) ./ (DF(iDF).cells{iGrp} + RefData);
            case 'MICHELSONinverse'
                DF(iDF).cells{iGrp} = (RefData - DF(iDF).cells{iGrp}) ./ (DF(iDF).cells{iGrp} + RefData);
            case 'DIFFERENCE'
                DF(iDF).cells{iGrp} = (DF(iDF).cells{iGrp} - RefData);
            case 'DIFFERENCEinverse'
                DF(iDF).cells{iGrp} = (RefData - DF(iDF).cells{iGrp});
            case 'RATIO'
                DF(iDF).cells{iGrp} = DF(iDF).cells{iGrp} ./ RefData;
            case 'RATIOinverse'
                DF(iDF).cells{iGrp} = RefData ./ DF(iDF).cells{iGrp};
        end
    end
end
