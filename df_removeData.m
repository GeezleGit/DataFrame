function [DF,keptIdx,remIdx] = df_removeData(DF,Option,varargin)
% DF_REMOVEDATA remove Datums from cell according to given option
%
% Examples
% --------
% ::
%
%     [DF,keptIdx,remIdx] = df_removeData(DF, ...)
%     [ ... ] = df_removeData(DF,'FIRST',KeepNum)
%     [ ... ] = df_removeData(DF,'RANDOM',KeepNum) - keep 
%     [ ... ] = df_removeData(DF,'ZEROS',replaceValue)
%     [ ... ] = df_removeData(DF,'VALUE',values,replaceValue)
%     [ ... ] = df_removeData(DF,'BOUNDS-EX',boundaries,replaceValue)
%     [ ... ] = df_removeData(DF,'BOUNDS-IN',boundaries,replaceValue)
%     [ ... ] = df_removeData(DF,'ISNAN',replaceValue)
%     [ ... ] = df_removeData(DF,'NOTISNAN',replaceValue)
%     [ ... ] = df_removeData(DF,'INDEX',indexCellArray,replaceValue)
%     [ ... ] = df_removeData(DF,'INDEX',DF2,replaceValue)


keptIdx = cell(size(DF.cells));
remIdx = cell(size(DF.cells));
switch Option
    case 'FIRST'
        k = numel(DF.cells);
        KeepNum = varargin{1};
        for i=1:k
            cDataIdx = find(~isnan(DF.cells{i}));
            cDataN = find(length(DF.cells{i}));
            if isempty(cDataIdx) || length(cDataIdx)<KeepNum
                DF.cells{i} = [];
                keptIdx{i} = [];
                remIdx{i} = 1:cDataN;
            else
                cDataIdx = cDataIdx(1:KeepNum);
                DF.cells{i} = DF.cells{i}(cDataIdx);
                keptIdx{i} = cDataIdx;
                remIdx{i} = setxor(1:cDataN,cDataIdx);
            end
        end
    case 'RANDOM'
        k = numel(DF.cells);
        KeepNum = varargin{1};
        for i=1:k
            cDataIdx = find(~isnan(DF.cells{i}));
            cDataN = find(length(DF.cells{i}));
            if isempty(cDataIdx) || length(cDataIdx)<KeepNum
                DF.cells{i} = [];
                keptIdx{i} = [];
                remIdx{i} = 1:cDataN;
            else
                cDataIdx = cDataIdx(randsample(length(cDataIdx),KeepNum));
                DF.cells{i} = DF.cells{i}(cDataIdx);
                keptIdx{i} = cDataIdx;
                remIdx{i} = setxor(1:cDataN,cDataIdx);
            end
        end
    case 'ZEROS'
        replaceValue = varargin{1};
        k = numel(DF.cells);
        for i=1:k
            remIdx{i} = DF.cells{i}==0;
            keptIdx{i} = ~remIdx{i};
            if isempty(replaceValue)
                DF.cells{i}(remIdx{i}) = [];
            else
                DF.cells{i}(remIdx{i}) = replaceValue;
            end
        end
    case 'EXVALUE'
        vals = varargin{1};
        k = numel(DF.cells);
        for i=1:k
            remIdx{i} = ismember(DF.cells{i},vals);
            keptIdx{i} = ~remIdx{i};
            if length(varargin)==1
                DF.cells{i}(remIdx{i}) = [];
            else
                DF.cells{i}(remIdx{i}) = varargin{2};
            end
        end
    case 'INVALUE'
        vals = varargin{1};
        k = numel(DF.cells);
        for i=1:k
            keptIdx{i} = ismember(DF.cells{i},vals);
            remIdx{i} = ~keptIdx{i};
            if length(varargin)==1
                DF.cells{i}(remIdx{i}) = [];
            else
                DF.cells{i}(remIdx{i}) = varargin{2};
            end
        end
    case 'BOUNDS-IN'
        bnds = varargin{1};
        if length(varargin)>1
            replaceValue = varargin{2};
        else
            replaceValue = [];
        end
        k = numel(DF.cells);
        for i=1:k
            remIdx{i} = DF.cells{i}<bnds(1) | DF.cells{i}>bnds(2);
            keptIdx{i} = ~remIdx{i};
            if isempty(replaceValue)
                DF.cells{i}(remIdx{i}) = [];
            else
                DF.cells{i}(remIdx{i}) = replaceValue;
            end
        end
    case 'BOUNDS-EX'
        bnds = varargin{1};
        if length(varargin)>1
            replaceValue = varargin{2};
        else
            replaceValue = [];
        end
        k = numel(DF.cells);
        for i=1:k
            remIdx{i} = DF.cells{i}<=bnds(1) | DF.cells{i}>=bnds(2);
            keptIdx{i} = ~remIdx{i};
            if isempty(replaceValue)
                DF.cells{i}(remIdx{i}) = [];
            else
                DF.cells{i}(remIdx{i}) = replaceValue;
            end
        end
    case 'ISNAN'
        if length(varargin)>0
            replaceValue = varargin{1};
        else
            replaceValue = [];
        end
        k = numel(DF.cells);
        for i=1:k
            remIdx{i} = isnan(DF.cells{i});
            keptIdx{i} = ~remIdx{i};
            if isempty(replaceValue)
                DF.cells{i}(remIdx{i}) = [];
            else
                DF.cells{i}(remIdx{i}) = replaceValue;
            end
        end
    case 'NOTISNAN'
        if length(varargin)>0
            replaceValue = varargin{1};
        else
            replaceValue = [];
        end
        k = numel(DF.cells);
        for i=1:k
            remIdx{i} = ~isnan(DF.cells{i});
            keptIdx{i} = ~remIdx{i};
            if isempty(replaceValue)
                DF.cells{i}(remIdx{i}) = [];
            else
                DF.cells{i}(remIdx{i}) = replaceValue;
            end
        end
    case 'INDEX'
        if iscell(varargin{1})
            remIdx = varargin{1};
        elseif isa(varargin{1},'DataFrame')
            remIdx = varargin{1}.cells;
        end
        if length(varargin)>1
            replaceValue = varargin{2};
        else
            replaceValue = [];
        end
        k = numel(DF.cells);
        for i=1:k
            keptIdx{i} = ~remIdx{i};
            DF.cells{i}(remIdx{i}) = replaceValue;
        end
end

