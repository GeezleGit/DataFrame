function self = df_feval(self,funName,varargin)
% applies a function on cells
%
% function self = df_feval(self,'functionName/Handle',funInput1,funInput2, ... ,'DataFrame.cells', ... ,funInputN)
%
% use 'DataFrame.cells' to indicate input position for cell content in the line of arguments 

inputIndex = strcmp('DataFrame.cells',varargin);

nDF = numel(self);
for iDF=1:nDF
    nGrps = numel(self(iDF).cells);
    for iGrp = 1:nGrps
        varargin(inputIndex) = self(iDF).cells(iGrp);
        self(iDF).cells{iGrp} = feval(funName,varargin{:});
    end
end

