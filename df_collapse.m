function [DF,cellidx] = df_collapse(DF,collDim,collIndex,catdim,CollapseToCells)
    % concatenate content of cells, collapsed data in first collIndex
    % [Y,cellidx] = collapse(DF,collDim,collIndex,catdim)
    % IN:
    % collDim ..... dimension of collapse
    % collIndex ... index along collDim of cells to collapse, [] collapses all.
    % catdim ...... concatenates content of cells along this dimension
    % CollapseToCells ... concatenate as cells 
    % OUT:
    % cellidx ... vector of length in collapsed dim. Contains 0's (collapsed
    % indices) and indices of array before collapse.
    
    n = size(DF.cells);
    nDim = length(n);
    
    if nargin<5
        CollapseToCells = false;
    end
    
    %% check indices to collapse
    if collDim>nDim && any(collIndex>1)
        error('Indices to collapse are inconsistent. Expected single level!');
    elseif collDim>nDim && all(collIndex==1)
        % no collapse needed, factor non existent and/or single
        cellidx = 0;
        return;
    elseif collDim<=nDim && isempty(collIndex)
        collIndex = 1:n(collDim);% collapse all along dim
    end
    collIndex = sort(collIndex);
    
    %% make collapse dim first dim
    ShiftArray = [1:nDim];
    PermuteArr1 = circshift([1:nDim],[0 1-collDim]);
    PermuteArr2 = circshift([1:nDim],[0 1-collDim].*-1);
    DF.cells = permute(DF.cells,PermuteArr1);
    n = circshift(n,[0 1-collDim]);
    
    %% re-map cell indices
    collidx = collIndex(1);% take FIRST!!!! cell index for the collapsed data
    cellidx = 1:n(1);
    cellidx = cellidx(~ismember(cellidx,collIndex(2:end)));% cell index after collapse
    cellidx(cellidx==collidx) = 0;% mark collapsed cell with zero
    
    %% loop thru first dim
    n(1) = n(1)-length(collIndex)+1;
    Y = cell(n);
    for i=1:n(1)
        if i~=collidx
            Y(i,:) = DF.cells(cellidx(i),:);
        else
            Y1 = cell(size(Y(i,:)));
            if CollapseToCells
                % concatenate as cell arrays
                Y1(:) = {{}};
                for icoll = 1:length(collIndex)
                    X1 = DF.cells(collIndex(icoll),:);
                    for icell=1:length(X1);
                        Y1{icell} = cat(catdim,Y1{icell},X1(icell));
                    end
                end
            else
                % concatenate as numerical matrix
                Y1(:) = {[]};
                for icoll = 1:length(collIndex)
                    X1 = DF.cells(collIndex(icoll),:);
                    for icell=1:length(X1);
                        if ~isempty(Y1{icell}) && ~isempty(X1{icell})
                            Y1{icell} = cat(catdim,Y1{icell},X1{icell});
                        elseif isempty(Y1{icell}) && ~isempty(X1{icell})
                            Y1{icell} = X1{icell};
                        elseif ~isempty(Y1{icell}) && isempty(X1{icell})
                            Y1{icell} = Y1{icell};
                        end
                    end
                end
            end
            Y(i,:) = Y1;
        end
    end
    
    %% shift dimensions back
    Y = permute(Y,PermuteArr2);
    
    DF.cells = Y;
end

