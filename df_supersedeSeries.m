function DF = df_supersedeSeries(DF,targetSeries,sourceSeries)

% replace all values of target series with values where targetSeries==sourceSeries

seriesOrder = DF.serieslabel(DF.seriesdim);
DF = df_permuteSeries(DF,{targetSeries,sourceSeries},false);

srcPar = df_getSeries(DF, sourceSeries);
tarPar = df_getSeries(DF, targetSeries);
tarNum = length(tarPar);
srcNum = length(srcPar);


[foundPar,tarParIndex] = ismember(srcPar,tarPar);


n = numel(DF.cells);
for i=1:n
    repArr = ones(1,ndims(DF.cells{i}));
    repArr(1) = tarNum;
    for iSrc = 1:srcNum
        DF.cells{i}(:,iSrc,:) = repmat(DF.cells{i}(tarParIndex(iSrc),iSrc,:),repArr);
    end
    
end

DF = df_permuteSeries(DF,seriesOrder,false);
