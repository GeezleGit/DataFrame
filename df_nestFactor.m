function [df,nestLevel] = df_nestFactor(DF,NestFactor,NestFactorLevelOrder,CatDim)
% Moves data of a given factor from the data cell array into
% the cells. Uses the collapse method
% df = nestFactor(DF,NestFactor,NestFactorLevelOrder,CatDim)
% NestFactor ............. factor name
% NestFactorLevelOrder ... reorder the levels
% CatDim ................. dimension for concatenation in cells
%
    nestFacNr = find(strcmp(NestFactor,DF.factor));
    if any(cellfun('length',DF.cells(:))>1)
        [df,cellidx] = df_collapse(DF,nestFacNr,NestFactorLevelOrder,CatDim,true);
    else
        [df,cellidx] = df_collapse(DF,nestFacNr,NestFactorLevelOrder,CatDim,false);
    end
    df.factor = df.factor(~strcmp(NestFactor,DF.factor));
    df.level = df.level(~strcmp(NestFactor,DF.factor));
    df.cells  = permute(df.cells,[find(~strcmp(NestFactor,DF.factor)) find(strcmp(NestFactor,DF.factor))]);
    nestLevel = DF.level{strcmp(NestFactor,DF.factor)};

