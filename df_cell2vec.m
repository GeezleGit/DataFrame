function X = df_cell2vec(DF)
    % Extracts data to a vertical column vector format.
    % X = cell2vec(DF)
    % Levels are encodes as numbers
    % If input is DataFrame array, array number will be last column
    % of matrix.
    % X rows ...... datum
    % X columns ... [Datum,Fac1 ... FacN,DataFrameNr]
    nDF = length(DF);
    X = cell(1,nDF);
    for iDF = 1:nDF% loop through DataFrame array
        [nFac,nLev] = getsize(DF(iDF));
        iLev = cell(1,nFac);
        cDatumNum = cellfun('length',DF(iDF).cells);
        nDatums(iDF) = sum(cDatumNum(:));
        X{iDF} = zeros(nDatums(iDF),nFac+2);
        for i = 1:prod(nLev)% loop through data cells and fill X
            [iLev{:}] = ind2sub(nLev,i);
            cIndex = sum(cDatumNum(1:i-1))+1 : sum(cDatumNum(1:i));
            X{iDF}(cIndex,1) = DF(iDF).cells{iLev{:}}(:);
            for iFac = 1:nFac
                X{iDF}(cIndex,1+iFac) = iLev{iFac};
            end
            X{iDF}(cIndex,nFac+2) = iDF;
        end
    end
    if nDF==1
        X = X{cell};
        X(:,end) = [];
    elseif nDF>1
        X = cat(1,X{:});
    end
end
