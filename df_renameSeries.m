function self = df_renameSeries(self,OldLabel,NewLabel)

for i=1:numel(self)
    if isempty(OldLabel)
        self(i).serieslabel = NewLabel;
    else
        iSer = strcmp(self(i).serieslabel,OldLabel);
        self(i).serieslabel{iSer} = NewLabel;
    end
end
