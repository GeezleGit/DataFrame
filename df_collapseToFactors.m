function DF = df_collapseToFactors(DF,ToFac,ToLevGrps,ToLevNew,catdim)

% collapse/rearrange mulitple factors and level
% 
% Usage
% =====
%
% DF = collapseToFactors(DF,ToFac,ToLevGrps,ToLevNew)
%
% Input
% =====
%
% ToFac ....... cell array of factor labels
% ToLevGrps ... regroup levels, leave empty to skip
%               {{Lev1Grp1 Lev2Grp1 ...} {Lev1Grp2 Lev2Grp2 ...} ...}
% ToLevNew .... {LevelLabelGrp1 LevelLabelGrp2 ... }
% catdim ...... dimension of concatenation within data cells
%
% Examples
% ========
%
% 

if nargin<5
    catdim = celldim(DF);
end

% first, let's remove factors that are not needed anymore
if isempty(DF.factor), foundFac = false;
else foundFac = ismember(DF.factor,ToFac);end
if any(~foundFac)
    collFacNr = find(~foundFac);
    collLevNr = cell(size(collFacNr));collLevNr(:) = {[]};
    [DF,SourceDimIndex,SqueezeSort] = df_collapsemulti(DF,collFacNr,collLevNr,catdim,true);
end

% sort factors
nNewFac = length(ToFac);
NewFac = cell(1,nNewFac);
NewFacLev = cell(1,nNewFac);
NewFacDim = 1:nNewFac;
[foundFac,SortFacInd] = ismember(ToFac,DF.factor);
NewFac(foundFac) = DF.factor(SortFacInd(foundFac));
NewFacLev(foundFac) = DF.level(SortFacInd(foundFac));
NewFacDim(foundFac) = SortFacInd(foundFac);
DF.factor = NewFac;
DF.level = NewFacLev;
if length(NewFacDim)>1
    DF.cells = permute(DF.cells,NewFacDim);
elseif length(NewFacDim)==1 && NewFacDim>1
    error('Can''t be!!');
end

% now, let's regroup the level
if isempty(ToLevGrps);return;end
nFac = length(DF.factor);
for iFac=1:nFac
    DF = df_recombineFactorLevel(DF,DF.factor{iFac},ToLevGrps{iFac},ToLevNew{iFac},catdim);
end

