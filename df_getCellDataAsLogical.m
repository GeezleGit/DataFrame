function c = df_getCellDataAsLogical(self,value)

% Return data cells containing logical values

c = self.cells;
for i=1:numel(c)
    c{i}(:) = value;
end