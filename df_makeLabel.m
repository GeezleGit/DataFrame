function Label = df_makeLabel(DF,style,index, varargin)

% make label strings from Factor/level
%
% Label = df_makeLabel(DF,style,index)
%
% index ... []                    all levels of all factors
%           [i ... n]             absolute index
%           {[i] [j] [k] ... [n]} subscript

[nFac,nLev,nEl] = df_getsize(DF);
cLevelIndex = cell(1,nFac);
nCells = prod(nLev);

% chech from index how many labels to make 
if nargin<3 || isempty(index)
    index = cell(nCells,nFac);
    for i=1:nCells
        [cLevelIndex{:}] = ind2sub(nLev,i);
        index(i,:) = cLevelIndex;
    end
    Label = cell(nLev);
    N = nCells;
elseif isnumeric(index)
    N = length(index);
    Label = cell(N,1);
    newIndex = cell(N,nFac);
    for i=1:N
        [newIndex{i,:}] = ind2sub(nLev,index(i));
    end
    index = newIndex;
elseif iscell(index)
    N = size(index,1);
    Label = cell(N,1);
end

% loop through all index
for i=1:N
    cLevelIndex = index(i,:);
    
    switch style
        case 'default'
            if nargin<4
                includeFactors = 1:nFac;
            else
                includeFactors = varargin{1};
            end
            Label{i} = '';
            if isempty(DF.level);return;end
            for iFac = includeFactors
                if isnumeric(DF.level{iFac}(cLevelIndex{iFac}))
                    if rem(DF.level{iFac}(cLevelIndex{iFac}),1)==0
                        Label{i} = sprintf('%s%s:%1.0f',Label{i},DF.factor{iFac},DF.level{iFac}(cLevelIndex{iFac}));
                    else
                        Label{i} = sprintf('%s%s:%1.2f',Label{i},DF.factor{iFac},DF.level{iFac}(cLevelIndex{iFac}));
                    end
                elseif iscell(DF.level{iFac}(cLevelIndex{iFac}))
                    Label{i} = sprintf('%s%s:%s',Label{i},DF.factor{iFac},DF.level{iFac}{cLevelIndex{iFac}});
                end
                if iFac<nFac
                    Label{i} = sprintf('%s ',Label{i});
                end
            end
    end
end


end

