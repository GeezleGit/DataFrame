function self = df_abs(self)

% apply the abs() function to all data cells

n = numel(self);
for i=1:n
    m = numel(self(i).cells);
    for j=1:m
        self(i).cells{j} = abs(self(i).cells{j});
    end
end