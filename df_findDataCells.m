function i = df_findDataCells(self,factors,levels, subscriptflag)

% Search for level combinations. 
% Returns all levels for factors not specified.
%
% i = df_findDataCells(DataFrame,{'fac1' 'fac2' ...},{lev1Fac1, lev1Fac2 ... ; lev2Fac1, lev2Fac2 ...; ...})
%
% i ... absolute cellindex

if nargin<4
    subscriptflag = false;
end

[searchCellsNum, searchFactorNum] = size(levels);
nLev = size(self.cells);
nFac = length(nLev);
foundFacNr = df_findFactor(self,factors{:});
freeFacNr  = setxor([1:nFac],foundFacNr);
freeFacNum = length(freeFacNr);
foundlevNr = zeros(searchCellsNum,nFac);

for iFac = 1:searchFactorNum
    foundlevNr(:,foundFacNr(iFac)) = df_findFactorLevel(self,factors{iFac},cat(1,levels{:,iFac}));
end


if isempty(freeFacNr)
    i = SubMat2Ind(nLev,foundlevNr);
else
    freeLevNum = prod(nLev(freeFacNr));
    freeLevIndex = cell(1,max([2 freeFacNum]));
    i = ones(freeLevNum*searchCellsNum,1);
    for iFreeLev = 1:freeLevNum
        [freeLevIndex{:}] = ind2sub(nLev(freeFacNr), iFreeLev);
        foundlevNr(:,freeFacNr) = repmat(cat(2,freeLevIndex{1:freeFacNum}),[searchCellsNum 1]);
        i(1+searchCellsNum*(iFreeLev-1):searchCellsNum*(iFreeLev)) = SubMat2Ind(nLev,foundlevNr);
    end
end

if subscriptflag
    LevIndex = cell(1,nFac);
    [LevIndex{:}] = ind2sub(nLev,sort(i));
    i = LevIndex;
else
    i = sort(i);
end



function ndx = SubMat2Ind(matsize,subs)
k   = [1 cumprod(matsize(1:end-1))];
ndx = 1;
numOfIndInput = size(subs,2);
for i = 1:numOfIndInput
    if (any(subs(:,i) < 1)) || (any(subs(:,i) > matsize(i)))
        %Verify subscripts are within range
        error(message('MATLAB:sub2ind:IndexOutOfRange'));
    end
    ndx = ndx + (double(subs(:,i))-1)*k(i);
end

