function varargout = df_getFactorLevel(DF,cFac,iLevel)
% DF_GETFACTORLEVEL
%
% Example
% -------
% ::
% 
%     % [lev,iFac] = getFactorLevel(DF,cFac,iLevel);
%     [lev,iFac] = getFactorLevel(DF,cFac,iLevel)
% lev ..... level label
% iFac .... factor index

if nargin==1
    varargout{1} = DF.level;
    return;
end

if isnumeric(cFac)
    iFac = cFac;
else
    [iFac] = find(strcmp(cFac,DF.factor));
end

if isempty(iFac)
    lev = [];
else
    lev = DF.level{iFac};
    if nargin>2
        lev = lev(iLevel);
    end
end
varargout{1} = lev;
varargout{2} = iFac;

