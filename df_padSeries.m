function [DF1,DF2] = df_padSeries(DF1,DF2)

nGrps(1) = numel(DF1.cells);
nGrps(2) = numel(DF2.cells);
nGrps = unique(nGrps);
if length(nGrps)>1
    error('DataFrames have to have same number of levels!');
end

if numel(DF1.cells{1})>numel(DF2.cells{1})
    DF2.seriesdim = DF1.seriesdim;
    DF2.seriesparam = DF1.seriesparam;
    DF2.serieslabel = DF1.serieslabel;
else
    DF1.seriesdim = DF2.seriesdim;
    DF1.seriesparam = DF2.seriesparam;
    DF1.serieslabel = DF2.serieslabel;
end

for iGrp = 1:nGrps
    
    cCellPair = [DF1.cells(iGrp) DF2.cells(iGrp)];
    nCells = length(cCellPair);
    
    % check array dimensions
    nd = cellfun('ndims', cCellPair);
    [lowDimNr, lowDimCellNr] = min(nd);
    [highDimNr, highDimCellNr] = max(nd);
    
    % check array sizes
    sz = ones(nCells,highDimNr);
    for iCell=1:nCells
        sz(iCell,1:nd(iCell)) = size(cCellPair{iCell});
    end
    repDim = max(sz,[],1);
    
    % replicate arrays
    repMatArr = ones(nCells,highDimNr);
    for iCell=1:nCells
        if any(sz(iCell,:)~=repDim & sz(iCell,:)~=1)
            error('Array dimension in cell-pairs must pair equal or singleton!');
        end
        cDimIndex = sz(iCell,:)==1;
        repMatArr(iCell,cDimIndex) = repDim(cDimIndex);
        cCellPair{iCell} = repmat(cCellPair{iCell},repMatArr(iCell,:));
    end
        
    DF1.cells(iGrp) = cCellPair(1);
    DF2.cells(iGrp) = cCellPair(2);

end


