function m = df_min(self,mode,Fac,Lev)
% get maximum value of data cells
% m = df_min(DF,mode,Fac,Lev)
%
% mode ... 'series'  get minimum of series data
%          'samples' get minimum of samples data
%          'total'   get total minimum
%          [dim]     get minimum along dimension

if nargin<2
    mode = 'samples';
end

if ischar(mode) && strcmp(mode,'samples')
    [sampDim,seriesDim] = df_celldim(self);
    opDim = sampDim;
elseif ischar(mode) && strcmp(mode,'series')
    [sampDim,seriesDim] = df_celldim(self);
    opDim = seriesDim;
elseif isnumeric(mode)
    opDim = mode;
elseif ischar(mode) && strcmp(mode,'total')
    opDim = [];
else
    opDim = [];
end

n = numel(self.cells);

if length(opDim) == 1
    m = min(cat(opDim,self.cells{:}),[],opDim);
elseif isempty(opDim)
    m = zeros(size(self.cells)).*NaN;
    for i=1:n
        if ~isempty(self.cells{i})
            m(i) = min(self.cells{i}(:));
        end
    end
    m = min(m(:));
elseif length(opDim) > 1;
    m = cell(size(self.cells));
    for i=1:n
        m{i} = self.cells{i};
        for j=1:length(opDim)
            m{i} = min(m{i},[],opDim(j));
        end
    end
    finalDim = min(opDim);
    m = cat(finalDim,m{:});
    m = min(m,[],finalDim);
end
    
    

