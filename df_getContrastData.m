function df = df_getContrastData(DF,cFac,cLevel)
%% DF_GETCONTRASTDATA Extract data of specified level of a factor
%
% df(1, ..., n) = df_getContrastData(DF,cFac,cLevel)
% Data ... [1 length(cLevel)] DataFrameObjects
%
% See also DF_EXTRACTFACTORLEVEL, DF_EXTRACTSERIES

[nFac,nLev] = df_getsize(DF);
if isnumeric(cLevel)
    [iFac] = df_findFactor(DF,cFac);
    iLevel = cLevel;
else
    [iLevel,iFac] = df_findFactorLevel(DF,cFac,cLevel);
end
if (iscell(iLevel)&&cellfun('isempty',iLevel)) || (isnumeric(iLevel)&&any(iLevel==0));
    error('Can''t find factor level!');
    df=[];return;
end

nL = length(cLevel);

n = prod(nLev)/nLev(iFac);
nLevReshaped = nLev([1:nFac]~=iFac);
if length(nLevReshaped)>1% make safe for one factor
    Data = cell(nLevReshaped);
elseif length(nLevReshaped)==1
    Data = cell(nLevReshaped,1);
elseif isempty(nLevReshaped)
    nLevReshaped = [1 1];
    Data = cell(1,1);
else
    error(' ... ');
end

for iL = 1:nL
    df(iL) = DF;
    df(iL).cells = Data;
    df(iL).factor = DF.factor([1:nFac]~=iFac);
    df(iL).level = DF.level([1:nFac]~=iFac);
end

LevIdx = cell(1,nFac);
nLevRed = nLev;
nLevRed(iFac) = 1;
for i=1:n
    [LevIdx{:}] = ind2sub(nLevRed,i);
    for iL = 1:nL
        LevIdx{iFac} = iLevel(iL);
        df(iL).cells(i) = DF.cells(LevIdx{:});
    end
end

