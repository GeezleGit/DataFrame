function DF = df_normSeriesData(DF,SeriesLabel,Option)
% Perform various normalisation operations on all data cells
%
% ... = df_normData(DF,'optionstring',SeriesLabel)
% optionstring ... 'SUBSTRACTMIN'


nDF = length(DF);
for iDF = 1:nDF
    [nLevels] = size(DF(iDF).cells);
    nGrps = prod(nLevels);
    
    serNr = strcmp(DF(iDF).serieslabel,SeriesLabel);
    serDim = df_getSeriesDim(DF(iDF),SeriesLabel);
    sampD = df_celldim(DF(iDF));
    
    for iGrp = 1:nGrps
        currCellSize = size(DF.cells{iGrp});
        switch Option
            case 'SUBSTRACTMIN'
                repArr         = ones(1,length(currCellSize));
                repArr(serDim) = currCellSize(serDim);
                m              = repmat(min(DF.cells{iGrp},[],serDim),repArr);
                DF.cells{iGrp} = DF.cells{iGrp}-m;
            case 'SCALEDTORANGE'
                repArr         = ones(1,length(currCellSize));
                repArr(serDim) = currCellSize(serDim);
                mn             = repmat(min(DF.cells{iGrp},[],serDim),repArr);
                mx             = repmat(max(DF.cells{iGrp},[],serDim),repArr);
                DF.cells{iGrp} = (DF.cells{iGrp}-mn)./(mx-mn);
            case 'Z-SCORE'
                repArr         = ones(1,length(currCellSize));
                repArr(serDim) = currCellSize(serDim);
                m              = repmat(nanmean(DF.cells{iGrp},serDim),repArr);
                s              = repmat(nanstd(DF.cells{iGrp},0,serDim),repArr);
                DF.cells{iGrp} = (DF.cells{iGrp}-m)./s;
        end
    end
end

