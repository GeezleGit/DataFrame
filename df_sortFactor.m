function DF = df_sortFactor(DF,FactorArray,sortIndex)

% rearrange factors
% DF = df_sortFactor(DF,{'Factor1','Factor2',...,'FactorN'})
% DF = df_sortFactor(DF,{'Factor1','Factor2',...,'FactorN'},[i1, i2, ..., iN])
% DF = df_sortFactor(DF,[i1, i2, ..., iN])
%
% FactorArray ... index vector or cell array of strings

if nargin<3
    sortIndex = [];
end

if numel(DF)>1
    for i=1:numel(DF)
        DF(i) = df_sortFactor(DF(i),FactorArray,sortIndex);
    end
    return;
end
nCells = df_cellsize(DF);
nDims = length(nCells);
nFac = length(DF.factor);

% create sort array
if iscell(FactorArray) && isempty(sortIndex)
    sortArray = zeros(size(FactorArray)).*NaN;
    isblank = cellfun('isempty',FactorArray);
    sortArray(~isblank) = df_findFactor(DF,FactorArray(~isblank));
    isnotfound = ~isblank & isnan(sortArray);
    if all(isnan(sortArray))
        error('Don''t know how to sort factors!!');
    end
else
    sortArray = FactorArray;
end

% process sorting array
nSortFac = length(sortArray);
if nSortFac>nDims
    nDims = nSortFac;
elseif nSortFac<nDims
    sortArray(nSortFac+1:nDims) = NaN;
end
sortArray(isnan(sortArray)) = setdiff(1:nDims,sortArray(~isnan(sortArray)));

DF.factor(sortArray(sortArray>nFac)) = {'dummyfactor'};
DF.level(sortArray(sortArray>nFac)) = {NaN};

% execute sorting
DF.factor = DF.factor(sortArray);
DF.level = DF.level(sortArray);
DF.cells = permute(DF.cells,sortArray);

% replace dummy factors with non found factor names
if iscell(FactorArray) && any(isnotfound)
    DF.factor(isnotfound) = FactorArray(isnotfound);
end