function [DF,nFac,nLev] = df_squeeze(DF,NonTrailingToo)
    % Removes factors with just one level from DataFrame.
    % [DF,nFac,nLev] = squeeze(DF,NonTrailingToo)
    % NonTrailingToo ...
    
    if nargin<2
        NonTrailingToo = false;
    end
    
    nLev = size(DF.cells);
    if length(nLev)==2 && nLev(end)==1;nLev=nLev(1);end
    nFac = length(nLev);
    
    FacLength = length(DF.factor);
    if nFac<FacLength
        DF.factor = DF.factor(1:nFac);
        DF.level = DF.level(1:nFac);
    elseif nFac>FacLength
        error('Factor label don''t match number of dimensions of data cells!');
    end
    
    if nFac==1 && nLev(1)>1
        return;
    elseif nFac==1 && nLev(1)==1
        warning('DataFrame contains only one factor with one level!');
        return;
    elseif nFac==1 && nLev(1)==0
        warning('DataFrame contains only one factor with zero level!');
        return;
    end
    
    isSingle = nLev<=1;
    if ~NonTrailingToo
        isSingle(1:find(nLev>1,1,'last')) = false;
    end
    
    permutevec = [1:nFac];
    permutevec = [permutevec(~isSingle) permutevec(isSingle)];
    DF.cells = permute(DF.cells,permutevec);
    DF.factor(isSingle) = [];
    DF.level(isSingle) = [];
    nLev = size(DF.cells);
    nFac = length(nLev);
end

