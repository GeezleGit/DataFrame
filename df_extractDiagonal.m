function df = df_extractDiagonal(df)

n = numel(df);
for i=1:n
    s1 = df(i).seriesdim==1;
    s2 = df(i).seriesdim==2;
    
    serNum = length(df(i).seriesparam);
    permArray = [1:serNum];
    permArray(serNum) = find(s2);
    permArray(s2) = serNum;
    
    cellSize = size(df(i).cells);
    df(i).cells = cellfun(@extractDiagonal, df(i).cells, repmat(df(i).seriesparam(s1),cellSize), repmat(df(i).seriesparam(s2),cellSize), repmat({permArray},cellSize), 'UniformOutput', false);
    
    df(i).seriesdim = df(i).seriesdim(~s2);
    df(i).seriesparam = df(i).seriesparam(~s2);
    df(i).serieslabel = df(i).serieslabel(~s2);
    
%     df(i).seriesperiod
%     df(i).seriesoffset

end

function C = extractDiagonal(C,par1,par2,permArray)
parNum1 = length(par1);
for ip1 = 1:parNum1
    ip2 = par2==par1(ip1);
    C(ip1,1,:) = C(ip1,ip2,:);
end
C = C(:,1,:);
C = permute(C,permArray);
