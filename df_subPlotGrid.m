function [hA,hF,Ax] = df_subPlotGrid(self,transposeFlag,FigMargin,PlotSpacing,minGrid,maxGrid,offsetGrid,hFig,inAxesFactors,xyzDims,noGridFlag)
% DF_SUBPLOTGRID Create Figures and Subplots from Factors and Series in a DataFrame object
%
% Parameters
% ----------
% transposeFlag: bool
%     swaps the first and second factor
% FigMargin:
%     subplot margins ``[left,right,top,bottom]``, in normalized units
% PlotSpacing:
%     subplot spacing ``[vertical, horizontal]``, in normalized units
% minGrid:
%      
% maxGrid:
%     
% offsetGrid:
%     
% hFig:
%      figure or axes handle to place the plots in
% inAxesFactors:
%      
% xyzDims:
%      series dimension for [x-Axes, y-Axes]
% noGridFlag:
%      instead of creating a grid of factors, create a grid from a single factor 
%
% Examples
% --------
% ::
% 
%     [hA,hF] = df_subPlotGrid(self,transposeFlag,FigMargin,PlotSpacing,minGrid,maxGrid,offsetGrid,hFig,inAxesFactors)
%


if nargin<11
    noGridFlag = false;
end
[nFac,nLev,nEl] = df_size(self);
cellSize = df_cellsize(self);

%% assign factorNRs to one of three groups
if isempty(inAxesFactors)
    facNrInAx = [];
else
    facNrInAx = df_findFactor(DF,inAxesFactors{:});
end
if transposeFlag
    facNrInFig = [2 1];
else
    facNrInFig = [1 2];
end
facNrFigs = setdiff([1:nFac],[facNrInAx facNrInFig]);

%% determine grid size for each figure
if noGridFlag
    verNum = floor(sqrt(cellSize(facNrInFig(1))));
    horNum = ceil(sqrt(cellSize(facNrInFig(1))));
    offsetGrid = [0 0];
else
    verNum = cellSize(facNrInFig(1))+offsetGrid(1);
    horNum = cellSize(facNrInFig(2))+offsetGrid(2);
end
if ~isnan(minGrid(1)), verNum = max([minGrid(1),verNum]);end
if ~isnan(maxGrid(1)), verNum = min([maxGrid(1),verNum]);end
if ~isnan(minGrid(2)), horNum = max([minGrid(2),horNum]);end
if ~isnan(maxGrid(2)), horNum = min([maxGrid(2),horNum]);end

%% create figures
nFig = prod(cellSize(facNrFigs));
if isempty(hFig)
    hFig = figure;
else
    hFig = hFig;
end

dNFig = nFig-length(hFig);
if dNFig>0
    [hFig(length(hFig)+1:length(hFig)+dNFig)] = copyobj(repmat(hFig(end),[1 dNFig]),zeros(1,dNFig));
elseif length(hFig)==nFig
else
    error('Not the right number of figures here!');
end

%% create axes
hA = cell(cellSize);
hF = cell(cellSize);
cellindex = cell(1,length(cellSize));
for iGrp = 1:prod(cellSize)
    [cellindex{:}] = ind2sub(cellSize,iGrp);
    if noGridFlag
        [cGridIndex(1),cGridIndex(2)] = ind2sub([verNum horNum],cellindex{facNrInFig(1)});
    else
        cGridIndex(1) = cellindex{facNrInFig(1)}+offsetGrid(1);
        cGridIndex(2) = cellindex{facNrInFig(2)}+offsetGrid(2);
    end
    iFig = prod(cat(2,cellindex{facNrFigs}));
    if verNum<cGridIndex(1) || horNum<cGridIndex(2)
        hA{cellindex{:}} = [];
        hF{cellindex{:}} = [];
    else
        hA{cellindex{:}} = subaxes(hFig(iFig),[verNum horNum],cGridIndex,PlotSpacing,FigMargin);
        hF{cellindex{:}} = hFig(iFig);
    end
    
    % collect axes handles
    Ax.handle(1,iGrp) = hA{cellindex{:}};
    Ax.isFirstRow(1,iGrp) = false;
    if cellindex{facNrInFig(1)}==1; Ax.isFirstRow(1,iGrp) = true;end
    Ax.isFirstCol(1,iGrp) = false;
    if cellindex{facNrInFig(2)}==1; Ax.isFirstCol(1,iGrp) = true;end
    Ax.isLastRow(1,iGrp) = false;
    if cellindex{facNrInFig(1)}==cellSize(facNrInFig(1)); Ax.isLastRow(1,iGrp) = true;end
    Ax.isLastCol(1,iGrp) = false;
    if cellindex{facNrInFig(2)}==cellSize(facNrInFig(2)); Ax.isLastCol(1,iGrp) = true;end
    Ax.title(1,iGrp) = get(Ax.handle(1,iGrp),'title');
    Ax.xlabel(1,iGrp) = get(Ax.handle(1,iGrp),'xlabel');
    Ax.ylabel(1,iGrp) = get(Ax.handle(1,iGrp),'ylabel');
    
    if length(xyzDims)>0 && any(self.seriesdim==xyzDims(1))
        cXSeriesNr = find(self.seriesdim==xyzDims(1),1);
        if isnumeric(self.seriesparam{cXSeriesNr})
            Ax.prop(1,iGrp).xlabel = self.serieslabel{cXSeriesNr};
            Ax.prop(1,iGrp).xticks = self.seriesparam{cXSeriesNr};
            Ax.prop(1,iGrp).xlim = [min(self.seriesparam{cXSeriesNr}) max(self.seriesparam{cXSeriesNr})];
        else
            Ax.prop(1,iGrp).xlabel = self.serieslabel{cXSeriesNr};
            xNum = length(self.seriesparam{cXSeriesNr});
            Ax.prop(1,iGrp).xticks = [1:xNum]+[-0.5 0.5];
            Ax.prop(1,iGrp).xlim = [1 xNum]+[-0.5 0.5];
        end
    else
        Ax.prop(1,iGrp).xlabel = [];
        Ax.prop(1,iGrp).xticks = [];
        Ax.prop(1,iGrp).xlim = [];
    end
    
    
    if length(xyzDims)>1 && any(self.seriesdim==xyzDims(2))
        cYSeriesNr = find(self.seriesdim==xyzDims(2),1);
        Ax.prop(1,iGrp).ylabel = self.serieslabel{cYSeriesNr};
        Ax.prop(1,iGrp).yticks = self.seriesparam{cYSeriesNr};
        Ax.prop(1,iGrp).ylim = [min(self.seriesparam{cYSeriesNr}) max(self.seriesparam{cYSeriesNr})];
    else
        Ax.prop(1,iGrp).ylabel = [];
        Ax.prop(1,iGrp).yticks = [];
        Ax.prop(1,iGrp).ylim = [];
    end
    
    if nFac>=facNrInFig(1) 
        Ax.prop(1,iGrp).rowFactor = self.factor(facNrInFig(1));
        Ax.prop(1,iGrp).rowLevel = self.level{facNrInFig(1)}(cellindex{facNrInFig(1)});
    else
        Ax.prop(1,iGrp).rowFactor = {};
        Ax.prop(1,iGrp).rowLevel = [];
    end    
    
    if nFac>=facNrInFig(2) && ~isempty(self.factor)
        Ax.prop(1,iGrp).colFactor = self.factor(facNrInFig(2));
        Ax.prop(1,iGrp).colLevel = self.level{facNrInFig(2)}(cellindex{facNrInFig(2)});
    else
        Ax.prop(1,iGrp).colFactor = {};
        Ax.prop(1,iGrp).colLevel = [];        
    end
    
    Ax.prop(1,iGrp).figFactor = self.factor(facNrFigs);
    for iii = 1:length(facNrFigs)
        Ax.prop(1,iGrp).figLevel{1,iii} = self.level{facNrFigs(iii)}(cellindex{facNrFigs(iii)});
    end
    
end





function [newh] = subaxes(h,sizeAxes,index,interspace,edgespace,printsize,printunits)

% [newh] = subaxes(h,sizeAxes,index,interspace,edgespace)
%
% tile a parent axes in an array of new axes
% INPUT:
% h ............ handle of figure or axes
% sizeAxes ..... m by n number of new axes, if scalar then ...
% index ........ subscript [row,col], if than one index pair  matlab returns
%               the area of axes
% interspace .... space between axes in [vertical,horizontal] distance in normalized units;
% edgespace .... space at edges [left,right,top,bottom]
% OUTPUT:
% newh ....... m by n matrix of handles

%% check input
if nargin<7
    printunits = '';
    if nargin<6
        printsize = [];
        if nargin<5
            edgespace = [0 0 0 0];
            if nargin<4
                interspace = [0 0];
                if nargin<3
                    index = [];
        end;end;end;end;end
if length(edgespace)==1;edgespace = ones(1,4).*edgespace;end

%% axes size is scalar
if numel(sizeAxes)==1
    sizeAxes = ones(1,2).*ceil(sqrt(sizeAxes));
end
if numel(index)==1
    [index(1),index(2)] = ind2sub(sizeAxes,index);
end

rowNr = repmat([1:sizeAxes(1)]',1,sizeAxes(2));
colNr = repmat([1:sizeAxes(2)],sizeAxes(1),1);

%% deal with old axes
if strcmp(upper(get(h,'type')),'FIGURE')
    baseUnits = 'normalized';
    basePos = [0 0 1 1];
    hFig = h;
elseif strcmp(upper(get(h,'type')),'AXES')
    baseUnits = 'normalized';
    set(h,'units',baseUnits);
    basePos = get(h,'position');
    hFig = get(h,'parent');
else
    error('Parent object to the subaxes function must be a figure or an axes !');
end

%% get measures
if ~isempty(printunits) & ~isempty(printsize) 
    OldpUnits = get(h,'paperunits');
    set(h,'paperunits',printunits);
    pPos = get(h,'paperposition');
    
    newwidth = printsize(1)/pPos(3);
    newheight = printsize(2)/pPos(4);
    interSpace(1) = interspace(1)/pPos(4);
    interSpace(2) = interspace(2)/pPos(3);
    edgeSpace([1 2]) = edgespace([1 2])./pPos(3);
    edgeSpace([3 4]) = edgespace([3 4])./pPos(4);
    
    set(h,'paperunits',OldpUnits);
    
    x0 = edgeSpace(1);
    y0 = 1-edgeSpace(3);
else
    interSpace = interspace.*[basePos(4) basePos(3)];
    edgeSpace = edgespace.*[basePos(3),basePos(3),basePos(4),basePos(4)];
    newheight =     (basePos(4) - (interSpace(1)*(sizeAxes(1)-1)) - edgeSpace(3) - edgeSpace(4)    ) /sizeAxes(1);
    newwidth =      (basePos(3) - (interSpace(2)*(sizeAxes(2)-1)) - edgeSpace(1) - edgeSpace(2)    ) /sizeAxes(2);
    
    x0 = basePos(1) + edgeSpace(1);
    y0 = basePos(2) + basePos(4) - edgeSpace(3);    
end

% make matrix for lower left position
X = x0 + (colNr-1).*newwidth + (colNr-1).*interSpace(2);
Y = y0 - (rowNr*newheight) - ((rowNr-1).*interSpace(1));

if isempty(index)
    % make all at once
     for m = 1:sizeAxes(1)
          for n = 1:sizeAxes(2)
               newh(m,n) = axes('parent',hFig,'units',baseUnits,'position',[X(m,n) Y(m,n) newwidth newheight]);
          end
     end
else
    % make selected axes
    % index can define an area !
     topI = min(index(:,1));
     bottomI = max(index(:,1));
     leftI = min(index(:,2));
     rightI = max(index(:,2));
     
     newPos(1) = X(bottomI,leftI);
     newPos(2) = Y(bottomI,leftI);
     newPos(3) = X(bottomI,rightI) - X(bottomI,leftI) + newwidth;
     newPos(4) = Y(topI,leftI) - Y(bottomI,leftI) + newheight;
     
     newh = axes('parent',hFig,'units',baseUnits,'position',newPos);
end


