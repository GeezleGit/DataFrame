function [p,h,stats,indexPairs] = df_testMultiLevel(self,Y,FunName,FunInput,FactorName,LevelName)
% compares two levels of a DataFrame
% FunName ......
% FunInput ..... 
% FactorName ... FactorName
% LevelName .... LevelName LevelIndex

[nFac,nLev] = df_getsize(self);
LevNum = cellfun('length',self.level);
for i=1:length(LevNum)
    LevelInd{i} = 1:LevNum(i);
end

if isempty(Y)
    scalarMode = false;
else
    scalarMode = true;
end

% make comparison pairs
if nargin==5
    comFacNr = find(strcmp(self.factor,FactorName));
    grpFacNr = setxor([1:length(self.factor)],comFacNr);
    
    if scalarMode
        indexPairs = df_levelIndexList(self, comFacNr, LevelInd(comFacNr), grpFacNr, LevelInd(grpFacNr));
    else
        indexPairs = df_levelIndexPairs(self, comFacNr, LevelInd(comFacNr), grpFacNr, LevelInd(grpFacNr));
    end

elseif nargin==6
    [comLevNr,comFacNr] = df_findFactorLevel(self,FactorName,LevelName);
    if isnumeric(comLevNr);comLevNr = {comLevNr};end
    grpFacNr = setxor([1:length(self.factor)],comFacNr);
    if scalarMode
        indexPairs = df_levelIndexList(self, comFacNr, comLevNr, grpFacNr, LevelInd(grpFacNr));
    else
        indexPairs = df_levelIndexPairs(self, comFacNr, comLevNr, grpFacNr, LevelInd(grpFacNr));
    end    
    
else
    if scalarMode
        indexPairs = df_levelIndexList(self);
    else
        indexPairs = df_levelIndexPairs(self);
    end    
    
end

if isnumeric(indexPairs)
    indexPairs = {indexPairs};
end
nCompGrps = size(indexPairs);
p     = cell(nCompGrps);
h     = cell(nCompGrps);
stats = cell(nCompGrps);

for iGrp=1:prod(nCompGrps)
    for iCom = 1:size(indexPairs{iGrp},3)
        iCell1 = num2cell(indexPairs{iGrp}(1,:,iCom));
        
        % pick data arrays
        X = self.cells{iCell1{:}};
        if ~scalarMode
            iCell2 = num2cell(indexPairs{iGrp}(2,:,iCom));
            Y = self.cells{iCell2{:}};
        end
        
        % check data arrays
        if isempty(X)||isempty(Y)||all(isnan(X))||all(isnan(Y))
            p{iGrp}(iCom) = NaN;
            h{iGrp}(iCom) = false;
            stats{iGrp}{iCom} = [];
            continue;
        end
        
        iix = ~isnan(X);
        iiy = ~isnan(Y);
        
        switch FunName
            case 'signrank'
                if numel(Y)==1
                    [p{iGrp}(iCom),h{iGrp}(iCom),stats{iGrp}{iCom}] = signrank(X(iix),Y(iiy),FunInput{:});
                else
                    [p{iGrp}(iCom),h{iGrp}(iCom),stats{iGrp}{iCom}] = signrank(X(iix&iiy),Y(iix&iiy),FunInput{:});
                end
            case 'signtest'
                if numel(Y)==1
                    [p{iGrp}(iCom),h{iGrp}(iCom),stats{iGrp}{iCom}] = signtest(X(iix),Y(iiy),FunInput{:});
                else
                    [p{iGrp}(iCom),h{iGrp}(iCom),stats{iGrp}{iCom}] = signtest(X(iix&iiy),Y(iix&iiy),FunInput{:});
                end
            case 'ttest'
                if numel(Y)==1
                    [h(iGrp),p(iGrp),ci,stats{iGrp}{iCom}] = ttest(X(iix),Y(iiy),FunInput{:});
                else
                    [h(iGrp),p(iGrp),ci,stats{iGrp}{iCom}] = ttest(X(iix&iiy),Y(iix&iiy),FunInput{:});
                end
            case 'ttest2'
                [h{iGrp}(iCom),p{iGrp}(iCom),ci,stats{iGrp}{iCom}] = ttest2(X(iix),Y(iiy),FunInput{:});
            case 'ranksum'
                [p{iGrp}(iCom),h{iGrp}(iCom),stats{iGrp}{iCom}] = ranksum(X(iix),Y(iiy),FunInput{:});
        end
    end
end


