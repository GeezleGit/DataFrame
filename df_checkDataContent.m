function x = df_checkDataContent(DF,TestModes)
    % check content of each cell
    % x = checkDataContent(DataFrame)
    % counts the non-NaN elements in each data.cell
    % x = checkDataContent(DataFrame,{'TestMode1' ... 'TestModeN'})
    % check each cell for TestModeCriterion
    % TestModes ... {'NaN'} {'empty'} or {value}
    if nargin<2
        x = zeros(size(DF.cells));
        for k = 1:numel(DF.cells)
            x(k) = sum(~isnan(DF.cells{k}));
        end
    else
        x = false(size(DF.cells));
        for i = 1:length(TestModes)
            if isnumeric(TestModes{i})
                for k = 1:numel(DF.cells)
                    x(k) = x(k) || all(DF.cells{k}==TestModes{i});
                end
            else
                switch upper(TestModes{i})
                    case 'NAN'
                        cx = false(size(x));
                        for k = 1:numel(DF.cells)
                            if any(isnan(DF.cells{k}(:)))
                                cx(k) = all(isnan(DF.cells{k}(:)));
                            end
                        end
                        x = x | cx;
                    case 'EMPTY'
                        x = x | cellfun('isempty',DF.cells);
                end
            end
        end
    end
end

