function [foundPar, allPar, DFarr] = df_ArrayCheckFactor(DFarr,factorLabel,minN)

N = numel(DFarr);

iFac = zeros(1,N);
allPar = [];
for i=1:N
    iFac(i) = find(strcmp(factorLabel, DFarr(i).factor));
    allPar = union(allPar, DFarr(i).level{iFac(i)}(:));
end

foundPar = false(size(allPar,1),N);
for i = 1:N
    iFoundPar = ismember(allPar, DFarr(i).level{iFac(i)}(:));
    foundPar(iFoundPar,i) = true;
end

if nargout>2
    isOK = sum(foundPar,2)>=minN;
    for i = 1:N
        DFarr(i) = df_extractFactorLevel(DFarr(i),factorLabel,allPar(isOK));
    end
end
