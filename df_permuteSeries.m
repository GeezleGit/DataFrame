function [self,permarray] = df_permuteSeries(self,varargin)

% Permute arrays within data cells
% self = df_permuteSeries(self,'SeriesLabel',targetDim)
% self = df_permuteSeries(self,{'SeriesLabel1' 'SeriesLabel2' ...},SqueezeFlag)

nDF = numel(self);

for iDF = 1:nDF
    nds = cellfun('ndims',self(iDF).cells);
    nds = max(nds(:));
    
    if iscell(varargin{1})
        SeriesName = varargin{1};
        SqueezeFlag = varargin{2};
        [serFound,iSer] = ismember(SeriesName,self(iDF).serieslabel);
        if SqueezeFlag
            nonSingle = getCellsNonSingleton(self(iDF).cells,1:nds);
            permarray = self(iDF).seriesdim(iSer(serFound));
            permarray = [permarray setdiff(nonSingle,permarray)];
            permarray = [permarray setdiff(1:nds,permarray)];
        else
            permarray = zeros(1,nds).*NaN;
            permarray(find(serFound)) = self(iDF).seriesdim(iSer(serFound));
            permarray(isnan(permarray)) = setdiff(1:nds,self(iDF).seriesdim(iSer(serFound)));
        end
    elseif ischar(varargin{1})
        SeriesName = varargin{1};
        targetDim = varargin{2};
        iSer = strcmp(self(iDF).serieslabel,SeriesName);
        dSer = self(iDF).seriesdim(iSer);
        permarray = 1:nds;
        permarray = circshift(permarray,[1 targetDim-dSer]);
    elseif isnumeric(varargin{1})
        iSer = varargin{1};
        permarray = iSer;
    end
    
    
    
    nGrps = numel(self(iDF).cells);
    for iGrp = 1:nGrps
        self(iDF).cells{iGrp} = permute(self(iDF).cells{iGrp},permarray);
    end
    
    for i=1:length(self(iDF).seriesdim)
        self(iDF).seriesdim(i) = find(self(iDF).seriesdim(i)==permarray);
    end
end


function dims = getCellsNonSingleton(c,dims)
nonSgl = false(1,length(dims));
for iD = 1:length(dims)
    cLen = cellfun('size',c,dims(iD));
    nonSgl(iD) =  any(cLen(:) > 1);
end
dims(~nonSgl) = [];