function [iLevel,FacNr] = df_findFactorLevel(DF,varargin)
% DF_FINDFACTORLEVEL finds indices to data cells of a given level combination
%
% Example
% -------
% ::
% 
%     [...] = findFactorLevel(DF, ...
%         'Fac1',{'F1Level1' 'F1Level2' ... 'F1LevelN'}, ... 
%         'Fac2',{'F2Level1' 'F2Level2' ... 'F2LevelN'}, ...
%         'FacN',{'FNLevel1' 'FNLevel2' ... 'FNLevelN'})

iLevel = [];
FacNr = [];

% check for Factor/Levelarray pairs
if rem(length(varargin),2)~=0;error('Input needs to be Factor/Level pairs!');end
nFacIn = length(varargin)/2;

[nFac,nLev,nEl] = df_getsize(DF);

iLevel = cell(1,nFacIn);
FacNr = zeros(1,nFacIn).*NaN;

for iFacIn = 1:nFacIn
    iscFactor = strcmp(varargin{(iFacIn-1)*2+1},DF.factor);
    if ~any(iscFactor);return;end
    FacNr(iFacIn) = find(iscFactor);
    
    if isnumeric(DF.level{FacNr(iFacIn)})
        [TF,iLevel{iFacIn}] = ismember(varargin{iFacIn*2},DF.level{FacNr(iFacIn)});
    elseif iscell(DF.level{FacNr(iFacIn)})
        if isnumeric(DF.level{FacNr(iFacIn)}{1})
            [TF,iLevel{iFacIn}] = ismember(varargin{iFacIn*2},cat(2,DF.level{FacNr(iFacIn)}{:}));
        else
            [TF,iLevel{iFacIn}] = ismember(varargin{iFacIn*2},DF.level{FacNr(iFacIn)});
        end
    end
    
    if ~any(TF)
        iLevel{iFacIn} = {[]};
        warning('Can''t find level for factor "%s" !!!',varargin{(iFacIn-1)*2+1});
    end
end
if nFacIn ==1
    iLevel = iLevel{1};
end

