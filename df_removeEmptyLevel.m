function [DF,Removelevel] = df_removeEmptyLevel(DF)
    % Removes levels (dim-index) without data (emptycells) of each factor(dim).
    % [X,DF.level,Removelevel] = cell_MissData2(X,DF.level)
    
    nLevel = size(DF.cells);
    while nLevel(end)==1
        nLevel(end)=[];
        if length(nLevel)==1;break;end
    end% remove trailing singleton dims
    nFac = length(nLevel);
    
    %% detecting empty levels
    EmptyCells = cellfun('isempty',DF.cells);
    for iFac = 1:nFac
        exlevel = EmptyCells;
        for i = 1:nFac
            if i==iFac;continue;end% leave out iFac dimension
            exlevel = all(exlevel,i);
        end
        Removelevel{iFac} = find(exlevel);
    end
    
    %% remove DF.level
    for iFac = 1:nFac
        DF.level{iFac}(Removelevel{iFac}) = [];
    end
    
    %% get elements to remove from DF.cells
    curridx = cell(1,nFac);
    removeidx = false(1,numel(DF.cells));% logical element for each cell
    for i=1:numel(DF.cells)
        [curridx{:}] = ind2sub(nLevel,i);
        for iFac = 1:nFac% check each subscript index if it's part of the Removelevel
            if isempty(Removelevel{iFac})
                continue;
            elseif ismember(curridx{iFac},Removelevel{iFac})
                removeidx(i) = true;
                break;
            else
                removeidx(i) = false;
            end
        end
    end
    
    %% remove from DF.cells
    DF.cells = DF.cells(:);
    DF.cells(removeidx) = [];
    reshapeIdx = cellfun('length',DF.level);
    if length(reshapeIdx)==1
        reshapeIdx = [reshapeIdx 1];
    end
    DF.cells = reshape(DF.cells,reshapeIdx);
end
