function varargout = df_collapseSeries(self,varargin)

% Collapse series by averaging (df_mean) across samples.
% Variance data is from last collapse of a series.
%
% [outMean,outSTD,outSE] = df_collapseSeries(self,'series1',seriesParam1,'series2',seriesParam2, ...)
%
% seriesParam1 ... []    -> collapse across whole range
%                  [nx2] -> collapse across n ranges [[i,1]:[i,2]]
%                  [mx1] -> collapse across index of m samples
%                  logical [m,n] -> m samples, n ranges

numCollaps = length(varargin)/2;
squeezeFlag = false;
iRange = 1;

for iColl = 1:numCollaps
    serieslabel = varargin{iColl*2-1};
    seriesrange = varargin{iColl*2};
    
    if iColl==1
        inDF = self;
    else
        inDF = outDF{1};
    end
    
    if iColl==numCollaps
        outDF = cell(1,3);
    else
        outDF = cell(1,1);
    end
    
    if isempty(seriesrange)
        [outDF{:}] = df_mean(inDF,serieslabel);
        
    elseif isnumeric(seriesrange) && size(seriesrange,2)==2
        cSeriesParam = df_getSeries(inDF,serieslabel);
        index = find(cSeriesParam<seriesrange(iRange,1) | cSeriesParam>seriesrange(iRange,2));
        inDF = df_removeSeries(inDF,serieslabel,index,squeezeFlag);
        [outDF{:}] = df_mean(inDF,serieslabel);
        
    elseif isnumeric(seriesrange) && size(seriesrange,2)==1
        index = seriesrange;
        inDF = df_removeSeries(inDF,serieslabel,index,squeezeFlag);
        [outDF{:}] = df_mean(inDF,serieslabel);
        
    elseif islogical(seriesrange)
        index = find(seriesrange(:,iRange));
        inDF = df_removeSeries(inDF,serieslabel,index,squeezeFlag);
        [outDF{:}] = df_mean(inDF,serieslabel);
    end
    
end

varargout{1} = df_clean(outDF{1});
varargout{2} = df_clean(outDF{2});
varargout{3} = df_clean(outDF{3});