function DF = DataFrame(x,varargin)
% DataFrame constructor, enter properties as field value pairs
%
% Examples
% --------
% set fields of DataFrame object::
% 
%     DF = DataFrame(DataFrame,fieldname1,fieldvalue1, ...)
% 
% copy fields of structure to DataFrame::
%
%     DF = DataFrame(DataFrame,structure)
% 
%
% @DataFrame properties::
%
%     s.tag            = '' label the data frame
%     ----- data ------------------------
%     s.factor         = {};
%     s.level          = {};
%     s.cells          = {};
%     ----- arrays within cells ---------
%     s.seriesdim      = [];
%     s.seriesparam    = {};
%     s.serieslabel    = {};
%     s.seriesperiod   = [];
%     s.seriesoffset   = [];
%     ----- data container --------------
%     s.userdata       = [];


s.tag = ''; % label the data frame

% data
s.factor = {};
s.level = {};
s.cells = {};

% within cells
s.seriesdim = [];
s.seriesparam = {};
s.serieslabel = {};
s.seriesperiod = [];
s.seriesoffset = [];

s.userdata = [];


%% react to different types of INPUT
if nargin == 0 || isempty(x)
   % CREATE an empty object
   DF = class(s,'DataFrame');
   
elseif isa(x,'struct')
   % CONVERT structure to an object
   xFields = fieldnames(x);
   sFields = fieldnames(s);
   nrFields = length(sFields);
   for fieldNr = 1:nrFields
	   indexInx = strmatch(sFields{fieldNr},xFields,'exact');
	   if ~isempty(indexInx)
           s.(sFields{fieldNr}) = x.(xFields{indexInx});
		   %s = setfield(s,sFields{fieldNr},getfield(x,xFields{indexInx}));
	   end   
   end
   DF = class(s,'DataFrame');

elseif isa(x,'DataFrame')
    nInputFields = length(varargin);
    
    if nInputFields>0 && rem(nInputFields,2)==0
        f1 = fieldnames(s);
        f2 = varargin(1:2:end);
        v2 = varargin(2:2:end);
        for j = 1:numel(x)
            for i=1:length(f2)
                if ~any(strcmp(f2{i},f1));continue;end
                x(j).(f2{i}) = v2{i};
            end
        end
    elseif nInputFields==0
    elseif nInputFields>0 && rem(nInputFields,2)~=0
        error('DataFrame: Input must be field/value pairs!!');
    end
    
    DF = x;

else
   s = class(s,'DataFrame');
   error(s,'No such constructor')
   
end
