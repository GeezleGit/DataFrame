function DF = df_addFactorLevel(DF,cFac,LevelName)
    nNew = length(LevelName);
    iFac = df_findFactor(DF,cFac);
    nLevel = cellfun('length',DF.level);
    iLevel = nLevel(iFac)+[1:nNew];
    nFac = length(nLevel);
    
    if nFac == 1
        permindex = [1 2];
    else
        permindex = 1:nFac;
    end
    cPermindex = circshift(permindex,[0 -iFac+1]);
    DF.cells = permute(DF.cells,cPermindex);
    DF.cells(iLevel,:) = cell(1,1);
    cPermindex = circshift(permindex,[0 iFac-1]);
    DF.cells = permute(DF.cells,cPermindex);
    
    DF.level{iFac}(iLevel) = LevelName;
end

