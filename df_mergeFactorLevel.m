function DF = df_mergeFactorLevel(DF,cFac,cLevel,NewLevelName,catdim)

% Collapse cell content of multiple factors

nDF = length(DF);
for iDF = 1:nDF
    [iLevel,iFac] = df_findFactorLevel(DF(iDF),cFac,cLevel);
    if isempty(iLevel) || (iscell(iLevel)&&cellfun('isempty',iLevel))
        continue;
    end
    [DF(iDF),cellidx] = df_collapse(DF(iDF),iFac,iLevel,catdim);
    
    ExistLevelName = DF(iDF).level{iFac}(cellidx(cellidx~=0));
    if isempty(NewLevelName)
        DF(iDF).level{iFac}(cellidx==0) = cLevel(1);
    elseif ischar(NewLevelName) && isnumeric(ExistLevelName)
        DF(iDF).level{iFac} = cell(size(cellidx));
        DF(iDF).level{iFac}(cellidx~=0) = num2cell(ExistLevelName);
        DF(iDF).level{iFac}(cellidx==0) = {NewLevelName};
    elseif ischar(NewLevelName) && iscell(ExistLevelName)
        DF(iDF).level{iFac} = cell(size(cellidx));
        DF(iDF).level{iFac}(cellidx~=0) = ExistLevelName;
        DF(iDF).level{iFac}(cellidx==0) = {NewLevelName};
    elseif iscell(NewLevelName) && iscell(ExistLevelName)
        DF(iDF).level{iFac} = cell(size(cellidx));
        DF(iDF).level{iFac}(cellidx~=0) = ExistLevelName;
        DF(iDF).level{iFac}(cellidx==0) = NewLevelName;
    elseif isnumeric(NewLevelName) && isnumeric(ExistLevelName)
        DF(iDF).level{iFac} = zeros(size(cellidx))*NaN;
        DF(iDF).level{iFac}(cellidx~=0) = ExistLevelName;
        DF(iDF).level{iFac}(cellidx==0) = NewLevelName;
    else
        error('Levelnames are inconsistent!');
    end
end
end
