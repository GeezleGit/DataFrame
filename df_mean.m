function varargout = df_mean(self,seriesLabel)
% DF_MEAN computes mean across samples/series
%
% Example
% -------
% ::
% 
%     % mean across samples
%     ... = df_mean(DataFrame)
%    
%     % mean across series calles 'SeriesLabel'
%     ... = df_mean(DataFrame,'SeriesLabel')
%     
%     % use 'samples' to average across non series dimensions
%     ... = df_mean(DataFrame,'samples')
%     
%     [mean_DataFrame,std_DataFrame,se_DataFrame] = df_mean(...)
% 


nGrps = numel(self.cells);
[sampD,seriesD] = df_celldim(self);

if nargin<2 || isempty(seriesLabel) || strcmpi(seriesLabel,'samples')
    MeanDim = sampD;
else
    MeanDim = self.seriesdim(strcmp(self.serieslabel,seriesLabel));
end

df = repmat(self,[1 3]);
for iGrp = 1:nGrps
    
    N = size(self.cells{iGrp},MeanDim)-sum(isnan(self.cells{iGrp}),MeanDim);
    
    df(1).cells{iGrp} = mean(self.cells{iGrp},MeanDim,'omitnan');
    df(2).cells{iGrp} = std(self.cells{iGrp},0,MeanDim,'omitnan');    
    df(3).cells{iGrp} = std(self.cells{iGrp},0,MeanDim,'omitnan') ./ N;
end

for ii = 1:3        
    df(ii).seriesparam(df(ii).seriesdim==MeanDim) = {[NaN]};
end

% construct output
if nargout==3
    varargout{1} = df(1);
    varargout{2} = df(2);
    varargout{3} = df(3);
elseif nargout==1
    varargout{1} = df(1);
end