.. @DataFrame documentation master file, created by
   sphinx-quickstart on Sun Jul 21 19:34:04 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to @DataFrame's documentation!
======================================

@DataFrame is class build around a multidimensional cell array containing multidimensional numerical arrays. 

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   cookbook
   methods
   reference

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. todolist::