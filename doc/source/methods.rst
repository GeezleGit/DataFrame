Methods
=======

.. highlight:: matlab

.. currentmodule:: @DataFrame

Extractions
-----------

- :func:`df_extractDataPoints`
- :func:`df_extractFactorLevel`
- :func:`df_extractMinSamples`
- :func:`df_extractSeries`

Permutations
------------

- :func:`df_Array2Factor`
- :func:`df_Array2Samples`
- :func:`df_Array2Series`
- :func:`df_Level2Array`
- :func:`df_Level2Series`
- :func:`df_Series2Array`
- :func:`df_Series2Level`
- :func:`df_Series2Samples`
- :func:`df_level2Factor`

Data-series
-----------

Data dimensions within cells which are not repetitions (trials) are referred to as series. The following methods manipulate data-series.

- :func:`df_setSeriesParam`
- :func:`df_getSeries`
- :func:`df_getSeriesDim`
- :func:`df_getSeriesLabel`