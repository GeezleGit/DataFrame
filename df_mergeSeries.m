function self = df_mergeSeries(self,seriesLabel,extractType,extractParam)
nDF = length(self);
for iDF = 1:nDF
    self(iDF) = df_extractSeries(self(iDF),seriesLabel,extractType,extractParam);
    self(iDF) = df_Series2Samples(self(iDF),seriesLabel);
end