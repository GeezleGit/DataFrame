function self = df_countData(self,Operation,RefValue,seriesLabel)

% counts samples
% self = df_countData(self,Operation,RefValue)
%
% operation ... 'n','<','>','<=','>='

if nargin<4
    seriesLabel = '';
    if nargin<3
        RefValue = 0;
        if  nargin<2
            Operation = 'n';
        end;end;end

n = numel(self.cells);

if isempty(seriesLabel)
    sumDim = df_celldim(self);
else
    sumDim = df_findSeriesDim(self,seriesLabel);
end

switch Operation
    case '<'
        for i = 1:n
            self.cells{i} = sum(self.cells{i}<RefValue,sumDim);
        end
    case '>'
        for i = 1:n
            self.cells{i} = sum(self.cells{i}>RefValue,sumDim);
        end
    case '<='
        for i = 1:n
            self.cells{i} = sum(self.cells{i}<=RefValue,sumDim);
        end
    case '>='
        for i = 1:n
            self.cells{i} = sum(self.cells{i}>=RefValue,sumDim);
        end
    case 'n'
        for i = 1:n
            self.cells{i} = size(self.cells{i},sumDim);
        end
    otherwise
end

