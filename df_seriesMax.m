function [dfPar,dfVal] = df_seriesMax(self,seriesID,parIndexOutput,subMode,subPar)

% Returns maximum within data series
% [dfPar,dfVal] = df_seriesMax(self,seriesID,parIndexOutput)
% 
% seriesID ......... label, dimension
% parIndexOutput ... flag to return parameter index instead of value
%
% dfPar ... DataFrame containing parameter value at maximum
% dfVal ... DataFrame containing maximum value

if nargin<4
    subMode = 'none';
    subPar = [];
end
    
if ischar(seriesID)
    serNr = strcmp(self.serieslabel,seriesID);
elseif isnumeric(seriesID)
    serNr = self.seriesdim==seriesID;
end

sP = self.seriesparam{serNr};
switch subMode
    case 'BOUNDS'
        i = find(sP>=subPar(1) & sP<=subPar(2));
    case 'WINDOW'
        i = find(sP>subPar(1) & sP<subPar(2));
    case 'BOUNDWINDOW'
        i = find(sP>=subPar(1) & sP<subPar(2));
    case 'WINDOWBOUND'
        i = find(sP>subPar(1) & sP<=subPar(2));
    otherwise
        i = [];
end

dfPar = self;
dfVal = self;

nGrp = numel(self.cells);
for iGrp = 1:nGrp
    
    if strcmpi(subMode, 'none')
        [dfVal.cells{iGrp},dfPar.cells{iGrp}] = max(self.cells{iGrp},[],self.seriesdim(serNr));
    else
        if sum(size(self.cells{iGrp})>1)>1
            error('This currently only works for vectors in cells!!!');
        else
            [dfVal.cells{iGrp},dfPar.cells{iGrp}] = max(self.cells{iGrp}(i),[],self.seriesdim(serNr));
            dfPar.cells{iGrp} = i(dfPar.cells{iGrp});
        end
    end
    if ~parIndexOutput
        dfPar.cells{iGrp}(:) = dfPar.seriesparam{serNr}(dfPar.cells{iGrp});
    end
end

dfVal = df_removeSeries(dfVal,dfVal.serieslabel{serNr},[],false);
dfPar = df_removeSeries(dfPar,dfPar.serieslabel{serNr},[],false);



