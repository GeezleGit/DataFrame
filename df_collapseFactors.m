function self = df_collapseFactors(self,Factornames)
% collapse across factors

if ischar(Factornames)
    Factornames = {Factornames};
end
[nFac,nLev] = df_size(self);
[sampD,seriesD] = df_celldim(self);

nColl = length(Factornames);
collFacNr = df_findFactor(self,Factornames);
catDim = repmat(sampD,[1 nColl]);

for i=1:nColl
    collLevelNrs{i} = [1:nLev(collFacNr(i))];
end

self = df_collapsemulti(self,collFacNr,collLevelNrs,catDim,false);

permarray = [setdiff(1:nFac,collFacNr) collFacNr];

if length(permarray)>1
    self.cells = permute(self.cells,permarray);
    self.factor(collFacNr) = [];
    self.level(collFacNr) = [];
end

