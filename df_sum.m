function self = df_sum(self,seriesLabel)

% computes sum across samples/series
%
% ... = df_sum(DataFrame)
% mean across samples
%
% ... = df_sum(DataFrame,'SeriesLabel')
% mean across series
%
% DataFrame = df_sum(...)
% 

nGrps = numel(self.cells);
[sampD,seriesD] = df_celldim(self);

if nargin<2 || isempty(seriesLabel)
    SumDim = sampD;
else
    SumDim = self.seriesdim(strcmp(self.serieslabel,seriesLabel));
end

for iGrp = 1:nGrps
    self.cells{iGrp} = sum(self.cells{iGrp},SumDim);
end

