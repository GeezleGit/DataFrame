function varargout = df_circmean(self,seriesLabel)

% computes mean across samples/series
%
% ... = df_mean(DataFrame)
% mean across samples
% DataFrame(1) = phase
% DataFrame(2) = radius [default=1]
%
% ... = df_mean(DataFrame,'SeriesLabel')
% mean across series calles 'SeriesLabel'
% use 'samples' to average across non series dimensions
%
% [mean_DataFrame,std_DataFrame,se_DataFrame] = df_mean(...)
% 

nGrps = numel(self(1).cells);
[sampD,seriesD] = df_celldim(self(1));

if length(self)==1
    self(2) = self(1);
    for iGrp=1:nGrps
        self(2).cells{iGrp}(:) = 1;
    end
end

if nargin<2 || isempty(seriesLabel) || strcmpi(seriesLabel,'samples')
    MeanDim = sampD;
else
    MeanDim = self(1).seriesdim(strcmp(self(1).serieslabel,seriesLabel));
end

for iGrp = 1:nGrps
    [self(1).cells{iGrp},csize] = dim2front(self(1).cells{iGrp},MeanDim,true);
    [self(2).cells{iGrp},csize] = dim2front(self(2).cells{iGrp},MeanDim,true);
    [self(1).cells{iGrp},self(2).cells{iGrp}] = pol2cart(self(1).cells{iGrp},self(2).cells{iGrp});
    self(1).cells{iGrp} = agmean(self(1).cells{iGrp},[],1);
    self(2).cells{iGrp} = agmean(self(2).cells{iGrp},[],1);
    csize(1) = 1;
    [self(1).cells{iGrp},self(2).cells{iGrp}] = cart2pol(self(1).cells{iGrp},self(2).cells{iGrp});
    self(1).cells{iGrp} = reshape(self(1).cells{iGrp},csize);
    self(2).cells{iGrp} = reshape(self(2).cells{iGrp},csize);
    self(1).cells{iGrp} = dim2front(self(1).cells{iGrp},MeanDim,false);
    self(2).cells{iGrp} = dim2front(self(2).cells{iGrp},MeanDim,false);
end

self(1).seriesparam(self(1).seriesdim==MeanDim) = {[NaN]};
self(2).seriesparam(self(2).seriesdim==MeanDim) = {[NaN]};

% construct output
if nargout==1
    varargout{1} = self(1);
elseif nargout==2
    varargout{1} = self(1);
    varargout{2} = self(2);
end
    