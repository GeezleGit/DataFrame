function self = df_clean(self,varargin)
% removes single-level factors and series and non-existing data
% self = df_clean(self,'factor','series','data')
% self = df_clean(self,[true true true])

if numel(self)>1
    for i=1:numel(self)
        self(i) = df_clean(self(i),varargin);
    end
    return;
end

%% check input
removeFactor = true;
removeSeries = true;
removeData = true;
if nargin==2 && islogical(varargin{1})
    removeFactor = varargin{1}(1);
    removeSeries = varargin{1}(2);
    removeData = varargin{1}(3);
elseif nargin>1 && all(cellfun('isclass',varargin,'char'))
    if any(strcmpi(varargin,'factor'));removeFactor = true;else;removeFactor = false;end
    if any(strcmpi(varargin,'series'));removeSeries = true;else;removeSeries = false;end
    if any(strcmpi(varargin,'data'));removeData = true;else;removeData = false;end
end    
    
%% remove singleton factors
if removeFactor
    nLev = cellfun('length',self.level);
    iLev = nLev==1;
    nSize = size(self.cells);
    if any(iLev)
        iLev = find(iLev);
        self = df_removeFactor(self,iLev);
%         if all(nSize(iLev)==1)
%             self = df_removeFactor(self,iLev);
%         else
%             error('Inconsistent level information!!!');
%         end
    end
    if isempty(self.factor) || (length(self.factor)==1 && cellfun('isempty',self.factor))
        self.factor = {'nofactor'};
        self.level = {[0]};
    end
end

%% remove singleton data series
% and squeeze data cells
if removeSeries
    nPar      = cellfun('length',self.seriesparam);
    maxParDim = max(self.seriesdim);
    if isempty(maxParDim);maxParDim=0;end
    cellDim   = cellfun('ndims',self.cells);
    cellDim(cellDim<maxParDim) = maxParDim;
    
    nCellArray = zeros(numel(cellDim),max(cellDim(:)));
    for i=1:max(cellDim(:))
        nCellArray(:,i) = cellfun('size',self.cells(:),i);
    end
    
    iSer = any(nCellArray(:,self.seriesdim)<=1,1);
    
    if any(iSer)
        squeezeFlag = true;
        for cSer = self.serieslabel(iSer) 
            self = df_removeSeries(self,cSer{1},[],squeezeFlag);
        end
    elseif ~any(iSer) && ~isempty(self.seriesdim)
        
        
    elseif ~any(iSer) && isempty(self.seriesdim)
        for i=1:numel(self.cells)
            %self.cells{i} = squeeze(self.cells{i});
            self.cells{i} = self.cells{i}(:);
        end
    end
end

%% remove data cells
if removeData
    iCells = cellfun('isempty',self.cells);
    nCells = size(self.cells);
    nFactor = length(self.factor);
    iFactor = false(1,nFactor);
    for i = 1:ndims(self.cells)
        if all(iCells,i)
            iFactor(i) = true;
        end
    end
    if any(iFactor)
        self = df_removeFactor(self,iLev);
    end
end


