function self = df_applyFunToCells(self,funHandle)

n = numel(self.cells);
for i=1:n
    self.cells{i} = funHandle(self.cells{i});
end