function [newArray,cLevels] = df_Level2Array(self,factorLabel)
% Extract levels of a factor as an array of DataFrames
%
% Parameters
% ----------
% factorLabel: char
%
% Examples
% --------
% ::
% 
%     [newArray,cLevels] = df_Level2Array(self,factorLabel)

nDF = numel(self);
for iDF = 1:nDF
    
    iFactor = strcmp(self(iDF).factor,factorLabel);
    cLevels = self(iDF).level{iFactor};
    nLevels = length(cLevels);
    newArray(iDF,:) = repmat(DataFrame(),1,nLevels);
    for ip = 1:length(cLevels)
        newArray(iDF,ip) = df_extractFactorLevel(self(iDF),factorLabel,cLevels(ip));
        newArray(iDF,ip) = df_removeFactor(newArray(iDF,ip),factorLabel);
    end

end
