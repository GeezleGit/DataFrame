function varargout = df_getFactor(self,f)

% factorarray = df_getFactor(DF)
%
% [iFac,lev] = df_getFactor(DF,f)
% f ... {''} or '' factorname
%
% [iFac,lev] = df_getFactor(DF,'factorName')
% [iFac,lev] = df_getFactor(DF,{'factorName1' ... 'factorNameN'})
% [FacName,lev] = df_getFactor(DF,[factorIndex1 ... factorIndexN])

if nargin==1
    varargout{1} = self.factor;
    varargout{2} = self.level;
    return;
end

if ischar(f);f = {f};end


if iscell(f)
    nf = length(f);
    iFac = zeros(1,nf).*NaN;
    lev = cell(1,nf);
    for i = 1:nf
        isf = strcmp(f{i},self.factor);
        if any(isf);
            iFac(i) = find(isf);
            lev(i) = self.level(isf);
        end
    end
elseif isnumeric(f)
    nf = length(f);
    iFac = cell(1,nf);
    lev = cell(1,nf);
    for i = 1:nf
        iFac(i) = self.factor(f(i));
        lev(i) = self.level(f(i));
    end
end

varargout{1} = iFac;
varargout{2} = lev;

