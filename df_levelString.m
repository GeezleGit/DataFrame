function levelString = df_levelString(DF,levelIndex,formatStr,delimiter,levelOnly)

if nargin<4
    delimiter = ' ';
    if nargin<3
        formatStr = '$F:$L';
    end;end

[nLevs,nFacs] = size(levelIndex);
levelString = cell(nLevs,1);
for iLev = 1:nLevs
    levelString{iLev} = '';
    for iFactor = 1:nFacs
        if ~levelOnly
            currStr = strrep(formatStr,'$F',DF.factor{iFactor});
        else
            currStr = formatStr;
        end
        if iscell(DF.level{iFactor})            
            currStr = strrep(currStr,'$L',DF.level{iFactor}{levelIndex{iLev,iFactor}});
        end
        
        if iFactor==1
            levelString{iLev} = [levelString{iLev} currStr];
        else
            levelString{iLev} = [levelString{iLev} delimiter currStr];
        end
    end
end