function [newself,paramarray] = df_Array2Samples(self,seriesLabel,alignParam)
% DF_ARRAY2SAMPLES Merge multiple DataFrames of identical factor/levels
%
% Parameters
% ----------
% seriesLabel: char
% alignParam: numeric
%
% Examples
% --------
% ::
%
%     [newself,paramarray] = df_Array2Samples(self,seriesLabel,alignParam)
%
% Note
% ----
% I often use this function to align vertical/laminar channels
%

if nargin<2
    seriesLabel = '';
    if nargin<3
        alignParam = [];
    end;end

szDF = size(self);
nDF = numel(self);
[nFac,nLev,nEl] = df_getsize(self(1));
newself = self(1);
paramarray = cell(size(self(1).cells));
[sampD,seriesD] = df_celldim(self(1));
if isempty(sampD)
    sampD = max(seriesD)+1;
end

%% ensure DataFrames have identical factor/level
if isempty(seriesLabel) && isempty(alignParam)
    for iDF = 2:nDF
        for iCell = 1:numel(newself.cells)
            newself.cells{iCell} = cat(sampD, newself.cells{iCell},self(iDF).cells{iCell});
        end
    end
end

%% merge data cells
if ~isempty(seriesLabel) && ~isempty(alignParam)
    if iscell(seriesLabel)
        nAL = length(seriesLabel);
    elseif ischar(seriesLabel)
        nAL = 1;
        seriesLabel = {seriesLabel};
    end
    
    allCellDims = cellfun('ndims',self(1).cells);
%     if any(allCellDims(:)>4)
%         error('Can''t handle arrays with more than 4 dimensions!');
%     end
    dimarrayNum = max(allCellDims(:))+1;
    for i = 1:prod(nLev)
        dimarray = ones(nDF,dimarrayNum);
        for iAL=1:nAL
            [alignIndex(:,iAL),alignDim(iAL),alignSeriesNr(iAL)] = getAlignmentParam(self,seriesLabel{iAL},alignParam(iAL));
            dimarray(:,alignDim(iAL)) = alignIndex(:,iAL);
        end
        
        % merge data cells
        data = cell(nDF,1);
        for iDF = 1:nDF
            data{iDF,1} = self(iDF).cells{i};
        end
        [data,alignbins,headnum,tailnum] = mergearrays(data,sampD,dimarray,[],false);
        newself.cells{i} = data;
        headnum(cellfun('isempty',headnum)) = {zeros(nDF,1)};
        tailnum(cellfun('isempty',tailnum)) = {zeros(nDF,1)};
        
        % merge param
        param = cell(nDF,nAL);
        for iAL=1:nAL
            for iDF = 1:nDF
                param{iDF,iAL} = [ones(1,headnum{alignDim(iAL)}(iDF)).*NaN self(iDF).seriesparam{alignSeriesNr(iAL)} ones(1,tailnum{alignDim(iAL)}(iDF)).*NaN];
            end
            newself.seriesparam{alignSeriesNr(iAL)} = nanmedian(cat(1,param{:,iAL}),1);
        end
    end
end






function [alignIndex,alignDim,alignSeriesNr] = getAlignmentParam(self,AlignSeriesLabel,AlignParam)

nDF = length(self);
alignSeriesNr = zeros(nDF,1).*NaN;
alignDim = zeros(nDF,1).*NaN;
alignIndex = zeros(nDF,1).*NaN;
for iDF=1:nDF
    alignSeriesNr(iDF,1) = find(strcmp(AlignSeriesLabel,self(iDF).serieslabel));
    alignDim(iDF,1) = self(iDF).seriesdim(alignSeriesNr(iDF,1));
    if isnumeric(AlignParam)
        cAlignIndex = find(self(iDF).seriesparam{alignSeriesNr(iDF,1)}==AlignParam);
        if isempty(cAlignIndex)
            [cAlignIndexMinDiff,cAlignIndex] = min(abs(self(iDF).seriesparam{alignSeriesNr(iDF,1)}-AlignParam));
            alignIndex(iDF,1) = cAlignIndex;
        else
            alignIndex(iDF,1) = cAlignIndex;
        end
    elseif ischar(AlignParam)
        alignIndex(iDF,1) = find(strcmp(self(iDF).seriesparam{alignSeriesNr(iDF,1)},AlignParam));
    end
end
alignDim = unique(alignDim,'rows');
alignSeriesNr = unique(alignSeriesNr,'rows');















