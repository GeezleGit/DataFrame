function self = df_weighData(self,refData)

nGrps = numel(self.cells);
[sampD,seriesD] = df_celldim(self);
nD = max([sampD,seriesD]);
sizeArr = cell(1,nD);
repArr = ones(1,nD);

refD = df_celldim(refData);
permArr = [1:nD];
permArr = circshift(permArr,[1 sampD-refD]);

for iGrp = 1:nGrps
    [sizeArr{:}] = size(self.cells{iGrp});
    repArr(seriesD) = cat(2,sizeArr{seriesD});
    
    % modify weights to sample size
    cWeights = refData.cells{iGrp};
    n = size(self.cells{iGrp},sampD);
    cWeights = cWeights .* (n/sum(cWeights));

    % permute the weighing array
    cWeights = permute(cWeights,permArr);
    cWeights = repmat(cWeights,repArr);

    % multiply to weigh
    self.cells{iGrp} = self.cells{iGrp} .* cWeights;
end

