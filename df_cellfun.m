function c = df_cellfun(self,funHandle,varargin)
% wrapper for cellfun.m
c = cellfun(funHandle,self.cells,varargin{:});