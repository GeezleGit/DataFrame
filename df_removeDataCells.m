function self = df_removeDataCells(self,varargin)

% removes data-cells (factorlevel combinations) from DataFrame by setting
% their content to [] or NaN
% DataFrame = removeFactorLevel(DataFrame,i,mode)
% DataFrame = removeFactorLevel(DataFrame,{i, j, ... },mode)
% DataFrame = removeFactorLevel(DataFrame,cFac,cLevel,mode)
%
% mode ... 'NaN', 'empty'


nDF = length(self);
for iDF = 1:nDF
    i    = varargin{1};
    mode = varargin{2};
    switch mode
        case 'empty'
            self(iDF).cells(i) = {[]};
        case 'NaN'
            for k=i(:)'
                self(iDF).cells{k}(:) = NaN;
            end
    end
end

