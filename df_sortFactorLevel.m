function DF = df_sortFactorLevel(DF,cFac,LevelSortIndex,CellsOnlyFlag)
% sort level of a certain factor in an array of DataFrames
% DF = sortFactorLevel(DF,cFac,cLevelSortIndex,CellsOnlyFlag)
% cFac .............. current factor
% cLevelSortIndex ... [nDF x nLev] 'ascend' 'descend'
% CellsOnlyFlag ..... if false, sort datacells AND levellabel

nDF = length(DF);
% if size(cLevelSortIndex,1)==1 && nDF>1
%     cLevelIndex = repmat(cLevelSortIndex,[nDF 1]);
% end

for iDF = 1:nDF
    [iFac] = df_findFactor(DF(iDF),cFac);
    if isnan(iFac); continue;end
    [nFac,nLev,nEl] = df_getsize(DF(iDF));
    
    if ischar(LevelSortIndex) && isnumeric(DF(iDF).level{iFac})
        [dummy,cLevSrtInd] = sort(DF(iDF).level{iFac}(:)',2,LevelSortIndex);
    else
        cLevSrtInd = LevelSortIndex(iDF,:);
    end
    
    if ~CellsOnlyFlag
        DF(iDF).level{iFac} = DF(iDF).level{iFac}(cLevSrtInd);
    end
    
    
    if iFac>1
        DF(iDF).cells = shiftdim(DF(iDF).cells,iFac-1);% shift to first
        DF(iDF).cells(:,:) = DF(iDF).cells(cLevSrtInd,:);
        DF(iDF).cells = shiftdim(DF(iDF).cells,nFac-(iFac-1));% shift back
    else
        DF(iDF).cells(:,:) = DF(iDF).cells(cLevSrtInd,:);
    end
    
    
end

