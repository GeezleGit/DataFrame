function DFout = df_normToSeries(DF,Option,SeriesLabel,SeriesParam)
% Perform various normalisation operations on all data cells
%
% ... = df_normData(DF,'optionstring',SeriesLabel,SeriesParam)
% optionstring ... 'MICHELSON'
%                  'MICHELSONinverse'
%                  'DIFFERENCE'
%                  'DIFFERENCEinverse'

if nargin<4
    SeriesParam = [];
end

DFout = DF;
nDF = length(DF);
for iDF = 1:nDF
    [nLevels] = size(DF(iDF).cells);
    nGrps = prod(nLevels);
    
    serNr = strcmp(DF(iDF).serieslabel,SeriesLabel);
    serNum = length(DF(iDF).seriesparam{serNr});
    
    if ~isempty(SeriesParam)
        if iscell(DF(iDF).seriesparam{serNr})
            serParNr = strcmp(DF(iDF).seriesparam{serNr},SeriesParam);
        elseif isnumeric(DF(iDF).seriesparam{serNr})
            serParNr = DF(iDF).seriesparam{serNr}==SeriesParam;
        end
        serParNr = find(serParNr);
    else
        serParNr = 1:serNum;
    end

    serDim = df_getSeriesDim(DF(iDF),SeriesLabel);
    sampD = df_celldim(DF(iDF));
    
    
    for iGrp = 1:nGrps
        
        if length(serParNr)==1
            RefData = DF(iDF).cells{iGrp};
            RefData = extractDimIndex(RefData,serDim,serParNr);
            RepArray = ones(1,max([DF(iDF).seriesdim sampD]));
            serN = size(DF(iDF).cells{iGrp},serDim);
            RepArray(serDim) = serN;
            RefData = repmat(RefData,RepArray);
        elseif length(serParNr)==serNum
            RefData = [];
            for i = 1:serNum
                RefData = extractDimIndex(DF(iDF).cells{iGrp},serDim,serParNr(i));
            end
        end
            
        
        switch Option
            case 'MICHELSON'
                DFout(iDF).cells{iGrp} = (DF(iDF).cells{iGrp} - RefData) ./ (DF(iDF).cells{iGrp} + RefData);
            case 'MICHELSONinverse'
                DFout(iDF).cells{iGrp} = (RefData - DF(iDF).cells{iGrp}) ./ (DF(iDF).cells{iGrp} + RefData);
            case 'DIFFERENCE'
                DFout(iDF).cells{iGrp} = (DF(iDF).cells{iGrp} - RefData);
            case 'DIFFERENCEinverse'
                DFout(iDF).cells{iGrp} = (RefData - DF(iDF).cells{iGrp});
            case 'RATIO'
                DFout(iDF).cells{iGrp} = DF(iDF).cells{iGrp} ./ RefData;
            case 'RATIOinverse'
                DFout(iDF).cells{iGrp} = RefData ./ DF(iDF).cells{iGrp};
        end
    end
end

function RefData = extractDimIndex(RefData,serDim,serParNr)
RefSize = size(RefData);
RefDimNum = length(RefSize);
RefPermArray = 1:RefDimNum;
RefPermArray = circshift(RefPermArray,[1 1-serDim]);
RefSize = circshift(RefSize,[1 1-serDim]);
RefData = permute(RefData,RefPermArray);
RefData = RefData(serParNr,:);
RefSize(1) = 1;
RefData = reshape(RefData,RefSize);
RefPermArray = 1:RefDimNum;
RefPermArray = circshift(RefPermArray,[1 serDim-1]);
RefData = permute(RefData,RefPermArray);
