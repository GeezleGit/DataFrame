function out = df_getFactorLevelNum(DF,cFac)
% DF_GETFACTORLEVELNUM 

if nargin<2
    out = cellfun('length',DF.level);
else
    FacNr = df_findFactor(DF,cFac);
    out = 0;
    if ~isnan(FacNr)
        out = length(DF.level{FacNr});
    end
end

