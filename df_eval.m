function varargout = df_eval(self,expression,OutputMode)
% applies a function on cells
% ... = df_eval(self,'[|y1|,|y2|] = fun(|x|);',OutputMode)

switch OutputMode
    case 'numeric'
        for iNout = 1:nargout
            varargout{iNout} = ones(size(self.cells)).*NaN;
        end
    case 'cell'
        for iNout = 1:nargout
            varargout{iNout} = cell(size(self.cells));
        end
end

nGrps = numel(self.cells);
for iGrp = 1:nGrps
    cExpression = strrep(expression,'|x|','self.cells{iGrp}');
    switch OutputMode
        case 'numeric'
            for iNout = 1:nargout
                cExpression = strrep(cExpression,sprintf('|y%d|',iNout),sprintf('varargout{%d}(iGrp)',iNout));
            end
        case 'cell'
            for iNout = 1:nargout
                cExpression = strrep(cExpression,sprintf('|y%d|',iNout),sprintf('varargout{%d}{iGrp}',iNout));
            end
    end
    eval(cExpression);
end

