function DF = df_fillEmptyCells(DF,option,value)

nDF = numel(DF);
for iDF = 1:nDF
    numDims = cellfun('ndims',DF(iDF).cells);
    numCells = length(numDims(:));
    numDims = max(numDims(:));
    cellSizes = zeros(numCells,numDims);
    for iDim=1:numDims
        d = cellfun('size',DF(iDF).cells,iDim);
        cellSizes(:,iDim) = d(:);
    end
    isZero = any(cellSizes==0,2);
    switch option
        case 'max'
            grandSize = max(cellSizes(~isZero,:));
        case 'min'
            grandSize = min(cellSizes(~isZero,:));
    end
    DF(iDF).cells(isZero) = {ones(grandSize).*value};
end

