function self = df_Series2Samples(self,seriesLabel,seriesBins,seriesPar)

% self = df_Series2Samples(self,seriesLabel)

nDF             = numel(self);
iSeries         = strcmp(self(1).serieslabel,seriesLabel);
sD              = self(1).seriesdim(iSeries);
[sampD,seriesD] = df_celldim(self(1));

if nargin==2

    % convert all series (old use)
    for i=1:nDF
        for j = 1:numel(self(i).cells)
            self(i).cells{j} = mergedim(self(i).cells{j},sD,sampD);
        end
        self(i) = df_removeSeriesInfo(self(i),iSeries);
    end
    
else
    
    % collapse series into new bins
    [nBins,thisShouldBeTwo] = size(seriesBins);
    for i=1:nDF
        for j = 1:numel(self(i).cells)
            self(i).cells{j} = mergedim2(self(i).cells{j},sD,sampD,seriesBins,self(i).seriesparam{iSeries});
        end
        self(i).seriesparam{iSeries}=seriesPar;
    end
    
end

function x = mergedim(x,dim1,dim2)
minDims = max([dim1,dim2]);
sz = size(x);
nDims = length(sz);
if nDims<minDims
    sz(nDims+1:minDims) = 1;
end
nDims = length(sz);

% Here we permute such that the 2 dimensions are in 1st (rows) and 2nd (cols) and then
% reshape that they are all in 2nd dim
permArr       = 1:nDims;
permArr(dim1) = 1;
permArr(1)    = dim1;
permArr(dim2) = permArr(2);
permArr(2)    = dim2;
x = permute(x,permArr);
sz = size(x);
sz([1 2]) = [1 sz(1)*sz(2)];
x = reshape(x,sz);
[dummy,permArr] = sort(permArr);
x = permute(x,permArr);


function x = mergedim2(x,dim1,dim2,seriesBins,seriesParam)
minDims = max([dim1,dim2]);
sz = size(x);
nDims = length(sz);
if nDims<minDims
    sz(nDims+1:minDims) = 1;
end
nDims = length(sz);

% Here we permute such that the 2 dimensions are in 1st (rows) and 2nd (cols) and then
% reshape that they are all in 2nd dim
permArr       = 1:nDims;
permArr(dim1) = 1;
permArr(1)    = dim1;
permArr(dim2) = permArr(2);
permArr(2)    = dim2;
x = permute(x,permArr);

nBins=size(seriesBins,1);
y = [];% this will be the new x array
for iBin=1:nBins
    binIndex = seriesParam>=seriesBins(iBin,1) & seriesParam<=seriesBins(iBin,2);
    binNum = sum(binIndex);
    sz = size(x);
    sz([1 2]) = [1 binNum*sz(2)];
    if sz(2)==size(y,2) || isempty(y)
        y = cat(1,y,reshape(x(binIndex,:),sz));
    elseif sz(2)>size(y,2)
        % pad y
        newSize = sz;
        newSize(2)= sz(2)-size(y,2);
        y = cat(1,cat(2,y,ones(newSize).*NaN),reshape(x(binIndex,:),sz));
    elseif sz(2)<size(y,2)
        % pad x
        newSize = sz;
        newSize(2)= size(y,2)-sz(2);
        y = cat(1,y,cat(2,reshape(x(binIndex,:),sz),ones(newSize).*NaN));
    end
end

[dummy,permArr] = sort(permArr);
x = permute(y,permArr);

%             for iBin=1:nBins
%                 binIndex = self.seriesparam(iSeries)>=seriesBins(iBin,1) & self.seriesparam(iSeries)>=seriesBins(iBin,1);
%                 
           
