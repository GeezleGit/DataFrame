function DFout = df_normToLevel(DF,Option,Factor,Level)
% Perform various normalisation operations on all data cells
%
% ... = df_normData(DF,'optionsstring',RefFactor,RefLevel)
% optionstring ... 'MICHELSON'
%                  'MICHELSONinverse'
%                  'DIFFERENCE'
%                  'DIFFERENCEinverse'

DFout = DF;
nDF = length(DF);
for iDF = 1:nDF
    [LevelNr,FacNr] = df_findLevel(DF(iDF),Factor,Level);
    [nLevels] = size(DF(iDF).cells);
    nFac = length(DF(iDF).factor);
    RefIndex = cell(1,nFac);
    nGrps = prod(nLevels);
    
    
    for iGrp = 1:nGrps
        [RefIndex{:}] = ind2sub(nLevels,iGrp);
        RefIndex{FacNr} = LevelNr;
        RefData = DF(iDF).cells{RefIndex{:}};
        
        switch Option
            case 'MICHELSON'
                DFout(iDF).cells{iGrp} = (DF(iDF).cells{iGrp} - RefData) ./ (DF(iDF).cells{iGrp} + RefData);
            case 'MICHELSONinverse'
                DFout(iDF).cells{iGrp} = (RefData - DF(iDF).cells{iGrp}) ./ (DF(iDF).cells{iGrp} + RefData);
            case 'DIFFERENCE'
                DFout(iDF).cells{iGrp} = (DF(iDF).cells{iGrp} - RefData);
            case 'DIFFERENCEinverse'
                DFout(iDF).cells{iGrp} = (RefData - DF(iDF).cells{iGrp});
        end
    end
end

