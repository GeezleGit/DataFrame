function varargout = df_cellsize(self,d)
% DF_CELLSIZE get array size of data container (better for use with array sizes etc.)
%
% Example
% -------
% ::
% 
%     [n1,n2, ...] = df_cellsize(self)
%               nd = df_cellsize(self,d)
%


if nargin==2
    varargout{1} = size(self.cells,d);
elseif nargin==1 && nargout<2
    varargout{1} = size(self.cells);
elseif nargin==1 && nargout>1
    varargout = cell(1,nargout);
    [varargout{:}] = size(self.cells);
end
    

