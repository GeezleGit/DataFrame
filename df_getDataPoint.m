function v = df_getDataPoint(self,i)
%% DF_GETDATAPOINT get data in cells
%
% self = df_setDataPoint(self,i,v)
% expects identical array sizes in all cells

nGrps = numel(self.cells);
n = length(i);
v = repmat({zeros(size(self.cells))},[1 n]);
for j = 1:n
    for iGrp = 1:nGrps
        v{j}(iGrp) = self.cells{iGrp}(i(j));
    end
end
if n==1
    v = v{1};
end