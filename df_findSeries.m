function [i,d] = df_findSeries(self,seriesLabel,seriesValue)
% DF_FINDSERIES get index and dimension of series
%
% Example
% -------
% ::
% 
%     [i,d] = df_findSeries(self,seriesLabel,seriesValue)

sIndex = strcmp(self.serieslabel,seriesLabel);
d = self.seriesdim(sIndex);
i = find(self.seriesparam{sIndex}==seriesValue);