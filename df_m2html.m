function df_m2html(self)

selfPath = which('df_m2html');
[selfDir,selfFileName,selfFileExt] = fileparts(selfPath);

[dummy,classesDir] = strtok(fliplr(selfDir),filesep);
classesDir = fliplr(classesDir);
[dummy,alwinDir] = strtok(fliplr(classesDir),filesep);
alwinDir = fliplr(alwinDir);

workingDir = pwd;
cd(selfDir);
cd ..

targetDir = fullfile('/home/alwin/Dropbox/Coding/Matlab','doc','@DataFrame');
if exist(targetDir,'dir')
    rmdir(targetDir,'s');
end

m2html( ...
    'mFiles', '@DataFrame', ...
    'htmldir',targetDir, ...
    'recursive','on', ...
    'ignoredDir',{'.svn' 'cvs' '.git' 'doc'}, ...
    'template','blue'); % '3frame' 'frame' 'brain' 'blue'
cd(workingDir);

