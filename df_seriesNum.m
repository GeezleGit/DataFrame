function n = df_seriesNum(self,dim)
% DF_SERIESNUM returns length of a series in data cells (i.e. size in a dimension)
% 
% Example
% -------
% ::
% 
%     n = df_seriesNum(self,'seriesLabel');
%     n = df_seriesNum(self,seriesDimension);
%
% cellmode

if nargin<2
    % compute size of arrays for every cell
    n = cell(size(self.cells));
    for i=1:numel(self.cells)
        n{i} = size(self.cells{i});
    end
    try
        n = unique(cat(1,n{:}),'rows'); 
    catch
        error('DataFrame cells contain different numbers of dimensions!');
    end
    
elseif isempty(dim) || (ischar(dim) && strcmpi(dim,'samples'))
    [sampD,seriesD] = df_celldim(self);
    n = cellfun('size',self.cells,sampD);
    
elseif ischar(dim)
    cSerDim = strcmp(dim,self.serieslabel);
    n = length(self.seriesparam{cSerDim});
    
elseif isnumeric(dim) && ~isempty(dim)
    nDims = length(dim);
    n = zeros(1,nDims).*NaN;
    for iDim=1:nDims
        iSer = self.seriesdim==dim(iDim);
        if any(iSer)
            n(1,iDim) = length(self.seriesparam{self.seriesdim==dim(iDim)});
        end
    end
end