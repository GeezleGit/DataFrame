function [d,iFoundLevel,FoundFacNr] = df_getData(DF,varargin)
    % finds indices to data cells of a given level combination
    %
    % select cells by labels
    % [...] = getData(DF,'Fac1',{'F1Level1' 'F1Level2' ... 'F1LevelN'}, ... ,'FacN',{'FNLevel1' 'FNLevel2' ... 'FNLevelN'})
    %
    % by scalar index
    % [...] = getData(DF,i)
    %
    % by subscript
    % [...] = getData(DF,{[i] [j] [k] ...})
    
    [nFac,nLev,nEl] = df_getsize(DF);
    
    if length(varargin)==1
        if isnumeric(varargin{1})
            d = DF.cells(varargin{1});
        elseif iscell(varargin{1})
            d = DF.cells(varargin{1}{:});
        end
        iFoundLevel = [];
        FoundFacNr = [];
    else
        
        iLevel = cell(1,nFac);
        [iFoundLevel,FoundFacNr] = df_findFactorLevel(DF,varargin{:});
        if any(isempty(iFoundLevel))||length(iFoundLevel)~=nFac
            error('Input put must be a single level of each existing factor');
        end
        if isnumeric(iFoundLevel)
            iLevel(FoundFacNr) = num2cell(iFoundLevel);
        else
            iLevel = iFoundLevel;
        end
        d = DF.cells(iLevel{:});
    end
end

