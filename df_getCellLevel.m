function c = df_getCellLevel(self,facDel,levDel,FacIndex)

% get the level combination for each data cell
%
%
%

nFac = length(self.factor);
nLevel = size(self.cells);
if nargin<4
    FacIndex = 1:nFac;
end
c = cell(nLevel);
n = numel(c);
cInd = cell(1,length(nLevel));

if nargin>1
    if ~isempty(facDel) && ~isempty(levDel)
        for i=1:n
            [cInd{:}] = ind2sub(nLevel,i);
            cString = '';
            for iFac = FacIndex
                cString = sprintf('%s%s%s%s',cString,self.factor{iFac},facDel,self.level{iFac}{cInd{iFac}});
                if iFac<nFac
                    cString = sprintf('%s%s',cString,levDel);
                end
            end
            c{i} = cString;
        end
    elseif isempty(facDel) && ~isempty(levDel)
        for i=1:n
            [cInd{:}] = ind2sub(nLevel,i);
            cString = '';
            for iFac = FacIndex
                cString = sprintf('%s%s',cString,self.level{iFac}{cInd{iFac}});
                if iFac<nFac
                    cString = sprintf('%s%s',cString,levDel);
                end
            end
            c{i} = cString;
        end
    end
else
    for i=1:n
        [cInd{:}] = ind2sub(nLevel,i);
        for iFac = 1:length(FacIndex)
            c{i}{iFac} = self.level{FacIndex(iFac)}(cInd{FacIndex(iFac)});
        end
    end
end
     