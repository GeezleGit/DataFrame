function iDF = df_compare(self,operator,varargin)

% DF = df_compare(DF,operator,DataFrame)
% DF = df_compare(DF,operator,Scalar)
% DF = df_compare(DF,operator,'FactorName',FactorValues)
% DF = df_compare(DF,'~',[])
% DF = df_compare(DF,'~~',[])
% DF = df_compare(DF,'NAN',[])
% DF = df_compare(DF,'~NAN',[])
% 
% '>' '>=' '<=' '<' '==' '~=' '~' '&' '|'

nGrps = numel(self.cells);
iDF = self;

if isa(varargin{1},'DataFrame')
    switch operator
        case '>'
            for iGrp=1:nGrps
                iDF.cells{iGrp} = self.cells{iGrp} > varargin{1}.cells{iGrp};
            end
        case '>='
            for iGrp=1:nGrps
                iDF.cells{iGrp} = self.cells{iGrp} >= varargin{1}.cells{iGrp};
            end
        case '<='
            for iGrp=1:nGrps
                iDF.cells{iGrp} = self.cells{iGrp} <= varargin{1}.cells{iGrp};
            end
        case '<'
            for iGrp=1:nGrps
                iDF.cells{iGrp} = self.cells{iGrp} < varargin{1}.cells{iGrp};
            end
        case '=='
            for iGrp=1:nGrps
                iDF.cells{iGrp} = self.cells{iGrp} == varargin{1}.cells{iGrp};
            end
        case '~='
            for iGrp=1:nGrps
                iDF.cells{iGrp} = self.cells{iGrp} ~= varargin{1}.cells{iGrp};
            end
        case '~'
            for iGrp=1:nGrps
                iDF.cells{iGrp} = ~self.cells{iGrp};
            end
        case '~~'
            for iGrp=1:nGrps
                iDF.cells{iGrp} = ~~self.cells{iGrp};
            end
        case '&'
            for iGrp=1:nGrps
                iDF.cells{iGrp} = self.cells{iGrp} & varargin{1}.cells{iGrp};
            end
        case '|'
            for iGrp=1:nGrps
                iDF.cells{iGrp} = self.cells{iGrp} | varargin{1}.cells{iGrp};
            end
        otherwise
            error('Operator not known!');
    end
elseif isnumeric(varargin{1})
    switch operator
        case '>'
            for iGrp=1:nGrps
                iDF.cells{iGrp} = self.cells{iGrp} > varargin{1};
            end
        case '>='
            for iGrp=1:nGrps
                iDF.cells{iGrp} = self.cells{iGrp} >= varargin{1};
            end
        case '<='
            for iGrp=1:nGrps
                iDF.cells{iGrp} = self.cells{iGrp} <= varargin{1};
            end
        case '<'
            for iGrp=1:nGrps
                iDF.cells{iGrp} = self.cells{iGrp} < varargin{1};
            end
        case '=='
            for iGrp=1:nGrps
                iDF.cells{iGrp} = self.cells{iGrp} == varargin{1};
            end
        case '~='
            for iGrp=1:nGrps
                iDF.cells{iGrp} = self.cells{iGrp} ~= varargin{1};
            end
        case '~'
            for iGrp=1:nGrps
                iDF.cells{iGrp} = ~self.cells{iGrp};
            end
        case '~~'
            for iGrp=1:nGrps
                iDF.cells{iGrp} = ~~self.cells{iGrp};
            end
        case 'NAN'
            for iGrp=1:nGrps
                iDF.cells{iGrp} = isnan(self.cells{iGrp});
            end
        case '~NAN'
            for iGrp=1:nGrps
                iDF.cells{iGrp} = ~isnan(self.cells{iGrp});
            end
        otherwise
            error('Operator not known!');
    end
    
elseif ischar(varargin{1})
    isFac = strcmp(self.factor,varargin{1});
    if isnumeric(self.level{isFac})
        LevNr(1) = find(self.level{isFac} == varargin{1}(1));
        LevNr(2) = find(self.level{isFac} == varargin{1}(2));
    elseif iscell(self.level{isFac})
        LevNr(1) = find(strcmp(self.level{isFac},varargin{1}(1)));
        LevNr(2) = find(strcmp(self.level{isFac},varargin{1}(1)));
    else
        error('');
    end
    
    cellSize = size(self.cells);
    cellIndex = cell(size(cellSize));
    removeLev = true(1,cellSize(isFac));
    removeLev(1) = false;
    iDF = df_removeFactorLevel(iDF,varargin{1},removeLev);
    cellSize(isFac) = 1;
    nGrps = prod(cellSize);
    
    for iGrp=1:nGrps
        [cellIndex{:}] = ind2sub(cellSize,iGrp);
        cIndex1 = cellIndex;
        cIndex2 = cellIndex;
        cIndex1{isFac} = LevNr(1);
        cIndex2{isFac} = LevNr(2);
        
        switch operator
            case '>'
                iDF.cells{iGrp} = self.cells{cIndex1{:}} >  self.cells{cIndex2{:}};
            case '>='
                iDF.cells{iGrp} = self.cells{cIndex1{:}} >= self.cells{cIndex2{:}};
            case '<='
                iDF.cells{iGrp} = self.cells{cIndex1{:}} <= self.cells{cIndex2{:}};
            case '<'
                iDF.cells{iGrp} = self.cells{cIndex1{:}} <  self.cells{cIndex2{:}};
            case '=='
                iDF.cells{iGrp} = self.cells{cIndex1{:}} == self.cells{cIndex2{:}};
            case '~='
                iDF.cells{iGrp} = self.cells{cIndex1{:}} == self.cells{cIndex2{:}};
            case '~'
                iDF.cells{iGrp} = ~self.cells{cIndex1{:}};
            case '&'
                iDF.cells{iGrp} = self.cells{cIndex1{:}} &  self.cells{cIndex2{:}};
            case '|'
                iDF.cells{iGrp} = self.cells{cIndex1{:}} |  self.cells{cIndex2{:}};
            otherwise
                error('Operator not known!');
        end
    end
    
end
            