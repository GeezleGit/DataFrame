function self = df_removeSeries(self,seriesLabel,index,squeezeFlag,replaceValue)
% remove a series from data cells
% self = df_removeSeries(self,seriesLabel,index,squeezeFlag)
% seriesLabel ... string or seriesindex
% index ......... indices of elements to remove
%                 if empty, series length must be one to remove seriesdata
% squeezeFlag ... squeeze array dims, if index is empty and series length
%                 is one

if nargin<5
    replaceValue = [];
end

%% get series
if ischar(seriesLabel)
    iSer  = find(strcmp(seriesLabel,self.serieslabel));
elseif isnumeric(seriesLabel)
    iSer = seriesLabel;
elseif islogical(seriesLabel)
    iSer = find(seriesLabel);
end

seriesDim = self.seriesdim(iSer);

%% remove parameter
if ~isempty(index)
    [self.cells] = removeFromArray(self.cells,seriesDim,index,replaceValue);
    if isempty(replaceValue)
        self.seriesparam{iSer}(index) = [];
    end
end

%% remove series info and squeeze
isSingletonSeries = all(cellfun('size',self.cells(:),seriesDim)==1);
if isempty(index) && isSingletonSeries && ~squeezeFlag
    % remove series data
    self.serieslabel(iSer) = [];
    self.seriesdim(iSer) = [];
    self.seriesparam(iSer) = [];
elseif isSingletonSeries && squeezeFlag
    % remove series data and squeeze
    self.serieslabel(iSer) = [];
    self.seriesparam(iSer) = [];
    self.seriesdim(iSer)   = [];
    if seriesDim<=max(cellfun('ndims',self.cells))
        [self.cells,permArray] = shiftSeriesDim(self.cells,seriesDim);
        for i=1:length(self.seriesdim)
            self.seriesdim(i) = find(self.seriesdim(i)==permArray);
        end
    end
end

function [x,permArray] = shiftSeriesDim(x,iD)
% this removes the last dimension detected by ndims behind given dimension
% iD
n = numel(x);
for i=1:n
    nds = ndims(x{i});
    permArray      = 1:nds;
    permArray(iD)  = nds;
    permArray(nds) = iD;
    x{i} = permute(x{i},permArray);
end


function [x,sz,nds] = removeFromArray(x,iD,index,replaceValue)
n = numel(x);
nds = zeros(n,1);
sz = zeros(n,10);
for i=1:n
    nds(i) = ndims(x{i});
    sz(i,1:nds(i)) = size(x{i});
    
    permArray = circshift([1:nds(i)],[1 1-iD 1]);
    x{i} = permute(x{i},permArray);
    cSize = size(x{i});
        
    if isempty(replaceValue)
        x{i}(index,:) = [];
        cSize(1) = cSize(1)-length(index);
    else
        x{i}(index,:) = replaceValue;
    end
    
    x{i} = reshape(x{i},cSize);
    permArray = circshift([1:nds(i)],[1 (1-iD)*(-1) 1]);
    x{i} = permute(x{i},permArray);
end



