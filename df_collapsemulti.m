function [DF,SourceDimIndex,SqueezeSort] = df_collapsemulti(DF,collDim,collIndex,catdim,SqueezeFlag)
% Multiple steps of concatenation of content of cells. Uses :func:`private.x_collapse`
%
% Parameters
% ----------
% collDim: int
%     dimension of collapse
% collIndex: cell
%     index along collDim of cells to collapse, [] collapses all.
% catdim: int
%     concatenates content of cells along this dimension
% SqueezeFlag: bool
%
% Returns
% -------
% SourceDimIndex: cell
%     vector of length in collapsed dim. Contains 0's (collapsed indices) and indices of array before collapse.
%
% Examples
% --------
% ::
%
%     [DF,cellidx] = collapsemulti(DF,collDim,collIndex,catdim)

    
    
%% check input
if nargin<4
    error('Not enough input arguments!');
end
n = size(DF.cells);
nDim = length(n);

%% process dimensions sequentially
SourceDimIndex = cell(1,nDim);
for iD = 1:nDim
    SourceDimIndex{iD} = 1:n(iD);
    ThisDimColls = find(collDim==iD);
    ThisDimCollsN = length(ThisDimColls);
    NewDimIndex = [];
    for iColl = 1:ThisDimCollsN
        
        % correct collIndex if this dimension already had been collapsed
        if iColl>1
            collIndex{ThisDimColls(iColl)} = find(ismember(SourceDimIndex{iD},collIndex{ThisDimColls(iColl)}));
        end
        
        [DF.cells,NewDimIndex] = x_collapse(DF.cells,collDim(ThisDimColls(iColl)),collIndex{ThisDimColls(iColl)},catdim(ThisDimColls(iColl)));
        NewDimIndex(NewDimIndex==0) = iColl*-1;
        if iColl>1
            replacei = find(NewDimIndex>0);
            NewDimIndex(replacei) = SourceDimIndex{iD}(NewDimIndex(replacei));
        end
        SourceDimIndex{iD} = NewDimIndex;
    end
end

%% remove singleton dimension from output
SqueezeSort = [];
if SqueezeFlag
    n = size(DF.cells);
    SqueezeSort = [find(n>1) find(n==1)];
    DF.factor = DF.factor(find(n>1));
    DF.level = DF.level(find(n>1));
    DF.cells = permute(DF.cells,SqueezeSort);
    SourceDimIndex = SourceDimIndex(find(n>1));
end

