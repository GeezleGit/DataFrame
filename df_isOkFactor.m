function [isOk] = df_isOkFactor(DF,Fac,Lev)
    % check if Factor, Level, any Datums of that level
    isOk = false;
    iFac = strcmp(Fac,DF.factor);if ~any(iFac);return;end
    if isempty(Lev)
        Lev = DF.level;
    end
    nLev = length(Lev);
    nodata = cellfun('length',DF.cells)==0;
    nodata = shiftdim(nodata,find(iFac)-1);
    for i = 1:nLev
        iLev = strcmp(Lev{i},DF.level{iFac});if ~any(iLev);return;end
        if all(nodata(iLev,:));return;end
    end
    isOk = true;
end
