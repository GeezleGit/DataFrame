function self = df_filtfilt(self,filterDim,filterType,filterParam)

switch filterType
    case 'butter'
        [b,a] = butter(filterParam{:});
end

n = numel(self);
for i=1:n
    
    oldSeriesOrder = self(i).serieslabel(self(i).seriesdim);
    
    self(i) = df_permuteSeries(self(i),'time',1);
    m = numel(self(i).cells);
    for j = 1:m
        self(i).cells{j}(:,:) = filtfilt(b, a, self(i).cells{j}(:,:));
    end
    
    self(i) = df_permuteSeries(self(i),oldSeriesOrder,true);
end
    