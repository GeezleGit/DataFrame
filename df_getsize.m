function [nFac,nLev,nEl] = df_getsize(DF)
% DF_GETSIZE
% 
% Example
% -------
% ::
% 
%     [nFac,nLev,nEl] = df_getsize(DF)
% 

[nFac,nLev,nEl] = df_size(DF);
% warning('df_getsize.m is depreciated. Use df_size instead.');

