function varargout = df_getLevelPairIndex(self)
%% DF_GETLEVELPAIRINDEX
%
% ind = df_getLevelPairIndex(self)
% [subs1,subs2] = df_getLevelPairIndex(self)

levN   = size(self.cells);
facN   = length(levN);
grpN = prod(levN);
x = pairPerm(grpN);

if nargout==2
    x1 = cell(1,facN); 
    x2 = cell(1,facN); 
    [x1{:}] = ind2sub(levN,x(:,1));
    [x2{:}] = ind2sub(levN,x(:,2));
    varargout{1} = cat(2,x1{:});
    varargout{2} = cat(2,x2{:});
else
    varargout{1} = x;
end

function x = pairPerm(grpN)
n = sum(grpN-1:-1:0);
x = zeros(n,2);
c = 0;
for iGrp = 1:grpN
    for jGrp = iGrp+1:grpN
        c = c+1;
        x(c,1) = iGrp;
        x(c,2) = jGrp;
    end
end
