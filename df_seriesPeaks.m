function [dfPar,dfVal] = df_seriesPeaks(self,seriesID,parIndexOutput,peakModeName,peakModePar)

% Returns peaks/troughs within data series
% [dfPar,dfVal] = df_seriesPeaks(self,seriesID,parIndexOutput)
% 
% seriesID ......... label, dimension
% parIndexOutput ... flag to return parameter index instead of value
%
% dfPar ... DataFrame containing parameter value at maximum
% dfVal ... DataFrame containing maximum value

if nargin<4
    peakModeName = 'none';
    peakModePar = 0;
end

if ischar(seriesID)
    serNr = strcmp(self.serieslabel,seriesID);
elseif isnumeric(seriesID)
    serNr = self.seriesdim==seriesID;
end

dfPar = self;
dfVal = self;

nGrp = numel(self.cells);
for iGrp = 1:nGrp
    [dfVal.cells{iGrp},dfPar.cells{iGrp}] = findPeaksInSeries(self.cells{iGrp},self.seriesdim(serNr),peakModeName,peakModePar);
    
    if ~parIndexOutput
        dfPar.cells{iGrp}(~isnan(dfPar.cells{iGrp})) = dfPar.seriesparam{serNr}(dfPar.cells{iGrp}(~isnan(dfPar.cells{iGrp})));
    end
end

dfPar.seriesdim(serNr) = [];
dfPar.seriesparam(serNr) = [];
dfPar.serieslabel(serNr) = [];
dfVal.seriesdim(serNr) = [];
dfVal.seriesparam(serNr) = [];
dfVal.serieslabel(serNr) = [];




function [pks,ind] = findPeaksInSeries(X,cSerDim,peakModeName,peakModePar)
nSer = size(X);

% mainSeries to front, other series as columns
permArray = 1:length(nSer);
permArray(cSerDim) = 1;
permArray(1) = cSerDim;
permDimNum = nSer;
permDimNum(1) = nSer(cSerDim);
permDimNum(cSerDim) = nSer(1);
nCols = prod(permDimNum(2:end));
X = permute(X,permArray);
X = reshape(X,[permDimNum(1) nCols]);

% cycle through residual series to find the peaks
n = zeros(1,nCols);
pks = cell(1,nCols);
ind = cell(1,nCols);
for k = 1:nCols
    [ind{k},pks{k}] = findpeaks(X(:,k));
    ind{k} = ind{k}(:);
    pks{k} = pks{k}(:);
    
    switch peakModeName
        case 'largest'
            [pks{k},sortIndex] = sort(pks{k});
            ind{k} = ind{k}(sortIndex);
            if length(pks{k})>peakModePar
                pks{k} = pks{k}(1:peakModePar);
                ind{k} = ind{k}(1:peakModePar);
            end
    end
    
    n(k) = length(ind{k});
end
maxPkNum = max(n);

% pad array of peaks with NaN's for constant length
for k = 1:nCols
    if n(k)<maxPkNum
        pks{k}(n(k)+1:maxPkNum,1) = NaN;
        ind{k}(n(k)+1:maxPkNum,1) = NaN;
    end
end
pks = cat(2,pks{:});
ind = cat(2,ind{:});

% reshape series
pks = reshape(pks,[maxPkNum permDimNum(2:end)]);
ind = reshape(ind,[maxPkNum permDimNum(2:end)]);
pks = permute(pks,permArray);
ind = permute(ind,permArray);

    

function [ind,peaks] = findpeaks(y)
% FINDPEAKS  Find peaks in real vector.
%  ind = findpeaks(y) finds the indices (ind) which are
%  local maxima in the sequence y.  
%
%  [ind,peaks] = findpeaks(y) returns the value of the peaks at 
%  these locations, i.e. peaks=y(ind);

y = y(:)';

switch length(y)
case 0
    ind = [];
case 1
    ind = 1;
otherwise
    dy = diff(y);
    not_plateau_ind = find(dy~=0);
    ind = find( ([dy(not_plateau_ind) 0]<0) & ([0 dy(not_plateau_ind)]>0) );
    ind = not_plateau_ind(ind);
    if y(1)>y(2)
        ind = [1 ind];
    end
    if y(end-1)<y(end)
        ind = [ind length(y)];
    end
end

if nargout > 1
    peaks = y(ind);
end
