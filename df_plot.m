function [hAx,hFg,hObj,AxLabel] = df_plot(cDF,S)
% DF_PLOT plot DataFrame data
%
% Example
% -------
% ::
% 
%     [hAx,hFg] = df_plot(cDF,S)
%
% Parameters
% ----------
% S: struct
%     ::
%     
%         S.plotArea              = [], {size as data cell array}
%         S.plotType              = 'line'; % 'line', 'errline', 'map', '3DMap'
%         S.plotFactor            = [];
%         S.stackSeriesLabel      = '';
%         S.stackOffset           = [];
%         S.stackFactor           = 1;
%         S.transposeAxGrid       = false;
%         S.figMargin             = [0.1 0.05 0.1 0.1];
%         S.axSpacing             = [0.01 0.01];
%         S.minGrid               = [1 1];
%         S.maxGrid               = [NaN NaN];
%         S.gridOffset            = [0 0];
%         S.labelAxes             = false;% 'axes' 'rows' 'columns'
%         S.noGridFlag            = false;
%         S.series2AxesMap        = [1 0];
%         S.swapXY                = false;
%         S.intercept             = [0 0 0];
%         S.mainObjectProperties  = {'tag',''};
%         S.otherObjectProperties = {0 {'linestyle','-'} {'linestyle','-'}};
%         S.ColorOrder            = [];

%% plot settings
defS.plotArea = [];
defS.plotType = 'line';
defS.plotFactor = [];
defS.stackSeriesLabel = '';
defS.stackOffset = [];
defS.stackFactor = 1;
defS.transposeAxGrid = false;
defS.figMargin = [0.1 0.05 0.1 0.1];
defS.axSpacing = [0.01 0.01];
defS.minGrid = [1 1];
defS.maxGrid = [NaN NaN];
defS.gridOffset = [0 0];
defS.labelAxes = false;% 'axes' 'rows' 'columns'
defS.noGridFlag = false;
defS.series2AxesMap = [1 0];
defS.swapXY = false;
defS.intercept = [0 0 0];
defS.mainObjectProperties = {'tag',''};
defS.otherObjectProperties = {0 {'linestyle','-'} {'linestyle','-'}};
defS.ColorOrder = [];

S = StructUpdate(defS,S);

%% create plot grid
if isempty(S.plotArea)
    S.plotArea = scaledfigure('a4','centimeters','landscape',[1 0 0],'a4','centimeters');
end

%%
[sampDim,seriDim] = df_celldim(cDF(1));
if any(cellfun('size',cDF(1).cells,sampDim)>1)
    foundNONSeriesData = true;
else
    foundNONSeriesData = false;    
end

%% plot data
switch S.plotType
%     case {'xmarker' 'ymarker' 'zmarker'}
%         [hAx,hFg,AxLabel] = makeParentAxes(cDF,S);
%         if ~isempty(S.stackSeriesLabel)
%             switch S.plotType
%                 case 'xmarker'
%                     S.intercept(:,2) = S.intercept(:,2).*S.stackFactor+S.stackOffset;
%                 case 'ymarker'
%                     cDF = df_scale(cDF,S.stackOffset,S.stackFactor,S.stackSeriesLabel);
%             end
%         end
%         
%         switch S.plotType
%             case 'xmarker'
%                 X = df_getCells(cDF);
%                 Y = df_getSeries(cDF,1);
%                 Y = Y{1};
%             case 'ymarker'
%                 Y = df_getCells(cDF);
%                 X = df_getSeries(cDF,1);
%                 X = X{1};
%         end
%         
%         % plot data
%         lineHandle = cell(size(Y));
%         for iGrp  = 1:numel(Y)
%             nSizes = size(Y{iGrp});
%             if any(nSizes(2:end)>1)
%                 % multiple line plot
%                 lineXData = repmat(X(:),[1 prod(nSizes(2:end))]);
%                 lineYData = reshape(Y{iGrp},[nSizes(1),prod(nSizes(2:end))]);
%                 lineHandle{iGrp} = line(lineXData,lineYData,'parent',hAx{iGrp});
%             else
%                 % single line plot
%                 lineHandle{iGrp} = line(X(:),Y{iGrp}(:,1),'parent',hAx{iGrp});
%             end
%         end
        
        
        
    case 'line'
        % plots first series against values
        [hAx,hFg,AxLabel] = makeParentAxes(cDF,S);    
        
        if ~isempty(S.stackSeriesLabel)
            cDF = df_scale(cDF,S.stackOffset,S.stackFactor,S.stackSeriesLabel);
        end
        
        hObj = plotLine(cDF,hAx);
        set(cat(1,hObj{:}),S.mainObjectProperties{:});
        if S.swapXY
            rotate90(cat(1,hObj{:}));
        end
        
    case 'errline'
        [hAx,hFg,AxLabel] = makeParentAxes(cDF(1),S);        
        if ~isempty(S.stackSeriesLabel)
            cDF(1) = df_scale(cDF(1),S.stackOffset,S.stackFactor,S.stackSeriesLabel);
            cDF(2) = df_scale(cDF(2),zeros(size(S.stackOffset)),S.stackFactor,S.stackSeriesLabel);
            cDF(3) = df_scale(cDF(3),zeros(size(S.stackOffset)),S.stackFactor,S.stackSeriesLabel);
        end
        
        hObj = plotErrorbars(cDF(1),cDF(2),cDF(3),hAx,S.mainObjectProperties,S.otherObjectProperties);
        if S.swapXY
            rotate90(concatHandleCells(hObj,'plotErrorbars'));
        end
        
    case 'map'
        [hAx,hFg,AxLabel] = makeParentAxes(cDF,S);
        hObj = plotImage(cDF,hAx);

    case '3DMap'
        [hAx,hFg,AxLabel] = makeParentAxes(cDF,S);
        hObj = plotSurface(cDF,hAx);
        
    case 'scatter'
        % plots first series against values
        [hAx,hFg,AxLabel] = makeParentAxes(cDF(1),S);    
        
        if ~isempty(S.stackSeriesLabel)
            cDF(2) = df_scale(cDF(2),S.stackOffset,S.stackFactor,S.stackSeriesLabel);
        end
        
        hObj = plotScatter(cDF(1),cDF(2),hAx);
        set(cat(1,hObj{:}),S.mainObjectProperties{:});
        if S.swapXY
            rotate90(cat(1,hObj{:}));
        end
end


%% tune plots
if (islogical(S.labelAxes) && S.labelAxes) || (~islogical(S.labelAxes) && ismember('axes',S.labelAxes)) 
    if isnumeric(AxLabel.prop(1).xticks)
        set(AxLabel.handle, ...
            'xtick',sort(AxLabel.prop(1).xticks),'xticklabel',sort(AxLabel.prop(1).xticks), ...
            'ytick',sort(AxLabel.prop(1).yticks),'yticklabel',sort(AxLabel.prop(1).yticks));
    elseif iscell(AxLabel.prop(1).xticks)
    end
end

if (islogical(S.labelAxes) && S.labelAxes) || (~islogical(S.labelAxes) && ismember('rows',S.labelAxes))
    for cAxH = AxLabel.handle(AxLabel.isFirstRow)
        if isempty(AxLabel.prop(AxLabel.handle==cAxH).colLevel);continue;end
        if isnumeric(AxLabel.prop(AxLabel.handle==cAxH).colLevel)
            cString = sprintf('%s=%1.2f',AxLabel.prop(AxLabel.handle==cAxH).colFactor{1},AxLabel.prop(AxLabel.handle==cAxH).colLevel);
        else
            cString = sprintf('%s=%s',AxLabel.prop(AxLabel.handle==cAxH).colFactor{1},char(AxLabel.prop(AxLabel.handle==cAxH).colLevel));
        end
        set(get(cAxH,'title'),'string',cString,'visible','on');
    end
end
    
if (islogical(S.labelAxes) && S.labelAxes) || (~islogical(S.labelAxes) && ismember('columns',S.labelAxes))
    for cAxH = AxLabel.handle(AxLabel.isFirstCol)
        if isempty(AxLabel.prop(AxLabel.handle==cAxH).rowLevel);continue;end
        if isnumeric(AxLabel.prop(AxLabel.handle==cAxH).rowLevel)
            cString = sprintf('%s=%1.0f',AxLabel.prop(AxLabel.handle==cAxH).rowFactor{1},AxLabel.prop(AxLabel.handle==cAxH).rowLevel);
        else
            cString = sprintf('%s=%s',AxLabel.prop(AxLabel.handle==cAxH).rowFactor{1},char(AxLabel.prop(AxLabel.handle==cAxH).rowLevel));
        end
        set(get(cAxH,'ylabel'),'string',cString,'visible','on');
    end
end

if islogical(S.labelAxes) && S.labelAxes
    rowNr = 1;
    colNr = 1;
    nFigFac = length(AxLabel.prop(rowNr).figFactor);
    allLevelNum = size(hFg);
    if nFigFac>0
        %figFacsLevelNum = allLevelNum;
        if nFigFac==1
            figFacsLevelNum = [allLevelNum(3) 1];
        else
            figFacsLevelNum = allLevelNum(3:end);
        end
        figFacLevIndex = cell(figFacsLevelNum);
        for i=1:prod(figFacsLevelNum)% loop the figures
            iAllLevel = sub2ind(allLevelNum,rowNr,colNr,i);
            [figFacLevIndex{:}] = ind2sub(figFacsLevelNum,i);
            cString = '';
            for iFFac = 1:nFigFac
                cString = cat(2,cString,sprintf('%s-%s',AxLabel.prop(iAllLevel).figFactor{iFFac},char(AxLabel.prop(iAllLevel).figLevel{iFFac})));
                if iFFac<nFigFac
                    cString = cat(2,cString,'.');
                end
            end
            set(hFg{iAllLevel},'numbertitle','off','name',cString);
        end
    end
end




function [hAx,hFg,AxLabel] = makeParentAxes(cDF,S)
if ~iscell(S.plotArea) %&& (strcmpi(get(S.plotArea,'type'),'axes') || strcmpi(get(S.plotArea,'type'),'figure'))
    xDim = 2;
    yDim = 1;
    zDim = 3;
    [hAx,hFg,AxLabel] = df_subPlotGrid(cDF,S.transposeAxGrid,S.figMargin,S.axSpacing,S.minGrid,S.maxGrid,S.gridOffset,S.plotArea,S.plotFactor,[xDim yDim zDim],S.noGridFlag);
else
    hAx = S.plotArea;
    hFg = get(S.plotArea{1},'parent');
    AxLabel = [];
end
if ~isempty(S.ColorOrder)
    set(cat(1,hAx{:}),'ColorOrder',S.ColorOrder);
end



function lineHandle = plotLine(cDF,hAx)

% get plot parameter
Y = df_getCells(cDF);
X = df_getSeries(cDF,1);

% plot data
lineHandle = cell(size(Y));
for iGrp  = 1:numel(Y)
    nSizes = size(Y{iGrp});
    if any(nSizes(2:end)>1)
        % multiple line plot
        lineXData = repmat(X(:),[1 prod(nSizes(2:end))]);
        lineYData = reshape(Y{iGrp},[nSizes(1),prod(nSizes(2:end))]);
        lineHandle{iGrp} = line(lineXData,lineYData,'parent',hAx{iGrp});
    else
        % single line plot
        lineHandle{iGrp} = line(X(:),Y{iGrp}(:,1),'parent',hAx{iGrp});
    end
end

function lineHandle = plotScatter(cDF1,cDF2,hAx)

% get plot parameter
X = df_getCells(cDF1);
Y = df_getCells(cDF2);

% plot data
lineHandle = cell(size(Y));
for iGrp  = 1:numel(Y)
    nSizes = size(Y{iGrp});
    if any(nSizes(2:end)>1)
        % multiple line plot
        lineXData = reshape(X{iGrp},[nSizes(1),prod(nSizes(2:end))]);
        lineYData = reshape(Y{iGrp},[nSizes(1),prod(nSizes(2:end))]);
        lineHandle{iGrp} = line(lineXData,lineYData,'parent',hAx{iGrp});
    else
        % single line plot
        lineHandle{iGrp} = line(X{iGrp}(:),Y{iGrp}(:),'parent',hAx{iGrp});
    end
end

function lineHandle = plotErrorbars(cDF,hiErrDF,loErrDF,hAx,lineProps,ErrBarProps)

% get plot parameter
Y = df_getCells(cDF);
X = df_getSeries(cDF,1);
if iscell(X) && numel(X)==1
    X = X{1};
elseif iscell(X) && numel(X)~=1
    error('expected a single cell here!!');
end
Ylo = df_getCells(loErrDF);
Yhi = df_getCells(hiErrDF);


% plot data
lineHandle = cell(size(Y));
for iGrp  = 1:numel(Y)
    
    iPropParent = strcmpi(ErrBarProps{2},'parent');
    if any(iPropParent)
        ErrBarProps{2}{find(iPropParent)+1} = hAx{iGrp};
    else
        ErrBarProps{2} = [{'parent',hAx{iGrp}} ErrBarProps{2}];
    end
    iPropParent = strcmpi(ErrBarProps{3},'parent');
    if any(iPropParent)
        ErrBarProps{3}{find(iPropParent)+1} = hAx{iGrp};
    else
        ErrBarProps{3} = [{'parent',hAx{iGrp}} ErrBarProps{3}];
    end
    
    nSizes = size(Y{iGrp});
    if any(nSizes(2:end)>1)
        % multiple line plot
        lineXData = repmat(X(:),[1 prod(nSizes(2:end))]);
        lineYData = reshape(Y{iGrp},[nSizes(1),prod(nSizes(2:end))]);
        lineYloData = reshape(Ylo{iGrp},[nSizes(1),prod(nSizes(2:end))]);
        lineYhiData = reshape(Yhi{iGrp},[nSizes(1),prod(nSizes(2:end))]);
        [lineHandle{iGrp}{2},lineHandle{iGrp}{3}] = drawerrorbars(lineXData,lineYData,lineYloData,lineYhiData,ErrBarProps{:});
        lineHandle{iGrp}{1} = line(lineXData,lineYData,'parent',hAx{iGrp},lineProps{:});
        
        for i=1:prod(nSizes(2:end))
            lineHandles = cat(1,lineHandle{iGrp}{1}(i));
            whiskerHandles = cat(1,lineHandle{iGrp}{2}{:,i});
            teeHandles = cat(1,lineHandle{iGrp}{3}{:,i});
            set(lineHandles,'tag',sprintf('line%d',i));
            if isnumeric(whiskerHandles) && ~isempty(whiskerHandles) && ~all(isnan(whiskerHandles(:)))
                set(whiskerHandles,'tag',sprintf('whisker%d',i));
            end
            if isnumeric(teeHandles) && ~isempty(teeHandles) && ~all(isnan(teeHandles(:)))
                set(teeHandles,'tag',sprintf('tee%d',i));
            end
        end
    else
        % single line plot
        lineYloData = reshape(Ylo{iGrp},[nSizes(1),prod(nSizes(2:end))]);
        lineYhiData = reshape(Yhi{iGrp},[nSizes(1),prod(nSizes(2:end))]);
        [lineHandle{iGrp}{2},lineHandle{iGrp}{3}] = drawerrorbars(X(:),Y{iGrp}(:,1),lineYloData,lineYhiData,ErrBarProps{:});
        
        lineHandle{iGrp}{1} = line(X(:),Y{iGrp}(:,1),'parent',hAx{iGrp},lineProps{:});
    end
end



function h = plotImage(cDF,hAx)
% get plot parameter
C = df_getCells(cDF);
Y = df_getSeries(cDF,1);
% Y = Y{1};
X = df_getSeries(cDF,2);
% X = X{1};
h = cell(size(C));

% plot data
for iGrp  = 1:numel(C)
    h{iGrp} = imagesc(X,Y,C{iGrp},'parent',hAx{iGrp});
    
%     h{iGrp} = surface(X,Y,C{iGrp}, ...
%         'parent',hAx{iGrp},'linestyle','none');
end

function h = plotSurface(cDF,hAx)
% get plot parameter
C = df_getCells(cDF);

Y = df_getSeries(cDF,1);
X = df_getSeries(cDF,2);
Z = df_getSeries(cDF,3);
Z = repmat(permute(Z(:),[2 3 1]),[length(Y) length(X) 1]);

h = cell(size(C));

% plot data
for iGrp  = 1:numel(C)
    for iZ  = 1:size(C{iGrp},3)
        h{iGrp} = mapPlot(C{iGrp}(:,:,iZ),'surf',X,Y,Z(:,:,iZ),'parent',hAx{iGrp});
    end
end

function [X,Y,Z] = interpolateData(x,y,z,ScaleFactor,Method)
MXN = [length(y) length(x)].*ScaleFactor;
Y = linspace(y(1),y(end),MXN(1));
X = linspace(x(1),x(end),MXN(2));
Z = imresize(z,MXN,Method);



function hh = concatHandleCells(h,handleGenerator)
switch handleGenerator
    case 'plotErrorbars'
        nGrps = numel(h);
        hh = cell(size(h));
        for iGrp = 1:nGrps
            hh{iGrp} = cat(1,h{iGrp}{1});
            for i=1:size(h{iGrp}{2},2)
                hh{iGrp} = cat(1,hh{iGrp},h{iGrp}{2}{1,i});
                hh{iGrp} = cat(1,hh{iGrp},h{iGrp}{2}{2,i});
            end
            for i=1:size(h{iGrp}{3},2)
                if ~isnan(h{iGrp}{3}{1,i})
                    hh{iGrp} = cat(1,hh{iGrp},h{iGrp}{3}{1,i});
                end
                if ~isnan(h{iGrp}{3}{2,i})
                    hh{iGrp} = cat(1,hh{iGrp},h{iGrp}{3}{2,i});
                end
            end
        end
        hh = cat(1,hh{:});
%         hh(isnan(hh)) = [];% this is not compatible with R2014b
    case 'line'
        nGrps = numel(h);
        hh = cell(size(h));
        for iGrp = 1:nGrps
            hh{iGrp} = cat(1,h{iGrp}{1});
        end
        hh = cat(1,hh{:});
%         hh(isnan(hh)) = [];% this is not compatible with R2014b
end













