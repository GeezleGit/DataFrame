function [self, sortIndex] = df_sortSeries(self,seriesID,varargin)

% get series index
if isnumeric(seriesID)
    isPar = self.seriesdim==seriesID;
    if any(isPar)
        iSeries = find(isPar);
    else
        iSeries = length(self.seriesdim)+1;
    end
else
    iSeries = df_findSeriesNr(self,seriesID);
end

[self.seriesparam{iSeries}, sortIndex] = sort(self.seriesparam{iSeries},varargin{:});

for i=1:numel(self.cells)
    n = size(self.cells{i});
    permArray = 1:length(n);
    permArray(1) = self.seriesdim(iSeries);
    permArray(self.seriesdim(iSeries)) = 1;
    self.cells{i} = permute(self.cells{i},permArray);
    self.cells{i}(:,:) = self.cells{i}(sortIndex,:);
    self.cells{i} = permute(self.cells{i},permArray);
end
