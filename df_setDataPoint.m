function self = df_setDataPoint(self,i,v)
%% DF_SETDATAPOINT set data in cells
%
% expects identical array sizes in all cells
%
% self = df_setDataPoint(self,i,v)
% i ... scalar
% v ... matrix of size(self.cells)
%
% self = df_setDataPoint(self,[i1, i2, ...],v)
% i ... array of indices
% v ... cell array of matrices of size(self.cells)

nGrps = numel(self.cells);
if iscell(v)
    for iV = 1:numel(v)
        for iGrp = 1:nGrps
            self.cells{iGrp}(i(iV)) = v{iV}(iGrp);
        end
    end
else
    for iGrp = 1:nGrps
        self.cells{iGrp}(i) = v(iGrp);
    end
end