function DF = df_columnize(DF)
    nDF = length(DF);
    for i=1:nDF
        for j=1:numel(DF(i).cells)
            DF(i).cells{j} = DF(i).cells{j}(:);
        end
    end
end

