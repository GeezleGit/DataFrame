function self = df_resizeMap(self,ScaleFactor,varargin)
% apply the imresize of the image processing toolbox to data cells 

nGrps = numel(self.cells);

iSerX = self.seriesdim==2;
iSerY = self.seriesdim==1;
if ~any(iSerX) || ~any(iSerY)
    error('Data cells need two dimensional arrays!');
end

nX = length(self.seriesparam{iSerX});
nY = length(self.seriesparam{iSerY});

MXN = [nY nX] .* ScaleFactor;

self.seriesparam{iSerY} = getPixPos(self.seriesparam{iSerY},ScaleFactor(1));
self.seriesparam{iSerX} = getPixPos(self.seriesparam{iSerX},ScaleFactor(2));


for iGrp = 1:nGrps
    Z = self.cells{iGrp};
    nZ = size(Z);
    ndZ = ndims(Z);
    nQ = nZ;
    nQ([1:2]) = MXN;
    Q = zeros(nQ).*NaN;
    if ndZ>2
        subMapIndex = cell(1,max([2 ndZ-2]));
        subMapIndex(:) = {[1]};
        for iSM = 1:prod(nZ(3:end))
            [subMapIndex{:}] = ind2sub(nZ(3:end),iSM);
            [Q(:,:,subMapIndex{:})] = imresize(Z(:,:,subMapIndex{:}),MXN,varargin{:});
        end
    else
        [Q] = imresize(Z,MXN,varargin{:});
    end
    self.cells{iGrp} = Q;
end


function p = getPixPos(p,m)
n = length(p);
dp = median(diff(p));
pEdges(1) = p(1)-(dp/2);
pEdges(2) = p(end)+(dp/2);
pEdges = linspace(pEdges(1),pEdges(2),(m*n)+1);
dp = median(diff(pEdges));
p = pEdges(1:end-1)+(dp/2);

% dY = self.seriesparam{iSerY}(2)-self.seriesparam{iSerY}(1);
% dX = self.seriesparam{iSerX}(2)-self.seriesparam{iSerX}(1);
% % dY = diff(self.seriesparam{iSerY});
% % dX = diff(self.seriesparam{iSerX});
% 
% if MXN(1)==1
%     % do nothing
% else
% 
%     self.seriesparam{iSerY} = linspace(self.seriesparam{iSerY}(1) - (dY/2) + (dY/2/MXN(1)),self.seriesparam{iSerY}(end) + (dY/2) - (dY/2/MXN(1)),MXN(1));
%     self.seriesparam{iSerX} = linspace(self.seriesparam{iSerX}(1) - (dX/2) + (dX/2/MXN(2)),self.seriesparam{iSerX}(end) + (dX/2) - (dX/2/MXN(2)),MXN(2));
%     % self.seriesparam{iSerY} = linspace(self.seriesparam{iSerY}(1),self.seriesparam{iSerY}(end),MXN(1));
%     % self.seriesparam{iSerX} = linspace(self.seriesparam{iSerX}(1),self.seriesparam{iSerX}(end),MXN(2));
% end
