function varargout = df_transformData(DF,Option,varargin)
    % transforms content of data cells
    switch Option
        case 'MEANSTATS'
            k = numel(DF.cells);
            d = celldim(DF);
            DFm = DF;
            DFs = DF;
            DFse = DF;
            DFmci = DF;
            for i=1:k
                [DFm.cells{i},DFs.cells{i},DFse.cells{i},DFmci.cells{i}] = agmean(DF.cells{i},[],d);
            end
            varargout = {DFm DFs DFse DFmci};
        otherwise
            varargout = {};
    end
end

