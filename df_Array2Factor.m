function self = df_Array2Factor(self,FactorLabel,FactorLevel,FactorDim)
% Merge an array of DataFrames into Factors
%
% Parameters
% ----------
% FactorLabel: char or cell
% FactorLevel: double or cell
% FactorDim: int
%
% Examples
% --------
% ::
%
%     self = df_Array2Factor(self,FactorLabel,FactorLevel,FactorDim)
%
% Todo
% ----
% - subfunctions singleMerger and multiMerger are redundant, use multiMerger only

if nargin<4
    FactorDim = [];
end

if any(strcmp(self(1).factor,FactorLabel))
    error('Factor is already existing!');
end

if ischar(FactorLabel)
    nNewFac = 1;
else
    nNewFac = length(FactorLabel);
end

if nNewFac>1
    self = multiMerger(self,FactorLabel,FactorLevel,FactorDim);
else
    self = singleMerger(self,FactorLabel,FactorLevel,FactorDim);
end

function self = singleMerger(self,FactorLabel,FactorLevel,FactorDim)

nArr = length(self);
nFac = length(self(1).factor);
newFacDim = nFac+1;
self(1).factor{newFacDim} = FactorLabel;
self(1).level{newFacDim} = FactorLevel;
for iArr = 2:nArr
    self(1).cells = cat(newFacDim,self(1).cells,self(iArr).cells);
end
self = self(1);

if ~isempty(FactorDim)
    FactorArray = 1:newFacDim;
    FactorArray(newFacDim) = FactorDim;
    FactorArray(FactorDim) = newFacDim;
    FactorArray(FactorArray==0) = NaN;
    self = df_sortFactor(self,FactorArray);
end

function self = multiMerger(self,FactorLabel,FactorLevel,FactorDim)

nArr = length(FactorLabel);
nFac = length(self(1).factor);
newFacDim = nFac+1:nFac+nArr;

self(1).factor(newFacDim) = FactorLabel;
self(1).level(newFacDim) = FactorLevel;
newLevelNum = cellfun('length',self(1).level);

for i = 2:numel(self)
    self(1).cells = cat(newFacDim(1),self(1).cells,self(i).cells);
end
self = self(1);
self.cells = reshape(self.cells,newLevelNum);

if ~isempty(FactorDim)
    FactorArray = 1:max(newFacDim);
    FactorArray(newFacDim) = FactorDim;
    FactorArray(FactorDim) = newFacDim;
    FactorArray(FactorArray==0) = NaN;
    self = df_sortFactor(self,FactorArray);
end