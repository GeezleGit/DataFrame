function [pDF,dDF] = df_testLevel(self,FunName,FunInput,FactorName,LevelName)
% compares two levels of a DataFrame
% FunName ......
% FunInput ..... 
% FactorName ... FactorName
% LevelName .... LevelName LevelIndex

[nFac,nLev] = df_getsize(self);
p = ones(nLev).*NaN;
h = zeros(nLev).*NaN;
stats = cell(nLev);
for i=1:prod(nLev)
    X = DF(1).cells{i}(:);
    Y = DF(2).cells{i}(:);
    if isempty(X)||isempty(Y)||all(isnan(X))||all(isnan(Y))
        p(i) = NaN;
        h(i) = false;
        stats{i} = [];
        continue;
    end
    switch FunName
        case 'signrank'
            ii = ~isnan(X)&~isnan(Y);
            [p(i),h(i),stats{i}] = signrank(X(ii),Y(ii),FunInput{:});
        case 'signtest'
            ii = ~isnan(X)&~isnan(Y);
            [p(i),h(i),stats{i}] = signtest(X(ii),Y(ii),FunInput{:});
        case 'ttest'
            ii = ~isnan(X)&~isnan(Y);
            [h(i),p(i),ci,stats{i}] = ttest(X(ii),Y(ii),FunInput{:});
        case 'ttest2'
            iix = ~isnan(X);
            iiy = ~isnan(Y);
            [h(i),p(i),ci,stats{i}] = ttest2(X(iix),Y(iiy),FunInput{:});
        case 'ranksum'
            iix = ~isnan(X);
            iiy = ~isnan(Y);
            [p(i),h(i),stats{i}] = ranksum(X(iix),Y(iiy),FunInput{:});
    end
    
end

