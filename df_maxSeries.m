function [DF,DFpar] = df_maxSeries(DF, seriesLabel, range, keepDim)
%
% find the maximum in a range of series parameter
%
% _Syntax_ [DF,DFpar] = df_maxSeries(DF, seriesLabel, range, keepDim)
%
% _Input_
%
%     seriesLabel     [char]         series ID
%     range           [1 x 2 double] subrange of series parameter
%     keepDim         [logical]      don't reduce series dimension
%

if nargin<4
    keepDim = false;
end

serDim = df_getSeriesDim(DF, seriesLabel);
serInd = df_findSeriesNr(DF,seriesLabel);
serNum = df_seriesNum(DF,seriesLabel);
if nargin>2 && ~isempty(range)
    DF = df_extractSeries(DF,seriesLabel,'BOUNDS',range);
end
serPar = df_getSeries(DF, seriesLabel);
n = numel(DF.cells);
DFpar = DF;
for i = 1:n
    
    [DF.cells{i},serParIndex] = max(DF.cells{i},[],serDim);
    DFpar.cells{i} = serPar(serParIndex);
    
    if keepDim
        repArray = ones(1,ndims(DFpar.cells{i}));
        repArray(serDim) = serNum;
        DF.cells{i} = repmat(DF.cells{i},repArray);
        DFpar.cells{i} = repmat(DFpar.cells{i},repArray);
    end
    
end

if ~keepDim
    DF = df_removeSeriesInfo(DF,serInd);
    DFpar = df_removeSeriesInfo(DFpar,serInd);
end
