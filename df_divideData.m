function DF = df_divideData(DF,DFdiv)
    nDF = length(DF);
    for iDF = 1:nDF
        nGrps = numel(DF(iDF).cells);
        for iGrp = 1:nGrps
            DF(iDF).cells{iGrp} = DF(iDF).cells{iGrp}./DFdiv(iDF).cells{iGrp};
        end
    end
end

