function self = df_removeFactor(self,factorLabel)
% DF_REMOVEFACTOR remove a factor from data cells
%
% This only removes the factor information and rearranges the data cells.
% To remove non-empty data cells you have to a) keep one level, or b) collapse to one level of that factor 
%
% Example
% -------
% ::
%
%     self = df_removeFactor(self,factorLabel)


%% get series
if ischar(factorLabel)
    iFac  = find(strcmp(factorLabel,self.factor));
elseif isnumeric(factorLabel)
    iFac = factorLabel;
elseif islogical(factorLabel)
    iFac = find(factorLabel);
end

self.factor(iFac) = [];
self.level(iFac) = [];
currNumDim = max([ndims(self.cells) iFac(:)']);
self.cells = permute(self.cells,[setxor(1:currNumDim,iFac) iFac]);


