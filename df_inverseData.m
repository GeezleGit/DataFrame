function DF = df_inverseData(DF)
    nDF = length(DF);
    for iDF = 1:nDF
        nGrps = numel(DF(iDF).cells);
        for iGrp = 1:nGrps
            DF(iDF).cells{iGrp} = 1./DF(iDF).cells{iGrp};
        end
    end
end

