function ss = df_getSeriesSubs(self, sLabel, sPar, sMod)
% DF_GETSERIESSUBS get indices for all series parameter
%
% Example
% -------
% ::
% 
%     ss = df_getSeriesSubs(self, {'series1', ... 'seriesN'}, {[p1], ... [pN]}, {'mod1', ... 'modN'});
%     
%     ss = df_getSeriesSubs(self, 'series1', [p1]);
%     

% check arguments
if ischar(sLabel)
    sLabel = {sLabel};
end
labelNum = length(sLabel);
if isnumeric(sPar)
    [nRows,nCols] = size(sPar);
    if nRows~=labelNum
        error('');
    end
    sPar = mat2cell(sPar,ones(nRows,1),nCols);
end
if ischar(sMod)
    sMod = repmat({sMod},labelNum,1);
end

% check existing series
[sampD,seriesD] = df_celldim(self);
n = df_seriesNum(self);
dimNum = length(n);

% make default subscript array
if dimNum==1
    ss = cell(1,2);
else
    ss = cell(1,dimNum);
end
for i=1:length(ss)
    if i<=dimNum
        ss{i} = 1:n(i)';
    else
        ss{i} = 1;
    end
end

% extract parameter
for iLabel = 1:labelNum
    sIndex = strcmp(self.serieslabel,sLabel{iLabel});
    
    switch upper(sMod{iLabel})
        case 'INDEX'
            ss{self.seriesdim(sIndex)} = sPar{iLabel};
        case 'BOUNDS'
            ss{self.seriesdim(sIndex)} = self.seriesparam{sIndex}>=sPar{iLabel}(1) & self.seriesparam{sIndex}<=sPar{iLabel}(2);
        case 'WINDOW'
            ss{self.seriesdim(sIndex)} = self.seriesparam{sIndex}>sPar{iLabel}(1) & self.seriesparam{sIndex}<sPar{iLabel}(2);
        case 'BOUNDWINDOW'
            ss{self.seriesdim(sIndex)} = self.seriesparam{sIndex}>=sPar{iLabel}(1) & self.seriesparam{sIndex}<sPar{iLabel}(2);
        case 'WINDOWBOUND'
            ss{self.seriesdim(sIndex)} = self.seriesparam{sIndex}>sPar{iLabel}(1) & self.seriesparam{sIndex}<=sPar{iLabel}(2);
        case 'EXACT'
            if isnumeric(self.seriesparam{sIndex})
                ss{self.seriesdim(sIndex)} = ismember(self.seriesparam{sIndex},sPar{iLabel});
            elseif iscell(self.seriesparam{sIndex}) || ischar(extractParam)
                ss{self.seriesdim(sIndex)} = strcmp(sPar{iLabel},self.seriesparam{sIndex});
            end
        case 'PICK'
            numPar = length(sPar{iLabel});
            ss{self.seriesdim(sIndex)} = zeros(1,numPar);
            if isnumeric(self.seriesparam)
                for j = 1:numPar
                    ss{self.seriesdim(sIndex)}(j) = find(self.seriesparam==sPar{iLabel}(j));
                end
            elseif iscell(self.seriesparam)
                for j = 1:numPar
                    ss{self.seriesdim(sIndex)}(j) = find(strcmp(sPar{iLabel}(j),self.seriesparam));
                end
            end
    end
end
    
    
    
    
    
    
    
