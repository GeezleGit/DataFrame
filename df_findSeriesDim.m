function d = df_findSeriesDim(self,seriesLabel)

i = df_findSeriesNr(self,seriesLabel);

if isempty(i)
    d = [];
    return;
end

d = i;
d(isnan(i)) = NaN;
d(~isnan(i)) = self.seriesdim(i(~isnan(i)));
    



