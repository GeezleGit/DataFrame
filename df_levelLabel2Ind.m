function [iLevel,iFac] = df_levelLabel2Ind(DF,factorLabel,levelLabel)
% DF_LEVELLABEL2IND converts a level-label to a level-index
%
% Parameters
% ----------
% factorlabel: char or cell
%     factor-label
% levelLabel: char or double or cell
%     level-label
%
% Returns
% -------
% ilevel: int
%     level index
% iFac: int
%     factor index
%
% Examples
% --------
% ::
%
%     [iLevel,iFac] = df_levelLabel2Ind(DF,factorLabel,' ... ')
%     [iLevel,iFac] = df_levelLabel2Ind(DF,factorLabel,{ ... })
%     [iLevel,iFac] = df_levelLabel2Ind(DF,factorLabel,[ ... ])


iFac = strcmp(factorLabel,DF.factor);
if iscell(levelLabel)
    [founddummy,iLevel] = ismember(levelLabel,DF.level{iFac});
elseif ischar(levelLabel)
    iLevel = find(strcmp(levelLabel,DF.level{iFac}));
elseif isnumeric(levelLabel)
    iLevel = find(levelLabel==DF.level{iFac});
else
    error('Input error!!');
end