function cellIndex = df_findData(DF,varargin)
% DF_FINDDATA returns an index to use with DF.cells
%
% Example
% -------
% ::
% 
%     cellIndex = df_findData(DF,FactorName_1,LevelLabel_1, ...)
%

FacFindArray = varargin(1:2:end);
LevFindArray = varargin(2:2:end);

nFac = length(DF.level);
nLev = cellfun('length',DF.level);
cellIndex = cell(1,nFac);

for iFac = 1:nFac
    isSearchFac = strcmp(DF.factor(iFac),FacFindArray);
    
    if sum(isSearchFac)==0
        cellIndex(iFac) = {[1:nLev(iFac)]};
    
    elseif sum(isSearchFac)==1 && isnumeric(LevFindArray{isSearchFac})
        cellIndex{iFac} = find(ismember(DF.level{iFac},LevFindArray{isSearchFac}));
    
    elseif sum(isSearchFac)==1 && ~isnumeric(LevFindArray{isSearchFac})
        cellIndex{iFac} = find(strcmp(LevFindArray{isSearchFac},DF.level{iFac}));
    
    end
end

