function self = df_scale(self,Offset,Factor,seriesLabel)

% scale ...

if nargin<4
    seriesLabel = [];
end

nGrps = numel(self.cells);

if isempty(seriesLabel)
    for iGrp = 1:nGrps
        self.cells{iGrp} = self.cells{iGrp} .* Factor + Offset;
    end
else
    reversePermuteArray = self.serieslabel(self.seriesdim);
    self = df_permuteSeries(self,seriesLabel,1);
    nPar = length(self.seriesparam{self.seriesdim==1});
    for iGrp = 1:nGrps
        for iPar = 1:nPar
            self.cells{iGrp}(iPar,:) = self.cells{iGrp}(iPar,:) .* Factor(iPar) + Offset(iPar);
        end
    end
    self = df_permuteSeries(self,reversePermuteArray,true);
end



