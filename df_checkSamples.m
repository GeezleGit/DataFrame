function [d,n] = df_checkSamples(self)

d = cellfun('ndims',self.cells);
d = setxor([1:max(d(:))],self.seriesdim);
nSampDims = length(d);
n = zeros(1,nSampDims);
isSampDim = false(1,nSampDims);
for iDim = 1:nSampDims
    currNs = cellfun('size',self.cells,d(iDim));
    n(iDim) = max(currNs(:));
    if n(iDim)>1
        isSampDim(iDim) = true;
    end
end

d(~isSampDim) = [];
n(~isSampDim) = [];

