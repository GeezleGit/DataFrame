function [LevelNr,DataCell] = df_getCorrespondingCell(DF1,DF2,i,fixedLevel)
    % finds the cell that matches a cell in another DataFrame
    % according to factorlevels
    % [LevelNr,DataCell] = CorrespondingCell(DF1,DF2,i,fixedLevel)
    % DF1 ..........
    % DF2,i ........
    % fixedLevel ... {FactorName,{Factorlevel}, ...}
    
    % check DF2
    cLevel = getLevelFromIndex(DF2,i);
    
    % search for  group in DF1
    SearchTerm = [DF2.factor;cLevel];
    SearchTerm = SearchTerm(:)';
    LevelNr = findData(DF1,SearchTerm{:});
    % set a fixed level according to input
    if nargin>=4 && ~isempty(fixedLevel)
        [iLevel,FacNr] = findFactorLevel(DF1,fixedLevel{:});
        LevelNr{FacNr} = iLevel;
    end
    
    DataCell = DF1.cells(LevelNr{:});
end
