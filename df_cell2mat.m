function X = df_cell2mat(DF,varargin)
    % Extracts single element of data cells
    % X = cell2mat(DF,IndexDim1,IndexDim2, ... IndexDimN)
    % If input is DataFrame array, the mat for each DataFrame will
    % be concatenated in first dimension
    if ~isempty(DF(1).seriesdim) && ~isnan(DF(1).seriesdim)
        % timeseries
        nDF = length(DF);
        index = varargin;
        SampleDim = celldim(DF(1));
        SeriesDim = DF(1).seriesdim;
        for iDF = 1:nDF
            X{iDF} = DF(iDF).cells{index{:}};
            Xalign(iDF,:) = [1 1 1];
        end
        X  = mergearrays(X,SampleDim,Xalign);
    else
        nDF = length(DF);
        EmptyCell = false(1,nDF);
        for iDF = 1:nDF
            if isempty(DF(iDF))
                X{iDF} = NaN;
                nFac(iDF) = 0;
                EmptyCell(iDF) = true;
            else
                EmptyCell(iDF) = false;
                [nFac(iDF),nLev(iDF,:),nEl] = getsize(DF(iDF));
                
                % check cells for single element (scalar)
                if any(nEl(:)>1)
                    error('cell2mat works only for data cells containing 1 element!');
                elseif any(nEl(:)==0)
                    DF(iDF).cells(nEl==0) = {NaN};
                end
                
                % construct index vector
                if nargin<2
                    index = cell(1,nFac(iDF));
                    index(:) = {[]};
                else
                    index = varargin;
                end
                
                % contruct index vector for "all" elements
                for i=1:nFac(iDF)
                    if isempty(index{i})
                        index{i} = 1:nLev(iDF,i);
                    end
                end
                
                % transform cells to numerical
                X{iDF} = cell2mat(DF(iDF).cells(index{:}));
            end
        end
        nLev = max(nLev);
        X(EmptyCell) = {ones(nLev).*NaN};
        X(cellfun('isempty',X)) = {ones(nLev).*NaN};% save for empty cells
        if nDF==1
            X = X{1};
        elseif nDF>1
            nFac = unique(nFac(~EmptyCell));
            X = cat(nFac+1,X{:});
            X = shiftdim(X,nFac);
        else
            X = [];
        end
    end
end
