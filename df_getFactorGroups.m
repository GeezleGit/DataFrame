function [grp,rem] = df_getFactorGroups(self, varargin)

numFac = length(self.factor);
nGrps = length(varargin);

rem.name  = self.factor;
rem.index = 1:numFac;
rem.level = self.level;
rem.n     = cellfun('length',self.level);

grp = repmat(rem,nGrps,1);

for iGrp = 1:nGrps
    isFac = ismember(rem.name, varargin{iGrp});
    
    grp(iGrp).name  = rem.name(isFac);
    grp(iGrp).index = rem.index(isFac);
    grp(iGrp).level = rem.level(isFac);
    grp(iGrp).n     = rem.n(isFac);

    rem.name  = rem.name(~isFac);
    rem.index = rem.index(~isFac);
    rem.level = rem.level(~isFac);
    rem.n     = rem.n(~isFac);
end

