function varargout = df_getLevelPairs(self,factorLabel,factorLevel,outputMode)
%% DF_GETLEVELPAIRS
%
% outputMode ... 'indices','cells','frames'

warning('UNDER CONSTRUCTION!!!');

levN   = size(self.cells);
facN   = length(levN);
grpN   = prod(levN);

[x1,x2] = df_getLevelPairIndex(self);
nPairs = size(x1,1);

%% extract a pair level
if ~isempty(factorLabel) && ~isempty(factorLevel)
    [iLevel,iFac] = df_findFactorLevel(self,factorLabel,factorLevel);
    keepPairIndex = (x1(:,iFac)==iLevel(1) & x2(:,iFac)==iLevel(2)) | (x1(:,iFac)==iLevel(2) & x2(:,iFac)==iLevel(1));
    x1(~keepPairIndex,:) = [];
    x2(~keepPairIndex,:) = [];

    sortIndex = find(x2(:,iFac)==iLevel(1));
    i_x1 = x1(sortIndex,:);
    i_x2 = x2(sortIndex,:);
    x1(sortIndex,:) = i_x2;
    x2(sortIndex,:) = i_x1;
elseif ~isempty(factorLabel) && isempty(factorLevel)
    iFac = strcmp(self.factor,factorLabel);
    keepPairIndex = all(x1(:,~iFac) == x2(:,~iFac),2);
    x1(~keepPairIndex,:) = [];
    x2(~keepPairIndex,:) = [];
end

%% make labels
nPairs = size(x1,1);
label1 = cell(nPairs,facN);
label2 = cell(nPairs,facN);
for iPair = 1:nPairs
    for iFac = 1:facN
        label1{iPair,iFac} = self.level{iFac}(x1(iPair,iFac));
        label2{iPair,iFac} = self.level{iFac}(x2(iPair,iFac));
    end
end

%%
switch outputMode
    case 'indices'
        varargout{1} = x1;
        varargout{2} = x2;
        varargout{3} = label1;
        varargout{4} = label2;
    case 'cells'
    case 'frames'
end
