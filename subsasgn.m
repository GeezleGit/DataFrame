function s = subsasgn(s,subscript,value)

% Subscript assignmemt for the @DataFrame object. 
%
% function s = subsasgn(s,subscript,value)
%

if ~isa(s,'DataFrame') && isempty(s)
    s = DataFrame;
end

if length(subscript)==1 && strcmp(subscript(1).type,'()')
    % this is to create arrays of al_spk
   s(subscript(1).subs{:}) = value;
elseif length(subscript)==1 && strcmp(subscript(1).type,'.')
    AllowedFields = {};
    if ismember(subscript(1).subs,AllowedFields)
        s.(char(subscript(1).subs)) = value;
    else
        error('Can''t assign values to fields "%s", use a method please!',char(subscript(1).subs));
    end
else
    error('Can''t assign values to fields, use a method please!');
end



