function [v, indexCells, indexSeries] = df_vectorize(DF,indexMode)
% concatenate content of cells
%
% indexMode        char 'subscript' all subscripts
%                       'index'     all index
%                       'sub/ind'   cells: subscripts, series: index
%                       'ind/sub'   cells: index,      series: subscripts
%
% v                [n x 1]       elements of cells as vertical vector
% indexCells       [n x 1]       index of cell for each element
% indexSeries      [n x nSeries] series dimension for each element

if nargin<2
    indexMode = 'ind/sub';
end

nLevel = size(DF.cells);
nFactors = length(nLevel);
nCells = prod(nLevel);
nDimCells = cellfun('ndims',DF.cells);
nSeries = max(nDimCells(:));

% squeeze cells
indexCells  = cell(nLevel);
indexSeries = cell(nLevel);
seriesNum   = cell(1,nSeries);
seriesSub   = cell(1,nSeries);
cellSub = cell(1,nFactors);
for iCell = 1:nCells 
    
    % check data in cells
    [seriesNum{:}] = size(DF.cells{iCell});
    sN = cat(2,seriesNum{:});
    nData = prod(sN);

    % squeeze data in cells
    DF.cells{iCell}    = DF.cells{iCell}(:);

    switch indexMode
    case 'subscript'
        [cellSub{:}]       = ind2sub(nLevel,iCell);
        indexCells{iCell}  = repmat(cat(2,cellSub{:}),[nData 1]);
        [seriesSub{:}]     = ind2sub(sN,[1:nData]');
        indexSeries{iCell} = cat(2,seriesSub{:});
    case 'index'
        indexCells{iCell}  = ones(nData,1).*iCell;
        indexSeries{iCell} = [1:nData]';
    case 'sub/ind'
        [cellSub{:}]       = ind2sub(nLevel,iCell);
        indexCells{iCell}  = repmat(cat(2,cellSub{:}),[nData 1]);
        indexSeries{iCell} = [1:nData]';
    case 'ind/sub'
        indexCells{iCell}  = ones(nData,1).*iCell;
        [seriesSub{:}]     = ind2sub(sN,[1:nData]');
        indexSeries{iCell} = cat(2,seriesSub{:});
    end

end

% concatenate cells
v           = cat(1,DF.cells{:});
indexCells  = cat(1,indexCells{:});
indexSeries = cat(1,indexSeries{:});

