function [isF,FNr] = df_isFactor(DF,varargin)
% DF_ISFACTOR 
%
% Example
% -------
% ::
% 
%     [isF,FNr] = ismember(varargin,DF.factor);

[isF,FNr] = ismember(varargin,DF.factor);

