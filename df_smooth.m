function self = df_smooth(self,seriesID,varargin)

if isnumeric(seriesID)
    iSer = seriesID;
else
    iSer = df_findSeriesNr(self,seriesID);
end
dSer = self.seriesdim(iSer);


for iGrp = 1:numel(self.cells)
    Y = self.cells{iGrp};
    
    s = size(Y);
    parr = 1:length(s);
    parr([1 dSer]) = [dSer 1];
    s([1 dSer]) = s([dSer 1]);
    Y = permute(Y,parr);
    Y = Y(:,:);
    for i=1:size(Y,2)
        Y(:,i) = smooth(Y(:,i),varargin{:});
    end
    Y = reshape(Y,s);
    parr([1 dSer]) = [dSer 1];
    Y = permute(Y,parr);
    
    self.cells{iGrp} = Y;
end

