function DF = df_removeFactorLevel(DF,cFac,cLevel)
    % removes data of a given factor/level
    % DF = removeFactorLevel(DF,cFac,cLevel)
    nDF = length(DF);
    for iDF = 1:nDF
        [nFac,nLev,nEl] = df_getsize(DF(iDF));
        if islogical(cLevel)
            iFac = df_findFactor(DF(iDF),cFac);
            iLevel = find(cLevel);
        else
            [iLevel,iFac] = df_findFactorLevel(DF(iDF),cFac,cLevel);
        end
        if isnan(iFac);continue;end
        
        DF(iDF).level{iFac}(iLevel) = [];
        if nFac>1
            %DF(iDF).cells = shiftdim(DF(iDF).cells,iFac-1);% shift to first
            permArray = circshift([1:nFac],[1 1-iFac 1]);
            DF(iDF).cells = permute(DF(iDF).cells,permArray);
            
            cDFsize = size(DF(iDF).cells);
            DF(iDF).cells(iLevel,:) = [];
            cDFsize(1) = cDFsize(1)-length(iLevel);
            DF(iDF).cells = reshape(DF(iDF).cells,cDFsize);
            
            %DF(iDF).cells = shiftdim(DF(iDF).cells,nFac-(iFac-1));% shift back
            permArray = circshift([1:nFac],[1 (1-iFac)*(-1) 1]);
            DF(iDF).cells = permute(DF(iDF).cells,permArray);
            
        else
            DF(iDF).cells(iLevel,:) = [];
        end
    end
end
