function t = df_table(DF,varName,subjName,removeNans,filepath)
%
% transform a @DataFrame to a matlab table
% 
% varName .... name of the "dependent" variable, i.e. the datatype/units in cells
% subjName ... name for each SamplePoint, default is sample index in cells

if nargin<5
    filepath = [];
    if nargin<4
        removeNans = [];
        if nargin<3
            subjName = [];
        end;end;end

[samDim,serDim]     = df_celldim(DF);
[facLabel,levLabel] = df_getFactor(DF);
nSer                = length(serDim);
nFac                = length(facLabel);

[v, indexCells, indexSeries] = df_vectorize(DF,'subscript');
if removeNans
    indexSeries(isnan(v),:) = [];
    indexCells(isnan(v),:) = [];
    v(isnan(v)) = [];
end

% add the dependent variable first
t = table(v,'VariableNames',{varName});

% now add subjects/samples
if islogical(subjName) && subjName
    t.subject = indexSeries(:,samDim);
elseif ~islogical(subjName) && ~isempty(subjName)
    t.subject = subjName(indexSeries(:,samDim));
end    

% now add the factors
for iFac = 1:nFac
    t.(DF.factor{iFac}) = DF.level{iFac}(indexCells(:,iFac))';
end

% now add series
% remember that series index does not translate directly into array dimension 
for iSer = 1:nSer
    serNr = find(DF.seriesdim==serDim(iSer));
    t.(DF.serieslabel{serNr}) = DF.seriesparam{serNr}(indexSeries(:,serDim(iSer)))';
end

% write a file if a file name is given
if ~isempty(filepath)
    writetable(t,filepath);
end