function x = df_getCells(self,levelIndex,seriesIndex,vectorize)
% Extract data cells
%
% Parameters
% ----------
% levelIndex: int
%     index for data cells, get it from :func:`df_findData`
% seriesIndex: cell
%     ``{[] [] ... []}`` subscript for data in cells, get it from :func:`df_getSeriesSubscript`
% vectorize: bool
%     if all data cells contain scalars, cells will be vertically concatenated
%
% Returns
% -------
% x: cell
%
% Examples
% --------
% ::
% 
%     x = df_getCells(self,levelIndex,seriesIndex,vectorize)
%

%% check input
if nargin<4
    vectorize = false;
    if nargin<3
        seriesIndex = [];
        if nargin<2
            levelIndex = [];
        end;end;end

%% get cells
if isempty(levelIndex) && isempty(seriesIndex)
    x = self.cells;
elseif ~isempty(levelIndex)  && isempty(seriesIndex)
    if isnumeric(levelIndex)
        x = self.cells(levelIndex(:,1));
    elseif iscell(levelIndex)
        x = self.cells(levelIndex{:});
    else
        error('');
    end
elseif ~isempty(levelIndex) && ~isempty(seriesIndex)
    if isnumeric(levelIndex)
        x = cell(size(levelIndex));
        for i = 1:numel(levelIndex)
            x{i} = self.cells{levelIndex(i)}(seriesIndex{:});
        end
    elseif iscell(levelIndex)
        x = self.cells{levelIndex{:}}(seriesIndex{:});
    end
elseif isempty(levelIndex) && ~isempty(seriesIndex)
    x = cellfun(@(x) x(seriesIndex{:}), self.cells, 'UniformOutput',false);
end

%% vectorize
if vectorize
    cdimnum = cellfun('ndims',x);
    currCatDim = max(cdimnum)+1;
    if numel(x)==1
        x = x{1};
    elseif ~any(cdimnum>2)
        % scalar cells detection
        nRows = cellfun('size',x,1);
        nCols = cellfun('size',x,2);
        if ~any(nRows(:)>1)&&~any(nCols(:)>1)
            currCatDim = 1;
        end
        out = zeros(size(x)).*NaN;
        isOk = ~cellfun('isempty',x);
        out(isOk) = cat(currCatDim,x{isOk});
        x = out;
    else
        currCatDim = max(cdimnum)+1;
        error('under construction');
    end
        
end
