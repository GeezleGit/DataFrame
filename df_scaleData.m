function DF = df_scaleData(DF,scalefactor)
    nDF = length(DF);
    for iDF = 1:nDF
        nGrps = numel(DF(iDF).cells);
        for iGrp = 1:nGrps
            DF(iDF).cells{iGrp} = DF(iDF).cells{iGrp}.*scalefactor;
        end
    end
end

