function levellabel = df_levelInd2Label(DF,iFactor,iLevel)
levellabel = DF.level{iFactor}(iLevel);